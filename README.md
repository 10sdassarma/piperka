Installation instructions for Piperka backend
=============================================

The backend has a dependency on the crawler, please follow its
installation instructions first. To download and install the Haskell
dependencies, run

```
cabal sandbox init
cabal sandbox add-source ../crawler
cabal configure
cabal install --only-dependencies --force-reinstalls
```

The default development configuration uses dev.piperka.net as its
hostname. The login cookies are set as secure and you will need to set
up a reverse HTTPS proxy. Piperka expects forwarded requests to have
`X-Proxy-Forward` (IP address) and `X-Original-Port` headers set.  Add
the hostname to `/etc/hosts` and set it to point to localhost. The
backend server runs on port 8000.

Here's an example nginx configuration:
```
server {
	listen 80;
	listen [::]:80;
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	# SSL needs a cert, at least Debian makes this available
	include snippets/snakeoil.conf;

	server_name dev.piperka.net;

	location / {
		proxy_set_header X-Proxy-Forward $remote_addr;
		proxy_set_header X-Original-Port $server_port;
		proxy_pass http://127.0.0.1:8000;
	}
}
```

The server uses a bit of XSLT for its setup.

```
sudo apt install xsltproc
```


Database
--------

```
sudo apt install postgresql-plperl postgresql-contrib
```

Piperka uses "kaol" as its user (for my own convenience). To enable
logins as that user for yourself, add something like this to
`pg_ident.conf`: `piperka youruser kaol` and edit `pg_hba.conf` as
follows: `local piperka all peer map=piperka` and restart postgresql.

Download the schema from https://piperka.net/piperka-schema.sql and
run the following commands as postgres superuser.

```
CREATE ROLE kaol WITH LOGIN;
CREATE ROLE piperka;
GRANT piperka TO kaol;
CREATE DATABASE piperka WITH OWNER piperka;
GRANT ALL ON DATABASE piperka TO kaol;
\c piperka
CREATE LANGUAGE plperl;
CREATE EXTENSION plpgsql WITH SCHEMA pg_catalog;
CREATE EXTENSION pgcrypto WITH SCHEMA public;
CREATE EXTENSION "uuid-ossp" WITH SCHEMA public;
```

Install the schema with (as postgres superuser)

```
psql piperka postgres < piperka-schema.sql
```

Configuration files
-------------------

The server requires some configuration files. The source tree includes
example configuration, copy them as follows.

```
cp devel.cfg.example devel.cfg
cp snaplets/auth/devel.cfg.example snaplets/auth/devel.cfg
cp snaplets/apiAuth/devel.cfg.example snaplets/apiAuth/devel.cfg
```

Congratulations, this should be enough to start the site. The site
should be fine to run with `cabal repl` and then `main`. The crawler
page may experience hiccups when run that way due to websockets use.

Moderator role
--------------

To start with, create a user account.

Some functionality may be restricted to user with uid 3 (which is what
I use on the real site). You may want to adjust your admin uid to be
the same. Run an update to the uid in the `login_method` table if you
change the uid after account creation.

You'll also need to run `insert into moderator (uid) values (3)`.

Crawler
-------

You'll need a parser configuration in the `parsers` table to use
it. Use `insert into parsers (start_hook) values ('')` to insert this
example.

```perl
if ($tag eq "a" && exists $attr->{rel} && $attr->{rel} eq "next") {
    my $url_tail = quotemeta($param->{url_tail});
    my $base = quotemeta($url_base);
    my $full_uri = URI->new_abs($attr->{href}, $url);
    if ($full_uri =~ m<^$base(.+)$url_tail$>) {
        $next = $1;
    }
    $self->eof;
}
```

Add a few symbolic links (set CRAWLER_DIR as appropriate first):

```
ln -s $(CRAWLER_DIR)/bin/perl_parser bin
mkdir lib
ln -s $(CRAWLER_DIR)/lib/crawler lib
ln -s $(CRAWLER_DIR)/crawler.cfg .
```

Then head to mod_crawler.html. To see that it works, set `parser_type`
as 1 and use `https://piperka.net/blog/` as the `url_base`, `/` as
`url_tail` and insert `2011/hello-world` as the first page. Click
start and it should crawl all the blog pages (I don't mind).

Moderator interface
-------------------

Here's a list of pages relevant to maintaining the site:

* moderate.html
* submissions.html
* mod_ticket.html

The usual way to navigate to the crawler page is from the submissions
or the mod_ticket pages. The easiest way to add comics is to submit
them from the usual submit page first and by opening the crawler from
the submissions page. After saving the initial crawl, click on the
genentry link.

Recommendations
---------------

Recommendations are provided by `recommenderlab` R package.  Install
`recommend.R` in $PATH without the suffix to enable them.

```
install.packages("recommenderlab")
install.packages("RPostgreSQL")
```

As of this writing R versions 3.5.0 and 3.5.1 have a bug that prevent
calling scripts via piping.  3.4 is known to work.  Alternatively add
`recommendEnabled = false" to `devel.cfg` to disable the page.
