(function( $ ){
    function centerDialog() {
	var row = $('#archivedialog tr:has(#currentpagemarker)');
	if (row.length > 0) {
	    var ord = row.data('ord');
	    $('#archivedialog').get(0).scrollTop = -($('#archivedialog').height()/2)+$('#archivedialog tr').get(ord).offsetTop;
	}
    }

    $.fn.pPageDialog = function() {
	$(this).dialog({autoOpen: false,
			minwidth: 300,
			width: 400,
			height: 300,
			open: centerDialog,
			position: {my: "left center", at: "left+10% center", of: window},
		       });
    }
    $.fn.pThumbDialog = function() {
	$(this).dialog({autoOpen: false,
			minwidth: 400,
			width: 700,
			height: 600,
			open: function() {
			    // Trigger to load first images
			    $(this).trigger('scroll');
			},
			position: {my: "left center", at: "right top", of: document.getElementById('archivedialog')}
		       });
    };
})( jQuery );

function makePageDialog(cid) {
    function archivePageSelected(ord) {
	$('.thumbnail').removeClass('thumb-selected');
	$('#thumb-'+ord).addClass('thumb-selected');
	var existing = $('#archive-'+ord).has('#currentpagemarker').length > 0
	$('#currentpagemarker').show().appendTo($('#archivedialog tr:eq('+(ord+1)+') .status'));
	return existing;
    }

    function thumbSelected() {
	// Move archive dialog contents to match
	var ord = $(this).data('ord');
	archivedialog[0].scroll(0, -archivedialog.height()/2+$('#archive-'+ord)[0].offsetTop);
	$('#archive-'+ord).trigger('archive_select', archivePageSelected(ord));
    }

    function archiveRowSelected() {
	// Move thumb to match
	var ord = $(this).data('ord');
	thumbdialog[0].scroll(0, -thumbdialog.height()/2+$('#thumb-'+ord)[0].offsetTop);
	$(this).trigger('archive_select', archivePageSelected(ord));
    }

    function openThumbs() {
	$('#thumbdialog').dialog('open');
    }

    var thumbdialog = $('#thumbdialog')
    var archivedialog = $('#archivedialog');
    var table = $('#archivedialog tbody');
    table.empty();
    thumbdialog.empty();
    thumbdialog.off('scroll');
    var lastScroll = undefined;
    thumbdialog.on('scroll', function(ev) {
	if (lastScroll === undefined || Math.abs(lastScroll-this.scrollTop) > 100) {
	    lastScroll = this.scrollTop;
	    var event = new Event('loadThumb');
	    var top = this.scrollTop-300;
	    var bottom = this.scrollTop+this.clientHeight+200;
	    var el = this.firstElementChild;
	    while (el) {
		if (top < el.offsetTop && el.offsetTop < bottom)
		    el.dispatchEvent(event);
		el = el.nextElementSibling;
	    }
	}
    });
    return $.when($.ajax({url:'https://'+window.location.hostname+'/s/archive/'+cid, method: 'GET', dataType: 'json'}))
	.then(function(rpy){
	    archivedialog.data(rpy);
	    var haveThumbs = false;
	    $.each(rpy.pages, function(idx){
		haveThumbs = this[3] || haveThumbs;
		var archiveRow = $('<tr id="archive-'+idx+'"/>')
		    .data('page', this[0])
		    .data('ord', idx)
		    .data('maxsubord', this[1])
		    .append('<td>'+(idx+1)+'</td>')
		    .append('<td class="page">'+(this[0] != null ? this[0] : 'Current page')+'</td>')
		    .append('<td class="status"/>')
		    .appendTo(table);
		var thumb = $('<div id="thumb-'+idx+'" class="thumbnail"/>');
		if (this[3]) {
		    var params = {ord: idx, cid: cid, title: this[4]};
		    thumb.one('loadThumb', function() {
			$('<img src="/thumbs/'+params.cid+'/'+params.ord+'.jpg"/>')
			    .attr('title', 'Pg '+(params.ord+1)+' — '+params.title)
			    .appendTo(thumb);
		    });
		} else {
		    thumb.addClass('missingThumb');
		}
		var selectFunc = function() {
		}
		thumb.data('ord', idx)
		    .appendTo(thumbdialog);
		thumb.on('click', thumbSelected);
		archiveRow.on('click', archiveRowSelected);
	    });
	    table.append($('<tr><td class="status"/></td>'));
	    archivedialog.off('dialogopen', openThumbs);
	    if (haveThumbs) {
		archivedialog.on('dialogopen', openThumbs);
	    }
	    return rpy;
	});
}
