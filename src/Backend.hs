{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE FlexibleContexts #-}

module Backend ( oauth2Login, oauth2Check ) where

import Piperka.Listing.Types

import Contravariant.Extras.Contrazip
import Control.Applicative
import Control.Monad (when)
import Control.Monad.Trans
import Control.Monad.Trans.Except
import qualified Data.Binary
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Char (isSpace)
import Data.Functor.Contravariant
import Data.Monoid
import qualified Data.Text as T
import Data.Text (Text)
import Data.UUID
import Snap
import Snap.Snaplet.CustomAuth
import Snap.Snaplet.Hasql
import qualified Hasql.Encoders as EN
import qualified Hasql.Decoders as DE
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import PostgreSQL.ErrorCodes (unique_violation)

import Application
import Piperka.OAuth2.Types
import Piperka.Util (monadicTuple2, getParamText)

encodeLoginPasswd :: EN.Params (T.Text, T.Text)
encodeLoginPasswd =
  contramap fst (EN.param EN.text) <>
  contramap snd (EN.param EN.text)

userRow :: DE.Row UUID -> DE.Row MyData
userRow x =
  MyData
  <$> DE.column DE.int4
  <*> DE.column DE.text
  <*> x
  <*> DE.column DE.uuid
  <*> (fromIntegral <$> DE.column DE.int4)
  <*> prefsRow

prefsRow :: DE.Row UserPrefs
prefsRow =
  UserPrefs
  <$> DE.column DE.int4
  <*> (liftA intToColumns (DE.column DE.int4))
  <*> DE.column DE.bool

-- I get the feeling that there should be a better way to express this.
userStatsRow :: DE.Row UUID -> DE.Row UserWithStats
userStatsRow x =
  (\(a,b,c) -> UserWithStats a (b,c))
  <$> (DE.column $ DE.composite ((,,)
                                <$> DE.field DE.int4
                                <*> DE.field DE.int4
                                <*> DE.field DE.int4))
  <*> liftA monadicTuple2 ((,)
                           <$> DE.nullableColumn DE.int4
                           <*> DE.nullableColumn DE.int4)
  <*> userRow x

userDefaultRow :: Text -> DE.Row MyData
userDefaultRow n =
  MyData
  <$> DE.column DE.int4
  <*> pure n
  <*> DE.column DE.uuid
  <*> DE.column DE.uuid
  <*> pure 0
  <*> pure defaultUserPrefs

statsDefaultRow :: Text -> DE.Row UserWithStats
statsDefaultRow n =
  defaultUserStats
  <$> userDefaultRow n

doLogin
  :: (MonadIO m, HasHasql m)
  => Text
  -> Text
  -> (DE.Row UUID -> DE.Row u)
  -> ByteString
  -> m (Either QueryError (Maybe u))
doLogin u pwd decodeRow sql = run $ statement (u, pwd) loginUserPasswd
    where
      loginUserPasswd = Statement sql encode decode True
      encode = encodeLoginPasswd
      decode = DE.rowMaybe $ decodeRow (DE.column DE.uuid)

doLogout
  :: (MonadIO m, HasHasql m)
  => Text
  -> m ()
doLogout t = do
  let tokenUuid = fromText t
  case tokenUuid of
   Nothing -> return ()  -- Don't know what to do in this case
   Just t' -> do
     run $ statement t' deleteSession
     return ()
  where
    deleteSession = Statement sql (EN.param EN.uuid) DE.unit True
    sql = "delete from p_session where ses=$1"

doRecover
  :: (MonadIO m, HasHasql m)
  => Text
  -> (DE.Row UUID -> DE.Row u)
  -> ByteString
  -> m (Either (AuthFailure QueryError) u)
doRecover t decodeRow sql = do
  let tokenUuid = fromText t
  case tokenUuid of
    Nothing -> return $ Left $ Login NoSession
    Just token -> do
      usr <- run $ statement token recoverSess
      return $ either (Left . UserError)
        (maybe (Left $ Login SessionRecoverFail) Right) usr
        where
          recoverSess = Statement sql encode decode True
          encode = contramap id (EN.param EN.uuid)
          decode = DE.rowMaybe $ decodeRow (pure token)

doPrepare
  :: (HasHasql m, MonadIO m, HasUserID u)
  => Maybe u
  -> Text
  -> m (Either QueryError AuthID)
doPrepare u p = run $ statement (p, extractUid <$> u) stmt
  where
    stmt = Statement sql encode decode True
    encode = contrazip2 (EN.param EN.text) (EN.nullableParam EN.int4)
    decode = DE.singleRow $ DE.column DE.int4
    sql = "select auth_create_password($1, $2)"

doCancelPrepare
  :: (HasHasql m, MonadIO m)
  => AuthID
  -> m ()
doCancelPrepare u = (run $ statement u stmt) >> return ()
  where
    stmt = Statement sql encode DE.unit True
    encode = EN.param EN.int4
    sql = "delete from login_method where lmid=$1"

validName
  :: Text
  -> Bool
validName name = len > 1 && len < 40 && isTrimmed
  where
    len = T.length name
    isTrimmed = not (isSpace (T.head name) || isSpace (T.last name))

doCreate
  :: (HasHasql m, MonadIO m, MonadSnap m)
  => Text
  -> AuthID
  -> DE.Result b
  -> m (Either (Either QueryError CreateFailure) b)
doCreate u loginId decode = runExceptT $ do
  email <- lift $ getParamText "email"
  when (not $ validName u) $ throwE $ Right InvalidName
  usr <- lift $ run $ statement (u, email, loginId) stmt
  either (throwE . maybeDuplicate) return usr
    where
      stmt = Statement sql encode decode True
      encode = contrazip3 (EN.param EN.text) (EN.nullableParam EN.text) (EN.param EN.int4)
      sql = "select uid, p_session, csrf_ham from auth_create($1, $2, $3)"
      maybeDuplicate e =
        if isDuplicateSqlError e then Right DuplicateName else Left e

attach
  :: (HasHasql m, MonadIO m, HasUserID u)
  => u
  -> AuthID
  -> m (Either QueryError ())
attach u i =  run $ statement (extractUid u, i) stmt
  where
    stmt = Statement sql encode DE.unit True
    encode = contrazip2 (EN.param EN.int4) (EN.param EN.int4)
    sql = "insert into login_method (uid, lmid) values ($1, $2)"

isDuplicateSqlError
  :: QueryError
  -> Bool
isDuplicateSqlError (QueryError _ _ (ResultError (ServerError c _ _ _))) = c == unique_violation
isDuplicateSqlError _ = False

-- For use with partial HTML render and AJAX calls
instance IAuthBackend MyData AuthID QueryError App where
  preparePasswordCreate = doPrepare
  cancelPrepare = doCancelPrepare
  create u i = doCreate u i $ DE.singleRow $ userDefaultRow u
  attachLoginMethod = attach
  login u pwd = doLogin u pwd userRow
                "select uid, name, p_session, csrf_ham, \
                \coalesce(level, 0) as moderator, \
                \display_rows, display_columns, new_windows \
                \from auth_login($1, $2) join users using (uid) \
                \left join moderator using (uid)"
  logout = doLogout
  recover t = doRecover t userRow
              "select uid, name, csrf_ham, coalesce(level, 0) as moderator, \
              \display_rows, display_columns, new_windows \
              \from recover_session($1) join users using (uid) \
              \left join moderator using (uid)"
  getUserId = return . toStrict . Data.Binary.encode . uid
  isDuplicateError = return . isDuplicateSqlError

-- Side effect: updates last read stats on login/recover
instance IAuthBackend UserWithStats AuthID QueryError App where
  preparePasswordCreate = doPrepare
  cancelPrepare = doCancelPrepare
  create u i = doCreate u i $ DE.singleRow $ statsDefaultRow u
  attachLoginMethod = attach
  login u pwd = doLogin u pwd userStatsRow
                "select (select (new_comics, total_new, new_in) from \
                \get_and_update_stats(uid, true)), \
                \mod_queue, cast(mod_days as int), \
                \uid, name, p_session, csrf_ham, coalesce(level, 0), \
                \display_rows, display_columns, new_windows \
                \from auth_login($1, $2) join users using (uid) left join \
                \(select uid, level, (select count(*) from only user_edit \
                \where cid in (select cid from comics)) as mod_queue, \
                \coalesce(extract(days from now() - last_moderate), 0) as mod_days \
                \from moderator) as m using (uid)"
  logout = doLogout
  recover t = doRecover t userStatsRow
              "select (select (new_comics, total_new, new_in) from \
              \get_and_update_stats(uid, false)), \
              \mod_queue, cast(mod_days as int), \
              \uid, name, csrf_ham, coalesce(level, 0), \
              \display_rows, display_columns, new_windows \
              \from recover_session($1) join users using (uid) left join \
              \(select uid, level, (select count(*) from only user_edit \
              \where cid in (select cid from comics)) as mod_queue, \
              \coalesce(extract(days from now() - last_moderate), 0) as mod_days \
              \from moderator) as m using (uid)"
  getUserId = return . toStrict . Data.Binary.encode . uid . user
  isDuplicateError = return . isDuplicateSqlError

oauth2Login
  :: (HasHasql m, MonadIO m)
  => Provider
  -> Text
  -> m (Either QueryError (Maybe MyData))
oauth2Login provider token = do
  run $ statement (fromIntegral $ providerOpid provider, token) stmt
  where
    stmt = Statement sql encode decode True
    encode = contrazip2 (EN.param EN.int4) (EN.param EN.text)
    decode = DE.rowMaybe $ userRow (DE.column DE.uuid)
    sql = "select uid, name, p_session, csrf_ham, level, \
          \display_rows, display_columns, new_windows \
          \from auth_oauth2($1, $2) join users using (uid) \
          \left join moderator using (uid)"

oauth2Check
  :: (HasHasql m, MonadIO m)
  => Provider
  -> Text
  -> m (Either QueryError (Maybe ByteString))
oauth2Check provider token = do
  run $ statement (fromIntegral $ providerOpid provider, token) stmt
  where
    stmt = Statement sql encode decode True
    encode = contrazip2 (EN.param EN.int4) (EN.param EN.text)
    decode = DE.rowMaybe $ (toStrict . Data.Binary.encode <$> DE.column DE.int4)
    sql = "select uid from login_method_oauth2 where \
          \uid is not null and opid = $1 and identification = $2"

instance UserData MyData where
  extractUser MyData{..} = AuthUser
    { name = uname
    , session = toASCIIBytes usession
    , csrfToken = toASCIIBytes ucsrfToken
    }

instance UserData UserWithStats where
  extractUser UserWithStats{..} = AuthUser
    { name = uname user
    , session = toASCIIBytes $ usession user
    , csrfToken = toASCIIBytes $ ucsrfToken user
    }

class HasUserID u where
  extractUid :: u -> UserID

instance HasUserID MyData where
  extractUid u = uid u

instance HasUserID UserWithStats where
  extractUid u = uid $ user u
