{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Health where

import Data.Map.Syntax
import Data.Semigroup
import qualified Data.Text as T
import Heist.Compiled
import Heist.Compiled.Extra
import qualified HTMLEntities.Text as HTML
import Snap.Snaplet.Hasql

import Application
import Piperka.Error.Splices
import Piperka.Maint.Health.Query
import Piperka.Maint.Health.Types
import Piperka.Util (abbrevText, formatTime')

import Crawler.Types

healthCrawler
  :: Splice AppHandler
healthCrawler = eitherDeferMap (const failures) stdSqlErrorSplice
  (manyWithSplices runChildren crawlFailureSplices) (return ())
  where
    failures = run fetchCrawlFailures
    crawlFailureSplices = do
      "cid" ## pureSplice . textSplice $ T.pack . show . cid
      "title" ## pureSplice . textSplice $ HTML.text . title
      "error" ## pureSplice . textSplice $ errMsg . end
      "errorAbbrev" ## pureSplice . textSplice $ abbrevText 100 . errMsg . end
      "hasLastSuccess" ## mayDeferMap (return . lastSuccess) lastSuccessSplice
      "hasOrigin" ## mayDeferMap (return . origin) originSplice
    errMsg (Left msg) = "Other error: " <> (HTML.text $ T.pack msg)
    errMsg (Right (EndError code txt, _)) =
      "HTTP " <> (T.pack $ show code) <> " " <> (HTML.text txt)
    errMsg (Right (EndCrawl (CurlFailure code _), _)) =
      "curl " <> (T.pack $ show code)
    errMsg (Right (EndCrawl FileForbidden, Just (Fetch tgt))) =
      "file " <> (HTML.text $ nextURL tgt)
    errMsg (Right err) = HTML.text $ T.pack $ show err
    lastSuccessSplice = withSplices runChildren $
      "lastSuccess" ## pureSplice . textSplice $ T.pack . formatTime'
    originSplice = withSplices runChildren $ mapV (pureSplice . textSplice) $ do
      "origin" ## id
      "originText" ## HTML.text . abbrevText 100
