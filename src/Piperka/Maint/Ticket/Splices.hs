{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Ticket.Splices (renderTicketDetail, renderTicketList) where

import Control.Error.Util hiding (err)
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import qualified Data.ByteString.Char8 as B
import qualified Data.Map.Strict as M
import Data.Map.Syntax
import Data.Maybe
import Data.Monoid
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8')
import Hasql.Session (QueryError)
import Heist
import Heist.Compiled
import Heist.Compiled.Extra (eitherDeferMap, checkedSplice)
import qualified HTMLEntities.Text as HTML
import Snap

import Application
import Piperka.Error.Splices
import Piperka.Maint.Ticket.Query
import Piperka.Maint.Ticket.Types
import Piperka.Ticket (renderReason)
import Piperka.Util (formatTime')

renderTicketList
  :: RuntimeAppHandler MyData
renderTicketList = const $ eitherDeferMap return stdSqlErrorSplice
  (deferMany runTwice) $ do
  notCid <- isNothing <$> (lift $ getQueryParam "cid")
  isPost <- (== POST) . rqMethod <$> lift getRequest
  case (notCid, isPost) of
    (_, True) -> lift $ runExceptT $ do
      ExceptT saveTicketReply
      Just <$> ExceptT getTickets
    (True, _) -> fmap Just <$> getTickets
    _ -> return $ Right Nothing
  where
    resolvedSplice t = withLocalSplices (resolvedSplices t) mempty runChildren
    resolvedSplices t = do
      "resolved" ## if t then runChildren else return mempty
      "notResolved" ## if t then return mempty else runChildren
    ticketSplices = mapV (pureSplice . textSplice) $ do
      "cid" ## T.pack . show . ticketCid . fst
      "title" ## title . fst
      "stamp" ## T.pack . formatTime' . stamp . fst
      "count" ## T.pack . show . snd
    ticketAttrSplices = do
      "class" ## \n _ -> do
        rm <- removalRequest . fst <$> n
        return $ if rm then [("class", "youCare")] else []
    renderTickets t = withSplices (resolvedSplice t)
      ("comics" ## manyWith runChildren ticketSplices ticketAttrSplices)
    runTwice n = do
      a <- renderTickets False $ fst <$> n
      b <- renderTickets True $ snd <$> n
      return $ a <> b

saveTicketReply
  :: AppHandler (Either QueryError ())
saveTicketReply = do
  params <- getParams
  let getInt n = fmap fst . B.readInt =<< join (listToMaybe <$> M.lookup n params)
      rpy = (,,,)
        <$> getInt "cid"
        <*> ((== ["all"]) <$> M.lookup "to_close" params)
        <*> pure (getInt "to_close")
        <*> (hush . decodeUtf8' =<<
             (join $ listToMaybe <$> M.lookup "resolve_msg" params))
      rpy' = case rpy of
        (Just (c, True, _, m)) -> Just $ Right (c, m)
        (Just (_, False, Just t, m)) -> Just $ Left (t, m)
        _ -> Nothing
  maybe (return $ Right ()) (either saveResolution saveResolutionAll) rpy'

renderTicketDetail
  :: RuntimeAppHandler a
renderTicketDetail = const $
  eitherDeferMap (const $ getDetail) stdSqlErrorSplice
  (deferMany
   (withSplices runChildren splices)) $ return ()
  where
    getDetail = do
      c <- (fmap fst . B.readInt =<<) <$> (lift $ getQueryParam "cid")
      resolved <- (== (Just "1")) <$> (lift $ getParam "resolved")
      runExceptT $ do
        detail <- maybe (return Nothing) (ExceptT . getTicketComicDetail) c
        return $ do
          c' <- c
          detail' <- detail
          return ((resolved, c'), detail')
    splices = do
      "tickets" ## eitherDeferMap (getComicTickets . fst) stdSqlErrorSplice
        (manyWithSplices runChildren detailSplices)
      "notResolved" ## checkedSplice (not . fst . fst)
      (mapV (pureSplice . textSplice) $ do
          "cid" ## T.pack . show . snd . fst
        )
      (mapV (\(f, s) n -> do
                nul <- callTemplate "_null"
                return $ yieldRuntime $
                  f . snd <$> n >>=
                  maybe (codeGen nul) (return . textSplice s)) $ do
          "bookmarkRegexp" ## (bookmarkRegexp, HTML.text)
          "parserType" ## (fmap (T.pack . show . parserType) . crawlParams, id)
          "updateValue" ## (fmap (T.pack . show . updateValue) . crawlParams, id)
          "extraURL" ## (extraURL <=< crawlParams, HTML.text)
          "extraData" ## (extraData <=< crawlParams, HTML.text)
          "updateScore" ## (fmap (T.pack . show) . (updateScore <=< crawlParams), id)
        )

detailSplices
  :: Splices (RuntimeAppHandler TicketDetail)
detailSplices =
  (mapV (pureSplice . textSplice) $ do
      "cid" ## T.pack . show . ticketCid . listTicket
      "tid" ## T.pack . show . ticketTid
      "title" ## title . listTicket
      "stamp" ## T.pack . formatTime' . stamp . listTicket
      "comment" ## HTML.text . maybe "" id . fst . reason
      "uid" ## T.pack . show . reporterUID
  ) <>
  ("resolved" ## mayDeferMap (return . resolution)
   (withSplices runChildren
    (mapV (pureSplice . textSplice) $ do
        "msg" ## HTML.text . fst
        "stamp" ## T.pack . formatTime' . snd
    ))
  ) <>
  ("ticketSubmit" ## deferMap (return . snd . reason) renderReason)
