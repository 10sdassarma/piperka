module Piperka.Maint.Ticket.Types where

import Data.Text (Text)
import Data.Time.Clock

import Application
import Piperka.Ticket.Types

data TicketListTicket = TicketListTicket
  { ticketCid :: Int
  , title :: Text
  , removalRequest :: Bool
  , stamp :: UTCTime
  }

data TicketDetail = TicketDetail
  { ticketTid :: Int
  , reporterUID :: Maybe UserID
  , listTicket :: TicketListTicket
  , reason :: (Maybe Text, Submit)
  , resolution :: Maybe (Text, UTCTime)
  }

data TicketCrawlParams = TicketCrawlParams
  { parserType :: Int
  , updateScore :: Maybe Float
  , updateValue :: Float
  , extraURL :: Maybe Text
  , extraData :: Maybe Text
  }

data TicketComicDetail = TicketComicDetail
  { bookmarkRegexp :: Maybe Text
  , crawlParams :: Maybe TicketCrawlParams
  }
