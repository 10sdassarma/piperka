{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Crawler.Query where

import Control.Applicative
import Control.Monad.Trans.Maybe
import Data.Int
import Data.Text (Text)
import Data.Vector (Vector)

import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement

import Crawler.Types
import Crawler.DB.Fetch (fetchSingle)

-- Everything that isn't in Crawler.Types.CrawlerConfig
data ComicParams = ComicParams
  { cid :: Int32
  , homepage :: Text
  , fixedHead :: Maybe Text
  , parserType :: Int16
  }

-- Using CrawlerConfig gives a bit more data than what is displayed by
-- the splices but it's not worth writing another query over.
fetchComicParams
  :: Int32
  -> Session (Maybe (ComicParams, CrawlerConfig))
fetchComicParams c =
  let decode =
        ComicParams
        <$> pure c
        <*> DE.column DE.text
        <*> DE.nullableColumn DE.text
        <*> DE.column DE.int2
  in runMaybeT $ (,)
  <$> (MaybeT $ statement c $
       Statement sql (EN.param EN.int4) (DE.rowMaybe decode) True)
  <*> (MaybeT $ fetchSingle c)
  where
    sql = "SELECT homepage, fixed_head, parser_type \
          \FROM comics JOIN crawler_config USING (cid) WHERE cid=$1"

data ArchivePage = ArchivePage
  { archiveOrd :: Int
  , archivePage :: Maybe (Text, Text)
  , bookmarks :: Int
  }

fetchArchive
  :: Int32
  -> Session (Vector ArchivePage)
fetchArchive = flip statement $
  Statement sql (EN.param EN.int4) (DE.rowVector decode) True
  where
    decode =
      ArchivePage
      <$> (fromIntegral <$> DE.column DE.int4)
      <*> (liftA (\(a, b) -> (,) <$> a <*> b)
           ((,)
            <$> DE.nullableColumn DE.text
            <*> DE.nullableColumn DE.text))
      <*> (fromIntegral <$> DE.column DE.int4)
    sql = "SELECT ord, name, url_base || name || url_tail, \
          \COALESCE(bookmarks, 0) \
          \FROM updates FULL OUTER JOIN (\
          \ SELECT cid, ord, COUNT(*) AS bookmarks FROM (\
          \  SELECT cid, ord+CASE WHEN subord <= COALESCE(maxsubord, 0) \
          \  THEN 0 ELSE 1 END AS ord FROM subscriptions LEFT JOIN (\
          \   SELECT cid, ord, max(subord) AS maxsubord FROM page_fragments \
          \   GROUP BY cid, ord) AS x USING (cid, ord)) AS y \
          \ GROUP BY cid, ord) AS z USING (cid, ord) JOIN comics USING (cid) \
          \WHERE cid=$1 ORDER BY ord"
