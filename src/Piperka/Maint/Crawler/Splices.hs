{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Crawler.Splices (renderCrawlerControl, renderCrawlerArchive) where

import Control.Error.Util
import Control.Monad.Trans
import Data.Map.Syntax
import Data.Maybe
import Data.Monoid
import qualified Data.Text as T
import qualified Data.Vector as V
import Heist.Compiled
import Heist.Compiled.Extra (eitherDeferMap, emptySplices, someNoneSplices)
import qualified HTMLEntities.Text as HTML
import Snap.Snaplet.Hasql

import Application
import Piperka.Error.Splices
import Piperka.Util (getCid)
import Piperka.Maint.Crawler.Query

import Crawler.Types

renderCrawlerControl
  :: RuntimeAppHandler MyData
renderCrawlerControl _ =
  eitherDeferMap return
  runWithEmptySplices
  (eitherDeferMap (lift . run . fetchComicParams) stdSqlErrorSplice
   (eitherDeferMap (return . note ()) runWithEmptySplices
    (withSplices runChildren splices)
   )
  ) $ note () . fmap (fromIntegral . snd) <$> lift getCid
  where
    runWithEmptySplices = const $
      withLocalSplices (emptySplices configSpliceNames) mempty runChildren
    configSpliceNames = [ "cid"
                        , "parserType"
                        , "urlBase"
                        , "urlTail"
                        , "homepage"
                        , "fixedHead"
                        , "extraURL"
                        , "extraData"
                        ]
    splices = mapV (pureSplice . textSplice) $ do
      "cid" ## T.pack . show . cid . fst
      "homepage" ## HTML.text . homepage . fst
      "fixedHead" ## maybe T.empty HTML.text . fixedHead . fst
      "parserType" ## T.pack . show . parserType . fst
      "urlBase" ## HTML.text . urlBase . snd
      "urlTail" ## HTML.text . urlTail . snd
      "extraURL" ## maybe T.empty HTML.text . extraURL . snd
      "extraData" ## maybe T.empty HTML.text . extraData . snd

renderCrawlerArchive
  :: RuntimeAppHandler MyData
renderCrawlerArchive _ =
  eitherDeferMap return
  runWithEmptySplices
  (eitherDeferMap (lift . run . fetchArchive) stdSqlErrorSplice
   (\n -> withLocalSplices
     ("archiveRows" ## archiveRows n)
     ("currentId" ## currentIdSplice n) runChildren)
  ) $ note () . fmap (fromIntegral . snd) <$> lift getCid
  where
    runWithEmptySplices = const $
      withLocalSplices (emptySplices ["archiveRows"])
      ("currentId" ## const $ return [("id", "tgt-0")]) runChildren
    archiveRows = manyWith runChildren splices attrSplices
    currentIdSplice n = const $ do
      xs <- n
      return [("id", "tgt-" <> (T.pack $ show $ V.length xs - 1))]
    splices = do
      "ifBookmarks" ##
        (mayDeferMap (return . bool Nothing (Just ()) . (0<) . bookmarks)
         (const runChildren))
      "archiveLink" ## \n ->
        (someNoneSplices $ withSplices runChildren $
         mapV (pureSplice . textSplice) $ do
            "name" ## HTML.text . fst
            "href" ## HTML.text . snd) $ archivePage <$> n
      mapV (pureSplice . textSplice) $ do
        "ord" ## T.pack . show . archiveOrd
        "bookmarks" ## T.pack . show . bookmarks
    attrSplices = do
      "id" ## \n t -> do
        ord <- archiveOrd <$> n
        return [("id", t <> (T.pack $ show $ ord))]
      "archiveClass" ## \n -> const $ do
        hasBookmarks <- (0<) . bookmarks <$> n
        isCurrent <- isNothing . archivePage <$> n
        return $ if (hasBookmarks || isCurrent)
          then [("class", foldr1 (\a b -> a <> " " <> b) $
                  (if hasBookmarks then ("has_bookmarks":) else id) .
                  (if isCurrent then ("current":) else id) $ [])]
          else []
