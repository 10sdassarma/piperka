module Piperka.Maint.Health.Types where

import Data.Text (Text)
import Data.Time.Clock

import Crawler.Types

data CrawlFailure = CrawlFailure
  { cid :: Int
  , title :: Text
  , origin :: Maybe Text
  , end :: Either String (CrawlEndCondition, Maybe ParseResult)
  , lastSuccess :: Maybe UTCTime
  }

