{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Health.Query where

import Control.Applicative
import Data.Aeson
import qualified Data.HashMap.Strict as Map
import Data.Time.LocalTime
import Data.Vector (Vector)
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement

import Piperka.Maint.Health.Types

-- Recent failures for crawl attempts with HTTP or curl errors when
-- accessing the archive page stored on Piperka with no recent
-- successes.
fetchCrawlFailures
  :: Session (Vector CrawlFailure)
fetchCrawlFailures = statement () $ Statement sql (EN.unit) (DE.rowVector decoder) True
  where
    decoder = CrawlFailure
      <$> (fromIntegral <$> DE.column DE.int4)
      <*> DE.column DE.text
      <*> DE.nullableColumn DE.text
      <*> (do
              let lk k (Object o) = Map.lookup k o
                  lk _ _ = Nothing
                  res (Error err) = Left err
                  res (Success x) = Right x
              val <- DE.column DE.jsonb
              return $ (,)
                <$> (maybe (Left "end not found in jsonb")
                     (res . fromJSON) . lk "end") val
                <*> (maybe (pure Nothing)
                     (fmap Just . res . fromJSON) . lk "parse") val
          )
      <*> (liftA (fmap $ localTimeToUTC utc) $ DE.nullableColumn DE.timestamp)
    sql = "SELECT cid, title, origin, event, last_success \
          \FROM (SELECT c1.cid, title, c2.event, origin, \
          \ max(c3.stamp) AS last_success FROM \
          \ (SELECT run_id, cid, event->'target'->>'next_url' AS origin, \
          \  rank() OVER (PARTITION BY cid ORDER BY stamp DESC) AS rk \
          \  FROM crawler_log WHERE event_type='target' \
          \  AND STAMP > now() - interval '7 days') AS c1 \
          \ JOIN crawler_log AS c2 USING (run_id, cid) JOIN comics USING (cid) \
          \ LEFT JOIN crawler_log AS c3 ON (c1.cid=c3.cid AND c3.event_type='terminal' \
          \  AND c3.event->'end'->>'type' NOT IN ('http_error', 'crawl') \
          \  AND c3.stamp > now() - interval '2 month') \
          \ LEFT JOIN crawler_log AS c4 ON (c1.cid=c4.cid AND c4.run_id=c1.run_id \
          \  AND c4.event_type='parse')\
          \ WHERE rk=1 AND c2.event_type='terminal' AND c4.cid IS NULL \
          \ AND c2.event->'end'->>'type' IN ('http_error', 'crawl') \
          \ GROUP BY c1.cid, title, origin, c2.event) AS x \
          \WHERE coalesce(last_success < now() - interval '7 days', true)"
