{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Sanitize where

import Control.Monad
import Data.Binary.Builder (toLazyByteString)
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Monoid
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8')
import qualified Data.Text as T
import qualified Text.XmlHtml as X

isValidURL
  :: Text
  -> Bool
isValidURL u = T.take 7 u == "http://" || T.take 8 u == "https://"

sanitize
  :: [Text]
  -> ByteString
  -> Either String Text
sanitize allowed txt =
  X.parseHTML "description" txt >>= sanitize' >>=
  either (Left . show) Right .
  decodeUtf8' . toStrict . toLazyByteString . X.render
  where
    sanitize' :: X.Document -> Either String X.Document
    sanitize' doc = do
      mapM_ isBad $ X.docContent doc
      return doc
    isBad :: X.Node -> Either String ()
    isBad el | X.isElement el = let tag = X.elementTag el in do
                 if tag `elem` allowed
                   then Right ()
                   else Left $ "Unallowed element " <> T.unpack tag
                 if "a" == tag
                   then do
                   v <- case X.elementAttrs el of
                     [("href",v)] -> return v
                     _ -> Left "\"a\" element must have a href attribute and nothing else"
                   when (not $ isValidURL v) $
                     Left $ T.unpack v <> " is not a valid URL"
                   else when (not $ null $ X.elementAttrs el) $
                        Left "Attributes only allowed for \"a\" element"
                 mapM_ isBad $ X.elementChildren el
             | otherwise = Right ()
