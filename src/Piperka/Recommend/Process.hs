{-# LANGUAGE RankNTypes #-}

module Piperka.Recommend.Process where

import Control.Concurrent
import Control.Exception
import Data.Int
import Data.Maybe
import Data.Semigroup
import Data.Vector (Vector, fromList)
import System.IO
import System.Process
import System.Timeout
import Text.Read (readMaybe)

data RecommendHandle = RecommendHandle
  { recommendIn :: Handle
  , recommendOut :: Handle
  , process :: ProcessHandle
  , uniqueAccess :: forall a. IO a -> IO a
  }

startRecommendProcess
  :: IO RecommendHandle
startRecommendProcess = do
  (Just hIn, Just hOut, _, pHandle) <-
    createProcess (System.Process.proc "recommend"
                   []){ std_in = CreatePipe
                      , std_out = CreatePipe
                      , std_err = NoStream
                      }
  hSetBuffering hIn LineBuffering
  uniq <- newEmptyMVar
  return $ RecommendHandle hIn hOut pHandle
    (bracket_ (putMVar uniq ()) (takeMVar uniq))

refreshRecommends
  :: RecommendHandle
  -> IO ()
refreshRecommends h = uniqueAccess h $ hPutStrLn (recommendIn h) "R" >>
                      hGetLine (recommendOut h) >> return ()

getRecommends
  :: RecommendHandle
  -> Int
  -> Int32
  -> IO (Vector Int32)
getRecommends h top uid = uniqueAccess h $ do
  let out = recommendOut h
      payload = show uid <> " " <> show top
  -- It should take about 200ms, 5s is generous
  line <- timeout 5000000 $ hPutStrLn (recommendIn h) payload >> hGetLine out
  case line of
    Nothing -> return mempty
    Just "none" -> return mempty
    Just x -> return $ fromList $ catMaybes $ map readMaybe $ words x
