{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Piperka.Account.Query
  ( getAccountSettings
  , updateUnpriv
  , tryUpdatePriv
  , validateToken
  , validatePriv
  , setOAuth2Login
  , getUserEmail
  ) where

import Control.Applicative
import Control.Error.Util (hush)
import Control.Monad
import Control.Monad.Trans (MonadIO, liftIO)
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Contravariant.Extras.Contrazip
import qualified Data.Configurator as C
import Data.Functor.Contravariant
import Data.Maybe
import Data.Monoid
import qualified Data.Text as T
import Data.Text (Text)
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap.Snaplet
import Snap.Snaplet.Hasql (run, HasHasql)

import Application
import Piperka.OAuth2.Types
import Piperka.Account.Types
import Piperka.Listing.Types (columnsToInt)
import Piperka.Profile.Types (intToPrivacy, privacyToInt)

getAccountSettings
  :: UserID
  -> AppHandler (Either QueryError AccountData)
getAccountSettings u = do
  cfg <- getSnapletUserConfig
  prov :: [(Int, Maybe Text -> ProviderData)] <- catMaybes <$>
    (liftIO $ mapM
     (\n -> runMaybeT $ do
         o <- MaybeT $ C.lookup cfg ("oauth2." <> n <> ".opid")
         l <- MaybeT $ C.lookup cfg ("oauth2." <> n <> ".label")
         return (o, ProviderData n l)
     ) =<< C.lookupDefault [] cfg "oauth2.providers")
  runExceptT $ do
    res1 <- ExceptT $ run $ statement u stmt1
    ids <- ExceptT $ run $ statement u stmt2
    return $ AccountData res1 $ map (\(i,p) -> p $ lookup i ids) prov
  where
    stmt1 = Statement sql1 encode (DE.singleRow decode1) True
    stmt2 = Statement sql2 encode (DE.rowList decode2) True
    encode = EN.param EN.int4
    decode1 = UserAccountSettings
              <$> (liftA intToPrivacy $ DE.column DE.int4)
              <*> DE.column DE.bool
              <*> DE.nullableColumn DE.text
              <*> DE.nullableColumn DE.text
              <*> (BookmarkOptions
                   <$> DE.column DE.int4
                   <*> DE.column DE.bool
                   <*> DE.column DE.bool)
    decode2 = (,)
              <$> (liftA fromIntegral $ DE.column DE.int4)
              <*> DE.column DE.text
    sql1 = "SELECT privacy, \
           \uid IN (SELECT uid FROM login_method_passwd WHERE uid IS NOT NULL), \
           \email, writeup, bookmark_sort, offset_bookmark_by_one, hold_bookmark \
           \FROM users WHERE uid=$1"
    sql2 = "SELECT opid, identification FROM login_method_oauth2 WHERE uid=$1"

checkPassword
  :: Statement (UserID, Text) Bool
checkPassword = Statement sql encode (DE.singleRow $ DE.column DE.bool) True
  where
    encode = contrazip2 (EN.param EN.int4) (EN.param EN.text)
    sql = "SELECT (hash = crypt($2, hash)) AS pwmatch \
          \FROM users JOIN login_method_passwd USING (uid) WHERE uid=$1"

updateUnpriv
  :: (HasHasql m, MonadIO m)
  => UserID
  -> AccountUpdate
  -> m (Either QueryError ())
updateUnpriv u a = run $ statement (u, a) stmt
  where
    stmt = Statement sql encode DE.unit True
    encode = contrazip2 (EN.param EN.int4) $ mconcat
             [ newWindows >$< EN.param EN.bool
             , rows' >$< EN.param EN.int4
             , columnsToInt . columns' >$< EN.param EN.int4
             , holdBookmark . bookmarkSettings' >$< EN.param EN.bool
             , bookmarkSort . bookmarkSettings' >$< EN.param EN.int4
             , offsetMode . bookmarkSettings' >$< EN.param EN.bool
             ]
    sql = "UPDATE users SET new_windows=$2, display_rows=$3, \
          \display_columns=$4, hold_bookmark=$5, bookmark_sort=$6, \
          \offset_bookmark_by_one=$7 WHERE uid=$1"


updatePriv
  :: Statement (UserID, PrivData) ()
updatePriv = Statement sql encode DE.unit True
  where
    scrub = (=<<) (\x -> if T.null x then Nothing else Just x)
    encode = contrazip2 (EN.param EN.int4)
             ((contramap (scrub . email') (EN.nullableParam EN.text)) <>
              (contramap (privacyToInt . privacy') (EN.param EN.int4)) <>
              (contramap (scrub . writeup') (EN.nullableParam EN.text)))
    sql = "UPDATE users SET email=$2, privacy=$3, writeup=$4 WHERE uid=$1"

updatePassword
  :: Statement (UserID, PrivData) ()
updatePassword = Statement sql encode DE.unit True
  where
    encode = contrazip2 (EN.param EN.int4)
      (contramap (fromJust . newPassword) (EN.param EN.text))
    sql = "SELECT auth_create_password($2, $1)"

deletePassword
  :: Statement UserID ()
deletePassword = Statement sql (EN.param EN.int4) DE.unit True
  where
    sql = "DELETE FROM login_method_passwd WHERE uid=$1"

setOAuth2Login
  :: Statement (UserID, Provider, Text) ()
setOAuth2Login = Statement sql encode DE.unit True
  where
    encode = contrazip3 (EN.param EN.int4)
      (fromIntegral . providerOpid >$< (EN.param EN.int4))
      (EN.param EN.text)
    sql = "INSERT INTO login_method_oauth2 (uid, opid, identification) VALUES \
          \($1, $2, $3) ON CONFLICT (uid, opid) DO NOTHING"

deleteOAuth2Login
  :: Statement (UserID, [Provider]) ()
deleteOAuth2Login = Statement sql encode DE.unit True
  where
    encode = contrazip2 (EN.param EN.int4)
      (EN.param (map (fromIntegral . providerOpid) >$<
                 (EN.array $ EN.dimension foldl $ EN.element EN.int4)))
    sql = "DELETE FROM login_method_oauth2 WHERE uid=$1 AND opid = ANY ($2 :: int[])"

validateToken
  :: (HasHasql m, MonadIO m)
  => UserID
  -> Provider
  -> Text
  -> m (Either QueryError Bool)
validateToken u p t = run $ statement (u, p, t) stmt
  where
    stmt = Statement sql encode (DE.singleRow $ DE.column DE.bool) True
    encode = contrazip3 (EN.param EN.int4)
      (EN.param $ (fromIntegral . providerOpid) >$< EN.int4)
      (EN.param EN.text)
    sql = "SELECT $1 IN (SELECT uid FROM users \
          \JOIN login_method_oauth2 USING (uid) \
          \WHERE opid=$2 AND identification=$3"

validatePriv
  :: (HasHasql m, MonadIO m)
  => UserID
  -> PrivData
  -> ValidateMethod
  -> m (Either (Either AccountUpdateError NeedsValidation) ())
validatePriv u a val = runExceptT $ do
  let pwFail = case a of
        (UpdateAccount n n' _ _ _ _ _) -> let
          pwChange = maybe False (not . T.null) n
          pwMismatch = n /= n'
          in pwChange && pwMismatch
        _ -> False
  case (val, pwFail) of
    (_, True) -> throwE $ Left AccountNewPasswordMismatch
    (OAuth2 provider, _) -> throwE $ Right $ NeedsValidation provider a
    (Password p, _) ->
      if T.null p then throwE $ Left AccountPasswordMissing else do
        pwOk <- withExceptT (Left . AccountSqlError) $
          ExceptT $ run $ statement (u, p) checkPassword
        if pwOk then return () else throwE $ Left AccountPasswordWrong
    (Trusted, _) -> return ()

tryUpdatePriv
  :: (HasHasql m, MonadIO m)
  => UserID
  -> PrivData
  -> m (Either QueryError ())
tryUpdatePriv u a@(UpdateAccount _ _ _ _ _ _ _) = run $ do
  if passwordless a
    then statement u deletePassword
    else when (maybe False (not . T.null) $ newPassword a) $
         statement (u, a) updatePassword
  when (not $ null $ oauth2Removes a) $
    statement (u, oauth2Removes a) deleteOAuth2Login
  statement (u, a) updatePriv

tryUpdatePriv u (AttachProvider provider token) =
  run $ statement (u, providerOpid provider, token) stmt
  where
    stmt :: Statement (UserID, AuthID, Text) ()
    stmt = Statement sql encode (DE.unit) True
    encode = contrazip3 (EN.param EN.int4)
      (EN.param EN.int4) (EN.param EN.text)
    sql = "INSERT INTO login_method_oauth2 \
          \(opid, identification, uid) VALUES ($2, $3, $1)"

getUserEmail
  :: (HasHasql m, MonadIO m)
  => UserID
  -> m Text
getUserEmail = (fromMaybe "" . hush <$>) . run . flip statement stmt
  where
    stmt = Statement sql (EN.param EN.int4) (DE.singleRow $ DE.column DE.text) False
    sql = "SELECT email FROM users WHERE uid=$1"
