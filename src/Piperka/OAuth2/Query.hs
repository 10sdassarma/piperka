{-# LANGUAGE OverloadedStrings #-}

module Piperka.OAuth2.Query where

import Contravariant.Extras.Contrazip
import Control.Monad.Trans
import Data.Functor.Contravariant
import Data.Text (Text)
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap.Snaplet.Hasql (run, HasHasql)

import Piperka.OAuth2.Types

reserveOAuth2Identity
  :: (HasHasql m, MonadIO m)
  => Provider
  -> Text
  -> m (Either QueryError AuthID)
reserveOAuth2Identity provider token = run $ statement (provider, token) stmt
  where
    stmt = Statement sql encode (DE.singleRow decode) True
    encode = contrazip2
      (EN.param $ (fromIntegral . providerOpid) >$< EN.int4)
      (EN.param EN.text)
    decode = DE.column DE.int4
    sql = "INSERT INTO login_method_oauth2 (opid, identification) \
          \VALUES ($1, $2) RETURNING lmid"
