{-# LANGUAGE OverloadedStrings #-}

module Piperka.Update.Statements (updateOptionsFetch, updateAndRedirect) where

import Contravariant.Extras.Contrazip
import Control.Applicative
import Data.ByteString (ByteString)
import Data.Int
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (sql)
import qualified Hasql.Session as S
import Hasql.Statement
import Network.IP.Addr
import Prelude hiding (Ordering)

import Application hiding (uid)
import Piperka.Update.Types
import Piperka.Listing.Types.Ordering

updateOrderingDecode :: Int32 -> Ordering
updateOrderingDecode 0 = UserUpdatesDesc
updateOrderingDecode 1 = UserUpdates
updateOrderingDecode 2 = TitleAsc
updateOrderingDecode 3 = UpdateDesc
updateOrderingDecode _ = UpdateAsc

updateOptionsRow :: DE.Row UpdateOptions
updateOptionsRow =
  UpdateOptions
  <$> DE.column DE.int4
  <*> (liftA updateOrderingDecode $ DE.column DE.int4)
  <*> DE.column DE.bool
  <*> DE.column DE.bool

updateOptionsFetch
  :: Statement UserID UpdateOptions
updateOptionsFetch = Statement sql encode (DE.singleRow updateOptionsRow) True
  where
    sql = "SELECT (SELECT COUNT(*) FROM comic_remain_frag(users.uid) WHERE num > 0), \
          \bookmark_sort, offset_bookmark_by_one, hold_bookmark \
          \FROM users WHERE uid=$1"
    encode = EN.param EN.int4

updateAndRedirect
  :: UserID
  -> Int32
  -> Bool
  -> NetAddr IP
  -> Session (Maybe ByteString)
updateAndRedirect uid cid back ip = do
  S.sql "COMMIT"
  S.sql "BEGIN"
  statement uid $ Statement "SELECT pg_advisory_xact_lock(0,$1)"
    (EN.param EN.int4) DE.unit True
  statement (uid, cid, back, ip) $
    Statement sql encode (DE.rowMaybe $ DE.column DE.bytea) True
  where
    encode = contrazip4
      (EN.param EN.int4)
      (EN.param EN.int4)
      (EN.param EN.bool)
      (EN.param EN.inet)
    sql = "WITH page AS (\
          \SELECT COALESCE(url, fixed_head, homepage) AS url, \
          \last_ord AS ord, last_subord AS subord FROM \
          \redir_url_and_last($1, $2, CASE WHEN $3 THEN -1 ELSE 0 END) \
          \CROSS JOIN comics WHERE cid=$2), \
          \history AS (\
          \INSERT INTO redirect_log \
          \(uid, host, cid, ord, subord, max_ord, offset_back, url) SELECT \
          \$1, $4, $2, page.ord, page.subord, \
          \(SELECT max(ord) FROM updates WHERE cid=$2), $3, page.url FROM page), \
          \deleted_recent AS (\
          \DELETE FROM recent WHERE uid=$1 AND cid=$2), r AS (\
          \INSERT INTO recent (uid, cid, ord, subord, used_on) SELECT \
          \$1, $2, page.ord, page.subord, now() FROM page), upd AS (\
          \UPDATE subscriptions SET ord=max_ord_of, subord=\
          \COALESCE((SELECT MAX(subord)+1 FROM page_fragments WHERE cid=\
          \subscriptions.cid AND ord=max_ord_of), 1) FROM max_ord_of($2) \
          \WHERE uid=$1 AND cid=$2) \
          \SELECT url FROM page"
