{-# LANGUAGE OverloadedStrings #-}

module Piperka.Profile.Statements where

import Contravariant.Extras.Contrazip
import Control.Applicative
import Control.Monad.IO.Class
import Data.Int
import Data.Text (Text)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap.Snaplet.Hasql

import Application
import Piperka.Profile.Types

decodeProfile :: DE.Row CommonProfile
decodeProfile =
  CommonProfile
  <$> DE.column DE.int4
  <*> DE.column DE.text
  <*> DE.column DE.bool
  <*> (liftA intToPrivacy $ DE.column DE.int4)
  <*> DE.nullableColumn DE.text
  <*> DE.column DE.int4
  <*> DE.column DE.int4

decodeOwnProfile :: DE.Row OwnProfile
decodeOwnProfile =
  OwnProfile
  <$> DE.column DE.int4
  <*> DE.column DE.int4
  <*> decodeProfile

decodeOtherProfile :: DE.Row OtherProfile
decodeOtherProfile =
  OtherProfile
  <$> (liftA intToPrivacy $ DE.column DE.int4)
  <*> DE.column DE.bool
  <*> DE.column DE.bool
  <*> DE.column DE.bool
  <*> decodeProfile

profileDataFetch :: Statement Text (Maybe CommonProfile)
profileDataFetch = Statement sql encode (DE.rowMaybe decodeProfile) True
  where
    encode = EN.param EN.text
    sql = "SELECT uid, name, perm, privacy, writeup, (totals).\"1\", (totals).\"2\" FROM \
          \(SELECT uid, name, privacy=3 AS perm, privacy, writeup, \
          \(SELECT CAST((COALESCE(SUM(ord), 0), COUNT(*)) AS integerpair) \
          \ FROM subscriptions WHERE uid=users.uid) AS totals \
          \FROM users WHERE LOWER(name)=LOWER($1)) AS x"

profileOwnDataFetch :: Statement Int32 OwnProfile
profileOwnDataFetch = Statement sql encode (DE.singleRow decodeOwnProfile) True
  where
    encode = EN.param EN.int4
    sql = "SELECT followers, followees, \
          \uid, name, perm, privacy, writeup, (totals).\"1\", (totals).\"2\" FROM \
          \(SELECT cast((SELECT COUNT(*) FROM follower JOIN users AS u USING (uid) \
          \ WHERE privacy > 1 AND follower.interest=users.uid) as integer) AS followers, \
          \cast((SELECT COUNT(*) FROM follower JOIN users AS u ON interest=u.uid \
          \ WHERE privacy > 1 AND follower.uid=users.uid) as integer) AS followees, \
          \uid, name, true AS perm, privacy, writeup, \
          \(SELECT CAST((COALESCE(SUM(ord),0), COUNT(*)) AS integerpair) \
          \ AS totals FROM subscriptions WHERE uid=users.uid) \
          \FROM users WHERE uid=$1) AS x"

profileOtherDataFetch :: Statement (Text, Int32) (Maybe OtherProfile)
profileOtherDataFetch = Statement sql encode (DE.rowMaybe decodeOtherProfile) True
  where
    encode = contrazip2 (EN.param EN.text) (EN.param EN.int4)
    sql = "SELECT rprivacy, rperm, titfortat, interest, \
          \uid, name, COALESCE(perm, false), privacy, writeup, \
          \(totals).\"1\", (totals).\"2\" FROM \
          \(SELECT me.privacy AS rprivacy, \
          \(me.uid, them.uid) IN (SELECT uid, followee \
          \ FROM follow_permission) AS rperm, \
          \me.privacy=1 AND them.privacy=2 AS titfortat, \
          \(me.uid, them.uid) IN (SELECT uid, interest \
          \ FROM follower) AS interest, \
          \them.uid AS uid, them.name, \
          \CASE them.privacy WHEN 3 THEN true WHEN 2 THEN \
          \ CASE me.privacy WHEN 1 THEN false ELSE \
          \ (them.uid, me.uid) IN (SELECT uid, followee \
          \ FROM follow_permission) END \
          \END AS perm, them.privacy, them.writeup, \
          \(SELECT CAST((COALESCE(SUM(ord),0), COUNT(*)) AS integerpair) \
          \AS totals FROM subscriptions WHERE uid=them.uid) \
          \FROM users AS them, users AS me \
          \WHERE LOWER(them.name)=LOWER($1) AND me.uid=$2) AS x"

getFollowers
  :: (HasHasql m, MonadIO m)
  => UserID
  -> m (Either QueryError Followers)
getFollowers u = run $
  Followers
  <$> (statement u $
       Statement "SELECT privacy FROM users WHERE uid=$1"
       (EN.param EN.int4)
       (DE.singleRow $ intToPrivacy <$> DE.column DE.int4) True)
  <*> (statement u $
       Statement sql1
       (EN.param EN.int4)
       (DE.rowVector $ (,,,)
        <$> DE.column DE.text
        <*> (intToPrivacy <$> DE.column DE.int4)
        <*> DE.column DE.bool
        <*> DE.column DE.bool
       ) True)
  <*> (statement u $
       Statement sql2
       (EN.param EN.int4)
       (DE.rowVector $ (,,)
        <$> DE.column DE.text
        <*> (intToPrivacy <$> DE.column DE.int4)
        <*> DE.column DE.bool
       ) True)
  where
    sql1 = "SELECT name, privacy, interest IS NOT NULL, permit IS NOT NULL \
           \FROM users JOIN (follower FULL OUTER JOIN \
           \(SELECT followee AS uid, uid AS permit FROM follow_permission) AS permit \
           \ON follower.uid=permit.uid AND interest=permit) ON users.uid=follower.uid \
           \WHERE COALESCE(interest, permit)=$1 AND \
           \(privacy > 1 OR permit IS NOT NULL) ORDER BY name DESC"
    sql2 = "SELECT name, privacy, followee IS NOT NULL \
           \FROM users JOIN follower ON users.uid=interest \
           \LEFT JOIN follow_permission ON interest=follow_permission.uid AND \
           \followee=follower.uid WHERE follower.uid=$1 AND \
           \privacy > 1 ORDER BY name DESC"
