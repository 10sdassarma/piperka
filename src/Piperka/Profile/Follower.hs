{-# LANGUAGE OverloadedStrings #-}

module Piperka.Profile.Follower (renderFollowers) where

import Control.Error.Util (bool)
import Data.ByteString.Builder (byteString)
import Data.Map.Syntax
import Data.Maybe
import Data.Monoid
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding
import qualified Data.Vector as V
import Heist
import Heist.Compiled
import Heist.Compiled.Extra (eitherDeferMap, checkedSplice)
import qualified HTMLEntities.Text as HTML
import Snap.Core
import Text.XmlHtml as X

import Application
import Piperka.Error.Splices
import Piperka.Profile.Types hiding (uid)
import Piperka.Profile.Statements

renderFollowers
  :: RuntimeAppHandler MyData
renderFollowers = eitherDeferMap (getFollowers . uid) stdSqlErrorSplice
  (withSplices runChildren followersSplices)

followersSplices
  :: Splices (RuntimeAppHandler Followers)
followersSplices = do
  "haveFollowers" ## checkedSplice $ not . V.null . myFollowers
  "isFollowing" ## checkedSplice $ not . V.null . myFollowees
  "privacy" ## \n -> do
    level <- read . T.unpack . fromJust . lookup "level" . X.elementAttrs <$>
      getParamNode
    x <- runChildren
    return $ yieldRuntime $ do
      p <- priv <$> n
      if p == level then codeGen x else return mempty
  "followers" ## manyWith runChildren followerSplices followerTextSplices .
    fmap (V.indexed . myFollowers)
  "followees" ## deferMany (withSplices runChildren followeeSplices) .
    fmap myFollowees

followerSplices
  :: Splices (RuntimeAppHandler (Int, (Text, Privacy, Bool, Bool)))
followerSplices = ("i" ## pureSplice . textSplice $ T.pack . show . fst) <>
  (mapV (. fmap snd) $ do
      "followerPublic" ## checkedSplice $ (< Private) . \(_,x,_,_) -> x
      "followerPrivate" ## checkedSplice $ (== Private) . \(_,x,_,_) -> x
      "followerURL" ## pureSplice $ byteString . urlEncode . encodeUtf8 . (\(x,_,_,_) -> x)
      "followerName" ## pureSplice . textSplice $ HTML.text . (\(x,_,_,_) -> x)
      "activeFollower" ## checkedSplice (\(_,_,x,_) -> x)
  )

followerTextSplices
  :: Splices (RuntimeSplice AppHandler (Int, (Text, Privacy, Bool, Bool)) -> AttrSplice AppHandler)
followerTextSplices = "checked" ## \n _ ->
  bool [] [("checked", "")] . (\(_,_,_,x) -> x) . snd <$> n

followeeSplices
  :: Splices (RuntimeAppHandler (Text, Privacy, Bool))
followeeSplices = do
  "followeeURL" ## pureSplice $ byteString . urlEncode . encodeUtf8 . (\(x,_,_) -> x)
  "followeeName" ## pureSplice . textSplice $ HTML.text . (\(x,_,_) -> x)
  "ifPending" ## checkedSplice $ \(_,p,allow) -> p == Friends && not allow
