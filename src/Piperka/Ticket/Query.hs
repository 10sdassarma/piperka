{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Ticket.Query where

import Contravariant.Extras.Contrazip
import Control.Monad.Trans
import Data.Aeson (toJSON)
import Data.Functor.Contravariant
import Data.Monoid
import Data.Text (Text)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap.Snaplet.Hasql

import Application
import Piperka.Ticket.Types

saveTicket
  :: (HasHasql m, MonadIO m)
  => Maybe UserID
  -> (Int, Maybe Text, Submit)
  -> m (Either QueryError ())
saveTicket u t = run $ statement (u, t) $ Statement sql encode DE.unit True
  where
    encode = contrazip2 (EN.nullableParam EN.int4)
      ((fromIntegral . (\(x,_,_) -> x) >$< EN.param EN.int4) <>
       (toJSON . (\(_,x,y) -> (x,y)) >$< EN.param EN.jsonb))
    sql = "INSERT INTO ticket (cid, uid, message) VALUES ($2, $1, $3)"

getTicketInfo
  :: (HasHasql m, MonadIO m)
  => Int
  -> m (Either QueryError (Maybe (Text, TicketInfo)))
getTicketInfo c = run $ statement c $ Statement sql
  (fromIntegral >$< EN.param EN.int4) (DE.rowMaybe decode) True
  where
    decode = do
      dead <- DE.column DE.bool
      tl <- DE.column DE.text
      tick <- TicketInfo
              <$> DE.nullableColumn DE.text
              <*> DE.nullableColumn DE.text
              <*> DE.column DE.text
      (tl,) <$> if dead then DeadTicketInfo <$> DE.column DE.text else return tick
    sql = "SELECT dead, title, firstpage, lastpage, homepage, reason FROM (\
          \SELECT false AS dead, cid, title, homepage, '' AS reason, \
          \(SELECT url_base||name||url_tail FROM updates \
          \WHERE cid=$1 AND ord=0 LIMIT 1) AS firstpage, \
          \(SELECT url_base||name||url_tail FROM updates \
          \WHERE cid=$1 AND name IS NOT NULL ORDER BY ord DESC LIMIT 1) AS lastpage \
          \FROM comics \
          \UNION \
          \SELECT true AS dead, cid, title, homepage, reason, \
          \null AS firstpage, null AS lastpage FROM graveyard) AS x \
          \WHERE cid=$1 LIMIT 1"
