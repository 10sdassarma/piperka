{-# LANGUAGE OverloadedStrings #-}

module Piperka.Ticket.User.Query where

import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Data.Maybe
import Data.Time.LocalTime
import Data.Vector (Vector, fromList)
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap.Snaplet.Hasql

import Application (UserID)
import Piperka.Maint.Ticket.Query (decodeReason)
import Piperka.Ticket.User.Types

getUserTickets
  :: (HasHasql m, MonadIO m)
  => UserID
  -> m (Either QueryError (Maybe (Vector Ticket, Vector Resolved)))
getUserTickets uid = run $ do
  tck <- fromList . catMaybes <$>
         (statement uid $
          Statement sql1 (EN.param EN.int4)
          (DE.rowList decodeTicket) True)
  res <- fromList . catMaybes <$>
         (statement uid $
          Statement sql2 (EN.param EN.int4)
          (DE.rowList decodeResolved) True)
  return $ guard (not $ null tck && null res) >> return (tck, res)
  where
    decodeTicket = runMaybeT $ decodeTicket'
    decodeTicket' = Ticket
      <$> (lift (fromIntegral <$> DE.column DE.int4))
      <*> (lift $ DE.column DE.text)
      <*> (lift $ localTimeToUTC utc <$> DE.column DE.timestamp)
      <*> decodeReason
    decodeResolved = runMaybeT $ Resolved
      <$> decodeTicket'
      <*> (lift $ localTimeToUTC utc <$> DE.column DE.timestamp)
      <*> (lift $ DE.column DE.text)
    sql1 = "SELECT cid, title, ticket.stamp, message \
           \FROM ticket JOIN comics USING (cid) \
           \WHERE uid=$1 AND tid NOT IN (SELECT tid FROM ticket_resolve) \
           \ORDER BY stamp DESC"
    sql2 = "SELECT cid, title, ticket.stamp, message, \
           \ticket_resolve.stamp, ticket_resolve.msg \
           \FROM ticket JOIN comics USING (cid) JOIN ticket_resolve USING (tid) \
           \WHERE uid=$1 ORDER BY ticket.stamp DESC"
