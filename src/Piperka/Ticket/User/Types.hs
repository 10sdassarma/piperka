module Piperka.Ticket.User.Types where

import Data.Text (Text)
import Data.Time.Clock (UTCTime)

import Piperka.Ticket.Types

data Ticket = Ticket
  { cid :: Int
  , name :: Text
  , stamp :: UTCTime
  , reason :: (Maybe Text, Submit)
  }

data Resolved = Resolved
  { ticket :: Ticket
  , resolvedStamp :: UTCTime
  , resolvedMessage :: Text
  }
