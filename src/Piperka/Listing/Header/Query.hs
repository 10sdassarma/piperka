{-# LANGUAGE OverloadedStrings #-}

module Piperka.Listing.Header.Query (alphabetIndex) where

import Control.Monad.Trans
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap.Snaplet.Hasql

alphabetIndex
  :: (HasHasql m, MonadIO m)
  => m (Either QueryError (Vector (Text, Int)))
alphabetIndex = run $ statement () stmt
  where
    stmt = Statement sql (EN.unit) (DE.rowVector decode) True
    decode = (,)
             <$> DE.column DE.text
             <*> (fromIntegral <$> DE.column DE.int4)
    sql = "SELECT letter, ord FROM alphabet_index ORDER BY letter"
