{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Listing.Render (renderListing) where

import Control.Lens (view)
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import qualified Data.ByteString.Char8 as B (unpack, readInt)
import Data.Map.Syntax
import Data.Maybe
import Data.Text (unpack)
import Heist
import Heist.Compiled as C
import Heist.Compiled.Extra as C
import Text.XmlHtml
import Data.Monoid
import Snap
import Data.Text.Encoding (encodeUtf8)

import Application hiding (prefs)
import Piperka.Error.Splices
import Piperka.Listing.Types hiding (listing', listing'')
import qualified Piperka.Listing.Types.Ordering as L (Ordering(..), orderingToText)
import Piperka.Listing.Query (getListing)
import Piperka.Listing.Statements (parseOrdering)
import Piperka.Listing.Header.Splices
import Piperka.Listing.Column.Splices
import Piperka.Profile.Types (profile, perm)


success
  :: ListingMode
  -> RuntimeAppHandler ((UserPrefs, Int, ListingParam, Maybe NavigateData), Bool)
success mode n = do
  lst <- withSplices (callTemplate "_listing")
         (listingColumnSplices mode) $
         (\x -> (if snd x then \(a,b,c,_) -> (a,b,c,Nothing) else id) $ fst x) <$> n
  hd <- withLocalSplices
        (mapV ($ (((\(_, _, x, _) -> x) . fst <$> n))) $
         listingHeaderSplices mode)
        listingHeaderAttrSplices runChildren
  return $ yieldRuntime $ do
    useMinimal <- snd <$> n
    lst' <- case mode of
      Profile -> do
        havePerm <- perm . profile . getProfile .
                    (\(_, _, x, _) -> x) . fst <$> n
        return $ if havePerm then lst else mempty
      _ -> return lst
    codeGen $ if useMinimal then lst' else hd <> lst'

renderListing
  :: RuntimeAppHandler (Maybe MyData)
renderListing runtime = do
  mode <- read . unpack . fromJust . getAttribute "mode" <$> getParamNode

  let failure action = do
        missing <- runMaybeT
                   (MaybeT (return $ case mode of
                             Update -> Just "_updateMissing"
                             Profile -> Just "_profileMissing"
                             _ -> Nothing)
                    >>= lift . C.callTemplate)
        sqlErr <- C.deferMap (\(SqlError err) -> return err)
                  (C.withSplices (C.callTemplate "_sqlErr") sqlErrorSplices) action
        C.bindLater (\val -> case val of
                      Missing -> C.codeGen $ fromJust missing
                      SqlError _ -> C.codeGen sqlErr) action

      addSort ord (p, q) = (p, q ++ [("sort", Just $ encodeUtf8 $ L.orderingToText ord)])

      getListingData usr = do
        let prefs = getPrefs usr
        (ord, paramOrd) <- case mode of
          Top -> return (L.TopDesc, Nothing)
          Graveyard -> return (L.TitleAsc, Nothing)
          -- Update ord will get overridden by user setting from DB.
          Update -> return (L.TitleAsc, Nothing)
          -- Ignored
          Recommend -> return (L.TitleAsc, Nothing)
          _ -> do
            paramOrd <- lift $ fmap (parseOrdering . B.unpack) <$> getParam "sort"
            return $ maybe (L.TitleAsc, Nothing) ((,) <$> id <*> Just) paramOrd
        useMinimal <- lift $ withTop' id $ view minimal :: RuntimeSplice AppHandler Bool
        offset <- lift $ (fromIntegral . maybe 0 ((maybe 0 (max 0 . fst)) . B.readInt))
                  <$> getParam "offset"
        let limit = (rows prefs) * (columnsToInt $ columns prefs)
        lst <- lift $ getListing mode ord offset limit (((,) <$> uid <*> uname)
                                                 <$> usr)
        let makeResult param =
              let pathQuery = maybe id addSort paramOrd $ getListingPathQuery mode param
                  tot = extractTotal param
                  navParams = if useMinimal || tot <= limit
                              then Nothing
                              else Just (pathQuery, fromIntegral offset,
                                         fromIntegral limit, fromIntegral tot)
              in ((prefs, fromIntegral offset, param, navParams), useMinimal)
        return $ fmap makeResult lst

  C.eitherDeferMap getListingData failure (success mode) runtime
