{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Piperka.Listing.Types where

import Network.HTTP.Types.URI (Query)
import Data.Binary
import Data.Int
import Data.Text (Text)
import Data.Vector hiding (elem)
import GHC.Generics (Generic)
import Prelude hiding (Ordering)
import Hasql.Session (QueryError)
import Data.Text.Encoding (encodeUtf8)

import Piperka.Profile.Types
  (Profile, CommonProfile, profile, name, inComics)
import Piperka.Update.Types (UpdateOptions)
import qualified Piperka.Update.Types (total)
import Piperka.Profile.Types (Profile(..))

data UpdateFreq = UpdateHigh | UpdateMid | UpdateLow
  deriving (Show, Eq, Read)

data ListingItem = ListingItem
  { cid :: Int32
  , title :: Text
  , updateFreq :: Maybe UpdateFreq
  } deriving (Show, Eq)

data UserListingItem = UserListingItem
  { subs :: Bool
  , newComic :: Bool
  , friends :: Bool
  , listing' :: ListingItem
  } deriving (Show, Eq)

data UpdateListingItem = UpdateListingItem
  { new :: Int32
  , nsfw :: Bool
  , directLink :: Maybe Text
  , listing'' :: ListingItem
  } deriving (Show, Eq)

data ViewColumns = OneColumn | TwoColumn | ThreeColumn
                 deriving (Show, Eq, Ord, Read, Generic)

instance Binary ViewColumns

data ListingError = Missing | SqlError QueryError
                  deriving (Show, Eq)

columnsToInt :: (Num n) => ViewColumns -> n
columnsToInt OneColumn = 1
columnsToInt TwoColumn = 2
columnsToInt ThreeColumn = 3

intToColumns :: (Num a, Eq a) => a -> ViewColumns
intToColumns 1 = OneColumn
intToColumns 3 = ThreeColumn
intToColumns _ = TwoColumn

data ListingMode = Top | Browse | Profile | Update | Graveyard | Recommend
                 deriving (Show, Read, Eq)

data ListingItemMode = ListingMode | UserMode | UpdateMode
                     deriving (Show, Read, Eq)

data ListingParam =
  ListingParam Bool Int (Vector ListingItem) |
  UserParam Bool Int (Vector UserListingItem) |
  UpdateParam UpdateOptions (Vector UpdateListingItem) |
  ProfileParam CommonProfile (Vector ListingItem) |
  UserProfileParam Profile (Vector UserListingItem) |
  GraveyardParam Int (Vector ListingItem)
  deriving (Show, Eq)

extractListing :: ListingParam -> Maybe (Vector ListingItem)
extractListing (ListingParam _ _ x) = Just x
extractListing (ProfileParam _ x) = Just x
extractListing (GraveyardParam _ x) = Just x
extractListing _ = Nothing

extractUserListing :: ListingParam -> Maybe (Vector UserListingItem)
extractUserListing (UserParam _ _ x) = Just x
extractUserListing (UserProfileParam _ x) = Just x
extractUserListing _ = Nothing

extractUpdateListing :: ListingParam -> Maybe (Vector UpdateListingItem)
extractUpdateListing (UpdateParam _ x) = Just x
extractUpdateListing _ = Nothing

extractTotal :: (Integral n) => ListingParam -> n
extractTotal (ListingParam _ x _) = fromIntegral x
extractTotal (UserParam _ x _) = fromIntegral x
extractTotal (UpdateParam x _) = fromIntegral $ Piperka.Update.Types.total x
extractTotal (ProfileParam x _) = fromIntegral $ inComics x
extractTotal (UserProfileParam x _) = fromIntegral $ inComics $ profile x
extractTotal (GraveyardParam x _) = fromIntegral x

getUpdateParam :: ListingParam -> UpdateOptions
getUpdateParam (UpdateParam o _) = o
getUpdateParam _ = error "not possible"

getProfile :: ListingParam -> Profile
getProfile (ProfileParam x _) = Common x
getProfile (UserProfileParam x _) = x
getProfile _ = undefined

listingModeSubscribes :: ListingMode -> Bool
listingModeSubscribes = flip elem [Top, Browse, Profile, Recommend]

getListingPageName :: ListingMode -> Text
getListingPageName Top = "top.html"
getListingPageName Browse = "browse.html"
getListingPageName Profile = "profile.html"
getListingPageName Update = "updates.html"
getListingPageName Recommend = "recommend.html"
getListingPageName Graveyard = "graveyard.html"

getListingPathQuery :: ListingMode -> ListingParam -> ([Text], Query)
getListingPathQuery Top _ = (["top.html"], [])
getListingPathQuery Browse _ = (["browse.html"], [])
getListingPathQuery Recommend _ = (["recommend.html"], [])
getListingPathQuery Graveyard _ = (["graveyard.html"], [])
getListingPathQuery Update _ = (["updates.html"], [])
getListingPathQuery Profile (ProfileParam prof _) =
  (["profile.html"], [("name", Just $ encodeUtf8 $ name $ profile prof)])
getListingPathQuery Profile (UserProfileParam prof _) =
  (["profile.html"], [("name", Just $ encodeUtf8 $ name $ profile prof)])
getListingPathQuery Profile _ = error "not possible"

{-
getListingPathQuery Profile (UserProfileParam (OwnProfile _ _ _) _) =
  PathQuery "profile.html" []
-}

class SubListing a where
  listing :: a -> ListingItem

instance SubListing ListingItem where
  listing = id

instance SubListing UserListingItem where
  listing = listing'

instance SubListing UpdateListingItem where
  listing = listing''

type NavigateData = (([Text], Query), Int, Int, Int)
