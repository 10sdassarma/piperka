{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Listing.Column.Splices (listingColumnSplices) where

import Control.Error.Util (bool, note)
import Data.Map.Syntax
import Data.Maybe
import Data.Monoid
import Data.Text (Text, pack, unpack, intercalate)
import qualified Data.Text as T
import qualified Data.Vector as V
import qualified HTMLEntities.Text as HTML
import Heist
import Heist.Compiled as C
import Heist.Compiled.LowLevel as C
import Heist.Compiled.Extra as C
import Text.XmlHtml

import Application hiding (prefs)
import Piperka.Listing.Navigate.Splices
import Piperka.Listing.Types hiding (listing', listing'')
import Piperka.Update.Types

listingColumnSplices
  :: ListingMode
  -> Splices (RuntimeSplice AppHandler
              (UserPrefs, Int, ListingParam, Maybe NavigateData)
              -> C.Splice AppHandler)
listingColumnSplices mode = do
  "subscribeControl" ## const $
    if listingModeSubscribes mode then runChildren else return mempty
  "navigateControl" ## case mode of
    Recommend -> const runChildren
    _ -> \n -> do
      ns <- runNodeList . elementChildren =<< getParamNode
      eitherDeferMap (return . note () . \(_,_,_,a) -> a)
        (const $ return ns)
        (\n' -> withLocalSplices
          (("apply-content" ## return ns) <> mapV ($ n') navigateSplices)
          mempty (callTemplate "_navigate")) n
  "columnMode" ## renderMode mode . fmap (\(a,b,c,_) -> (a,b,c))

renderMode
  :: ListingMode
  -> (RuntimeSplice AppHandler
      (UserPrefs, Int, ListingParam))
  -> C.Splice AppHandler
renderMode mode runtime = do
  col <- read . unpack . fromJust . getAttribute "col" <$> getParamNode
  tplL <- mkColumnSplices paramsL
  tplU <- mkColumnSplices paramsU
  tplP <- mkColumnSplices paramsP
  return $ C.yieldRuntime $ do
    (prefs, offset, param) <- runtime
    -- Recommend mode overrides nrows to always fit all results.
    let userCols = columns prefs
        nrows = fromIntegral $
          (case (mode, userCols) of
              (Recommend, TwoColumn) -> max 50
              (Recommend, ThreeColumn) -> max 34
              _ -> id) $ rows prefs
    if (col /= userCols)
      then return mempty
      else do
      let splitFunc =
            zipWith (\c (a,b) -> (a,(b,c)))
            [(1+offset),(1+offset+nrows)..] .
            takeWhile (not . V.null . snd) .
            case userCols of
             OneColumn -> (:[]) . (1,)
             TwoColumn -> \v -> let (v1, v2) = V.splitAt nrows v
                                in [(1,v1), (2,v2)]
             ThreeColumn -> \v -> let (v1, v1') = V.splitAt nrows v
                                      (v2, v3) = V.splitAt nrows v1'
                                  in [(1,v1), (2,v2), (3,v3)]
      tpl <- mconcat [ tplL param splitFunc
                     , tplU param splitFunc
                     , tplP param splitFunc
                     ]
      C.codeGen tpl
  where
    paramsL = (ListingMode, listingItemAttrSplices
              , listingItemSplices, extractListing)
    paramsU = (UserMode, userListingItemAttrSplices
              , userListingItemSplices, extractUserListing)
    paramsP = (UpdateMode, updateListingItemAttrSplices
              , updateListingItemSplices, extractUpdateListing)
    mkColumnSplices (itemMode, itemAttrSplices, itemSplices, extract) = do
      promise <- C.newEmptyPromise
      tpl <- C.deferMap (return . fst)
             (let withUpdateSplices spl =
                    C.deferMap (return . snd) (C.withSplices spl updateListingSplices)
                    (C.getPromise promise) in
              (case itemMode of UpdateMode -> withUpdateSplices
                                _ -> id) .
               (C.withSplices C.runChildren
                 (columnSplices mode itemMode $
                  C.manyWith C.runChildren itemSplices itemAttrSplices)))
             (C.getPromise promise)
      return $ \param f -> maybe (return mempty)
                           (\lst -> C.putPromise promise (f lst, param) >> return tpl) $
                           extract param

columnSplices
  :: ListingMode
  -> ListingItemMode
  -> RuntimeAppHandler (V.Vector a)
  -> Splices (RuntimeSplice AppHandler [(Int, (V.Vector a, Int))] -> Splice AppHandler)
columnSplices mode itemMode itemsAction = "column" ## \runtime -> do
  colNum <- read . unpack . fromJust . getAttribute "column" <$>
            getParamNode
  mayDeferMap (return . lookup colNum)
    (\runtime' ->
      (if mode `elem` [Top, Recommend]
        then \spl -> C.withSplices spl
                     ("startNum" ## C.pureSplice . C.textSplice $ pack . show . snd)
                     runtime'
        else id) $
      C.withSplices (C.callTemplate "_column")
      (singleColumnSplices mode itemMode itemsAction) $
      fmap fst runtime') runtime

updateListingSplices
  :: Splices (RuntimeSplice AppHandler ListingParam -> Splice AppHandler)
updateListingSplices = do
  "holdbookmark" ## C.checkedSplice (holdBookmark . getUpdateParam)
  "offsetBackParam" ## \runtime -> return $ yieldRuntimeText $ do
    offset <- offsetMode . getUpdateParam <$> runtime
    return $ if offset then "&offset_back=1" else ""

singleColumnSplices
  :: ListingMode
  -> ListingItemMode
  -> RuntimeAppHandler (V.Vector a)
  -> Splices (RuntimeSplice AppHandler (V.Vector a) -> Splice AppHandler)
singleColumnSplices mode itemMode itemsAction = do
  "listingMode" ## renderSingleColumn mode
  "item" ## \n -> renderItem itemMode (itemsAction n)
  "listingStdItem" ## const $ C.callTemplate "_listingStdItem"

renderSingleColumn
  :: ListingMode
  -> RuntimeSplice AppHandler (V.Vector a)
  -> Splice AppHandler
renderSingleColumn mode _ = do
  modes :: [ListingMode] <- read . unpack . fromJust . getAttribute "type"
                            <$> getParamNode
  if mode `elem` modes
    then C.runChildren
    else return $ C.yieldPure mempty

renderItem
  :: ListingItemMode
  -> Splice AppHandler
  -> Splice AppHandler
renderItem itemMode itemsSplice = do
  allowedMode :: ListingItemMode <- read . unpack . fromJust .
                                    getAttribute "type" <$> getParamNode
  if (itemMode /= allowedMode)
    then return $ C.yieldPure mempty
    else itemsSplice

listingItemSplices
  :: Splices (RuntimeSplice AppHandler ListingItem -> Splice AppHandler)
listingItemSplices = mapV (C.pureSplice . C.textSplice) $ do
  "title" ## HTML.text . title
  "cid" ## pack . show . cid

listingItemAttrSplices
  :: Splices (RuntimeSplice  AppHandler ListingItem -> AttrSplice AppHandler)
listingItemAttrSplices = do
  "freqClass" ## \n _ -> freqClass <$> n

userListingItemSplices
  :: Splices (RuntimeSplice AppHandler UserListingItem -> Splice AppHandler)
userListingItemSplices = do
  "title" ## C.pureSplice . C.textSplice $ HTML.text . title . listing
  "cid" ## C.pureSplice . C.textSplice $ pack . show . cid . listing
  "followee" ## C.checkedSplice friends
  "subscribed" ## C.checkedSplice subs
  "isNew" ## C.checkedSplice newComic

userListingItemAttrSplices
  :: Splices (RuntimeSplice AppHandler UserListingItem -> AttrSplice AppHandler)
userListingItemAttrSplices = do
  "freqClass" ## \n _ -> freqClass <$> n

updateListingItemSplices
  :: Splices (RuntimeSplice AppHandler UpdateListingItem -> Splice AppHandler)
updateListingItemSplices = mapV (C.pureSplice . C.textSplice) $ do
  "title" ## HTML.text . title . listing
  "cid" ## pack . show . cid . listing
  "new" ## pack . show . new
  "directLink" ## HTML.text . fromMaybe "" . directLink

updateListingItemAttrSplices
  :: Splices (RuntimeSplice AppHandler UpdateListingItem -> AttrSplice AppHandler)
updateListingItemAttrSplices = do
  "class" ## \n t ->
    ((\xs -> if null xs then [] else [("class", intercalate " " xs)])
     . foldr ($) [] . flip map
     [ const $ if T.null t then id else (t:)
     , bool id ("nsfw":) . nsfw
     , maybe id ((:) . snd) . listToMaybe . freqClass
     ] . flip id) <$> n

freqClass
  :: SubListing a
  => a
  -> [(Text, Text)]
freqClass = let cl (Just UpdateHigh) = [("class", "update-high")]
                cl (Just UpdateMid) = [("class", "update-mid")]
                cl (Just UpdateLow) = [("class", "update-low")]
                cl _ = []
            in cl . updateFreq . listing
