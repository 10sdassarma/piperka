{-# LANGUAGE OverloadedStrings #-}

module Piperka.Listing.Navigate.Splices (navigateSplices) where

import Heist
import Heist.Compiled as C
import Prelude hiding (Ordering)
import Data.Text (Text)
import Network.HTTP.Types.URI (Query)
import qualified Data.ByteString.Char8 as B
import Data.Map.Syntax

import Application
import Piperka.Listing.Types
import Piperka.Util (encodePathToText)

navigateSplices
  :: Splices (RuntimeAppHandler NavigateData)
navigateSplices = do
  "start" ## renderNavigate (const . const . const 0) (<)
  "prev" ## renderNavigate (const (-)) (<)
  "next" ## renderNavigate (const (+)) (>)
  "end" ## renderNavigate (const . const) (>)

renderNavigate
  :: (Int -> Int -> Int -> Int)
  -> (Int -> Int -> Bool)
  -> RuntimeAppHandler (([Text], Query), Int, Int, Int)
renderNavigate getOffset cmp runtime = do
  deferMap getThisOffset
    (\runtimeAction -> withLocalSplices mempty
                       (offsetHref runtimeAction)
                       C.runChildren) runtime
  where
    getThisOffset (q, curOffset, blockSize, total) =
      let total' = total - blockSize
          offset = getOffset total' curOffset blockSize
          offset' = if offset < 0 then 0
                    else if offset > (total - 1) then total'
                         else offset
      in return $ if offset' `cmp` curOffset then Just (q, offset') else Nothing

offsetHref
  :: RuntimeSplice AppHandler (Maybe (([Text], Query), Int))
  -> Splices (AttrSplice AppHandler)
offsetHref runtime =
  "href" ## \attrName -> do
    val <- runtime
    return $ maybe [] (\((path, q), offset) ->
                        let q' = q ++ [("offset", Just $ B.pack $ show offset)] in
                        [(attrName, encodePathToText path q')]) val
