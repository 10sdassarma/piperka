{-# LANGUAGE OverloadedStrings #-}

-- Stand-alone single shot DB connections

module Piperka.Util.DB (cfg, runSession, runTransaction) where

import Control.Exception
import Data.Bifunctor
import Hasql.Connection
import Hasql.Session

-- TODO: Remove hardcoded settings parameter.
cfg :: Settings
cfg = "postgresql://kaol@/piperka"

runTransaction
  :: Session a
  -> IO (Either String a)
runTransaction session = runSession $
  sql "BEGIN" >> session >>= \a -> sql "COMMIT" >> return a

runSession
  :: Session a
  -> IO (Either String a)
runSession session =
  bracket (acquire cfg)
  (either (const $ return ()) release) eitherRun
  where
    eitherRun (Left e) = return $ Left $ show e
    eitherRun (Right x) = first show <$> run session x
