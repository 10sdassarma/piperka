{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Recommend where

import Control.Lens
import Control.Monad.Trans
import Control.Monad.State
import Data.Int
import Data.Map.Syntax
import qualified Data.Vector as V
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Heist.Compiled
import Heist.Compiled.Extra
import Snap.Snaplet.Hasql

import Application
import Piperka.Error.Splices
import Piperka.Recommend.Process

-- countme gets usually updated nightly but this is one case where
-- results would differ without checking
fetchUserSubscriptionsCount
  :: Int32
  -> Session (Int, Bool)
fetchUserSubscriptionsCount = flip statement $ Statement
  "WITH force_countme AS (\
  \UPDATE users SET countme=true WHERE uid=$1 AND NOT countme RETURNING *) \
  \SELECT count(*), countme IS NOT NULL \
  \FROM subscriptions JOIN comics USING (cid) LEFT JOIN force_countme USING (uid) \
  \WHERE uid=$1 GROUP BY countme"
  (EN.param EN.int4)
  (DE.singleRow $ (,) <$> (fromIntegral <$> DE.column DE.int4) <*> DE.column DE.bool) True

renderRecommend
  :: RuntimeAppHandler MyData
renderRecommend n = do
  eitherDeferMap (run . fetchUserSubscriptionsCount . uid)
    stdSqlErrorSplice
    (withSplices runChildren
     (do
         "tooFew" ## \n' -> do
           x <- runChildren
           flip bindLater n' $ \subs -> do
             -- TODO: This doesn't actually work.  The recommendations
             -- do work after the minutely refresh but this call
             -- doesn't seem to trigger an update successfully.  It's
             -- an edge case so I'll leave it for now.
             {-
             when (snd subs) $ do
               h <- lift $ view recommendHandle
               liftIO $ refreshRecommends h
             -}
             if fst subs < 5 then codeGen x else return mempty
         "canRecommend" ## \n' -> do
           x <- renderCanRecommend n
           flip bindLater n' $ \subs -> do
             if fst subs >= 5 then codeGen x else return mempty
     )) n

renderCanRecommend
  :: RuntimeAppHandler MyData
renderCanRecommend =
  let runRenderChildren s = const $ withLocalSplices s mempty runChildren
  in eitherDeferMap (\usr -> do
                        h <- lift $ view recommendHandle
                        rc <- liftIO $ getRecommends h 100 $ uid usr
                        if V.null rc
                          then return $ Left ()
                          else do
                          lift $ modify $ set recommendComics rc
                          return $ Right ()
                    )
     (runRenderChildren $ ("error" ## runChildren) >> ("results" ## return mempty))
     (runRenderChildren $ ("error" ## return mempty) >> ("results" ## runChildren))
