module Piperka.API (
    quickSearch
  , comicInfo
  , tagList
  , userPrefs
  , dumpArchive
  , profileSubmission
  , attachProvider
  , receiveSubmit
  , lookupPage
  , updateWatch
  -- Used by the unofficial Android app
  , apiLogin
  -- Moderator actions
  , readSubmit
  , readUserEdit
  , dropUserEdit
  , viewSubmitBanner
  , readGenentry
  , crawler
  ) where

import Piperka.API.Archive
import Piperka.API.ComicInfo
import Piperka.API.Crawler
import Piperka.API.Genentry
import Piperka.API.Login
import Piperka.API.Lookup
import Piperka.API.Profile
import Piperka.API.Provider
import Piperka.API.QuickSearch
import Piperka.API.Submit
import Piperka.API.SubmitInfo
import Piperka.API.TagList
import Piperka.API.UpdateWatch
import Piperka.API.UserPrefs
