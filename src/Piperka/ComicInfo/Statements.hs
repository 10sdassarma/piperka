{-# LANGUAGE OverloadedStrings #-}

module Piperka.ComicInfo.Statements where

import Contravariant.Extras.Contrazip
import Control.Applicative
import Control.Monad
import Data.Time.LocalTime
import Data.Int
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Statement

import Piperka.ComicInfo.Types

type ComicInfoSeed = Vector CrawlError
                     -> [ExternalEntry]
                     -> ComicInfo

decodeComicInfo :: ([Int] -> [ComicTag]) -> DE.Row ComicInfoSeed
decodeComicInfo taglookup =
  ComicInfo
  <$> (liftA fromIntegral $ DE.column DE.int4)
  <*> DE.column DE.text
  <*> DE.column DE.text
  <*> (liftA fromIntegral $ DE.column DE.int4)
  <*> DE.column DE.bool
  <*> (liftA (fmap fromIntegral) $ DE.nullableColumn DE.int4)
  <*> (liftA fromIntegral $ DE.column DE.int4)
  <*> liftA (\(a,b,c) -> do
                a' <- a; b' <- b; c' <- c
                return (a',b',c')) ((,,)
                                    <$> DE.nullableColumn DE.text
                                    <*> DE.nullableColumn DE.text
                                    <*> DE.nullableColumn DE.bool)
  <*> (liftA (fmap $ localTimeToUTC utc) $ DE.nullableColumn DE.timestamp)
  <*> DE.nullableColumn DE.bool
  <*> DE.column DE.bool
  <*> DE.nullableColumn (DE.composite ((,)
                                      <$> DE.field DE.text
                                      <*> (liftA (localTimeToUTC utc) $
                                           DE.field DE.timestamp)))
  <*> DE.nullableColumn DE.text
  <*> (liftA (maybe "" id) $ DE.nullableColumn DE.text)
  <*> (liftA (taglookup . map fromIntegral) $
       DE.column (DE.array $ DE.dimension replicateM $ DE.element DE.int4))

decodeDeadComicInfo
  :: ([Int] -> [ComicTag])
  -> DE.Row ComicInfo
decodeDeadComicInfo taglookup =
  decodeComicInfo taglookup
  <*> pure mempty
  <*> pure mempty

decodeCrawlError :: DE.Row CrawlError
decodeCrawlError =
  CrawlError
  <$> (liftA fromIntegral $ DE.column DE.int4)
  <*> (liftA (localTimeToUTC utc) $ DE.column DE.timestamp)
  <*> DE.column DE.text
  <*> (liftA (fmap fromIntegral) $ DE.nullableColumn DE.int4)
  <*> DE.column DE.text

decodeExternalEntry
  :: (Int -> Text -> Maybe ExternalEntry)
  -> DE.Row (Maybe ExternalEntry)
decodeExternalEntry extlookup =
  extlookup
  <$> (liftA fromIntegral $ DE.column DE.int4)
  <*> DE.column DE.text

comicInfoFetch
  :: ([Int] -> [ComicTag])
  -> Statement (Maybe Int32, Int32) (Maybe ComicInfoSeed)
comicInfoFetch taglookup =
  Statement sql (contrazip2 (EN.nullableParam EN.int4) (EN.param EN.int4))
  (DE.rowMaybe $ decodeComicInfo taglookup) True
  where
    sql = "SELECT cid, title, homepage, readers, \
          \cid IN (SELECT cid FROM subscriptions AS subs \
          \ LEFT JOIN (SELECT cid, interest AS uid, interest \
          \ FROM permitted_interest WHERE uid=u.uid) AS interest USING (cid, uid) \
          \ WHERE uid IN (SELECT uid FROM users WHERE countme AND privacy=3) \
          \  OR interest IS NOT NULL) as public_readers, \
          \max_ord_of(cid)+1, (SELECT COUNT(*) FROM page_fragments WHERE cid=c.cid), \
          \(SELECT url_base||name||url_tail FROM updates WHERE cid=c.cid AND ord=0 LIMIT 1), \
          \(SELECT url_base||name||url_tail FROM updates WHERE cid=c.cid AND name IS NOT NULL ORDER BY ord DESC LIMIT 1), \
          \(SELECT name IS NULL FROM updates WHERE cid=c.cid ORDER BY ord DESC LIMIT 1), \
          \added_on, EXISTS (SELECT uid FROM subscriptions WHERE cid=c.cid AND uid=u.uid) AS subscribed, \
          \mapped, null AS dead, file AS banner, description, \
          \COALESCE((SELECT array_agg(tagid) FROM comic_tag WHERE cid=c.cid), '{}') AS tags \
          \FROM comics AS c LEFT JOIN banners USING (cid) \
          \LEFT JOIN users AS u ON (u.uid=$1) WHERE cid=$2"

deadInfoFetch
  :: ([Int] -> [ComicTag])
  -> Statement (Maybe Int32, Int32) (Maybe ComicInfo)
deadInfoFetch taglookup =
  Statement sql (contrazip2 (EN.nullableParam EN.int4) (EN.param EN.int4))
  (DE.rowMaybe $ decodeDeadComicInfo taglookup) True
  where
    sql = "SELECT cid, title, homepage, 0 AS readers, false AS public_readers, \
          \max_ord_of(cid)+1, (SELECT count(*) FROM page_fragments WHERE cid=c.cid), \
          \(SELECT url_base||name||url_tail FROM updates WHERE cid=c.cid AND ord=0 LIMIT 1), \
          \(SELECT url_base||name||url_tail FROM updates WHERE cid=c.cid AND name IS NOT NULL ORDER BY ord DESC LIMIT 1), \
          \(SELECT name IS NULL FROM updates WHERE cid=c.cid ORDER BY ord DESC LIMIT 1), \
          \added_on, EXISTS (SELECT true FROM subscriptions WHERE cid=c.cid AND uid=u.uid) AS subscribed, \
          \false, (reason, removed_on), file AS banner, description, \
          \COALESCE((SELECT array_agg(tagid) FROM comic_tag WHERE cid=c.cid), '{}') AS tags \
          \FROM graveyard AS c LEFT JOIN banners USING (cid) \
          \LEFT JOIN users AS u ON (u.uid=$1) WHERE c.cid=$2"

crawlErrorFetch
  :: Statement Int32 (Vector CrawlError)
crawlErrorFetch =
  Statement sql (EN.param EN.int4) (DE.rowVector decodeCrawlError) True
  where
    sql = "SELECT ord, stamp, url, http_code, coalesce(http_message, '') \
           \FROM crawl_error WHERE cid=$1 AND stamp > date 'now' - integer '30' \
           \ORDER BY errid DESC LIMIT 5"

externalEntryFetch
  :: (Int -> Text -> Maybe ExternalEntry)
  -> Statement Int32 [Maybe ExternalEntry]
externalEntryFetch extlookup =
  Statement sql (EN.param EN.int4)
  (DE.rowList $ decodeExternalEntry extlookup) True
  where
    sql = "SELECT epid, entry FROM external_entry WHERE cid=$1 ORDER BY epid"

isComicDeadFetch
  :: Statement Int32 (Bool, Maybe (Int, Text, Bool))
isComicDeadFetch =
  Statement sql (EN.param EN.int4) (DE.singleRow $ decode) True
  where
    decode = (,)
      <$> (DE.column DE.bool)
      <*> (DE.nullableColumn $ DE.composite $ (,,)
            <$> (fromIntegral <$> DE.field DE.int4)
            <*> DE.field DE.text
            <*> DE.field DE.bool)
    sql = "SELECT $1 IN (SELECT cid FROM graveyard), (\
          \SELECT (cid, title, cid IN (SELECT cid FROM graveyard)) \
          \FROM merged JOIN all_comics USING (cid) WHERE merged_from=$1)"
