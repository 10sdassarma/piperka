{-# LANGUAGE OverloadedStrings #-}

module Piperka.ComicInfo.Query where

import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Text (Text)
import Data.Maybe
import Hasql.Session hiding (run)
import Snap.Snaplet.Hasql

import Application hiding (uid, taglookup, extlookup, prefs)
import qualified Application (uid)
import Piperka.ComicInfo.Statements
import Piperka.ComicInfo.Types hiding (cid)

getComicInfo
  :: (HasHasql m, MonadIO m, Integral n)
  => ([Int] -> [ComicTag])
  -> (Int -> Text -> Maybe ExternalEntry)
  -> n
  -> Maybe MyData
  -> m (Either ComicInfoError ComicInfo)
getComicInfo taglookup extlookup cid usr = do
  let uid = Application.uid <$> usr
  runExceptT $ do
    info' <- ExceptT $ (either (Left . SqlError) return) <$>
             (run $ statement (uid, fromIntegral cid) $
              comicInfoFetch taglookup)
    info <- ExceptT $ maybe (Left <$> checkDead (fromIntegral cid))
            (return . return) info'
    err <- ExceptT $ (either (Left . SqlError) return) <$>
           (run $ statement (fromIntegral cid) crawlErrorFetch)
    ext <- ExceptT $ (either (Left . SqlError) (return . catMaybes)) <$>
           (run $ statement (fromIntegral cid) $
            externalEntryFetch extlookup)
    return $ info err ext

checkDead
  :: (HasHasql m, MonadIO m)
  => Int
  -> m ComicInfoError
checkDead cid =
  either SqlError errType <$>
  (run $ statement (fromIntegral cid) isComicDeadFetch)
  where
    errType (True, _) = FoundDead
    errType (_, Just x) = Merged x
    errType _ = Missing

getDeadComicInfo
  :: (HasHasql m, MonadIO m, Integral n)
  => ([Int] -> [ComicTag])
  -> n
  -> Maybe MyData
  -> m (Either ComicInfoError ComicInfo)
getDeadComicInfo taglookup cid usr = do
  let uid = Application.uid <$> usr
  (either (Left . SqlError) (maybe (Left Missing) return)) <$>
    (run $ statement (uid, fromIntegral cid) $
     deadInfoFetch taglookup)
