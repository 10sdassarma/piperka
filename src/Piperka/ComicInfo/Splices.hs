{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.ComicInfo.Splices
  (
    getComicInfoData
  , renderComicInfo
  , renderWithCid
  )
  where

import Control.Error.Util
import Control.Exception
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.State (modify)
import Control.Lens (view, set)
import Data.Maybe
import Data.Monoid
import Data.Map.Syntax
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.IO as IO
import qualified Data.Vector as V
import Heist
import Heist.Compiled
import Heist.Compiled.Extra
import qualified HTMLEntities.Text as HTML
import Snap
import qualified Text.XmlHtml as X

import Application
import Piperka.ComicInfo.Query
import Piperka.ComicInfo.Types
import Piperka.Error.Splices
import Piperka.Util (formatTime', getCid)

renderFailure
  :: Bool
  -> RuntimeAppHandler ComicInfoError
renderFailure deadPage n = do
  let mergedSplices = mapV (pureSplice . textSplice) $ do
        "otherComic" ## \(c,_,d) ->
          (if d then "deadinfo.html?cid=" else "info.html?cid=") <> (T.pack $ show c)
        "title" ## HTML.text . \(_,x,_) -> x
  sqlErr <- stdSqlErrorSplice $ (\(~(SqlError e)) -> e) <$> n
  removed <- callTemplate "/cinfo/_removed"
  missing <- callTemplate $
    if deadPage then "/cinfo/_dead_missing" else "/cinfo/_missing"
  merged <- withSplices (callTemplate "/cinfo/_merged") mergedSplices $
            (\(~(Merged x)) -> x) <$> n
  let errs Missing = lift (modifyResponse $ setResponseCode 404) >> codeGen missing
      errs (SqlError _) = codeGen sqlErr
      errs FoundDead = codeGen removed
      errs (Merged _) = codeGen merged
  bindLater errs n

-- When used as a top level splice
renderComicInfo
  :: RuntimeAppHandler (Maybe MyData)
renderComicInfo n = do
  nodes <- X.elementChildren <$> getParamNode
  let success n' = withSplices
        (if null nodes then callTemplate "/include/cinfo" else runNodeList nodes)
        comicInfoSplices n'
      fetch = yieldRuntimeEffect $ do
        u <- n
        lift $ modify . set prefetchComicInfo . Just =<< getComicInfoData Info u
  rd <- render success (renderFailure False)
  return $ fetch <> rd

renderWithCid
  :: Splice AppHandler
renderWithCid = do
  deadPage <- maybe False (read . T.unpack) . X.getAttribute "dead" <$>
              getParamNode
  let success n' = withSplices runChildren
                   ("exists" ## renderExists) n'
      failure =
        withLocalSplices ("exists" ## return mempty) mempty . renderFailure deadPage
  render success failure

getComicInfoData
  :: Prefetch
  -> Maybe MyData
  -> AppHandler (Either ComicInfoError ComicInfo)
getComicInfoData mode u = do
  tlookup <- view taglookup
  elookup <- view extlookup
  let getInfo = getComicInfo tlookup elookup
      getDeadInfo = getDeadComicInfo tlookup

  runExceptT $ do
    c <- snd <$> (hoistEither . note Missing =<< lift getCid)
    info <- ExceptT $ (case mode of
                         Info -> getInfo
                         Dead -> getDeadInfo) c u
    return info

render
  :: RuntimeAppHandler ComicInfo
  -> RuntimeAppHandler ComicInfoError
  -> Splice AppHandler
render success failure =
  withLocalSplices
  ("cid" ## return $ yieldRuntimeText $
    (maybe "" (T.decodeLatin1 . fst) <$> lift getCid))
  mempty $
  eitherDeferMap (const $ maybe (Left Missing) id <$>
                   (lift $ view prefetchComicInfo))
  failure success $ return ()

renderExists :: RuntimeAppHandler ComicInfo
renderExists = withSplices runChildren $ do
  "related" ## \info -> return $ yieldRuntimeText $ do
    c <- cid <$> info
    let fileName = "/srv/piperka.net/files/related/" <> show c
    liftIO $ catch (IO.readFile fileName)
      (\e -> let _ = e :: IOException in return "")
  "comicInfo" ## \n -> withSplices (callTemplate "/include/cinfo")
                       comicInfoSplices n
  "ifSubscribed" ## \n -> do
    nodes <- X.elementChildren <$> getParamNode
    let nodeFilter name = runNodeList $ concatMap X.childNodes $
                          filter (maybe False (== name) . X.tagName) nodes
    t <- nodeFilter "true"
    f <- nodeFilter "false"
    flip bindLater n $ \info -> do
      codeGen $ if fromMaybe False $ subscribed info then f else t
  "crawlErrors" ## renderCrawlErrors
  "ifMapped" ## \n -> do
    x <- runChildren
    flip bindLater n $ \n' -> do
      if mapped n' then codeGen x else return mempty

renderCrawlErrors
  :: RuntimeAppHandler ComicInfo
renderCrawlErrors n = do
  let rowSplices = mapV (pureSplice . textSplice) $ do
        "ord" ## T.pack . show . ord
        "time" ## T.pack . formatTime' . time
        "url" ## archiveUrl
        "code" ## maybe "" (T.pack . show) . code
        "msg" ## msg
  let renderRows = manyWithSplices runChildren rowSplices $ crawlErrors <$> n
  inner <- withLocalSplices ("rows" ## renderRows) mempty runChildren
  flip bindLater n $ \info ->
    if V.null $ crawlErrors info then return mempty else codeGen inner

comicInfoSplices
  :: Splices (RuntimeAppHandler ComicInfo)
comicInfoSplices = do
  "comicInfo" ## const $ runChildren
  "banner" ## renderBanner
  "title" ## pureSplice . textSplice $ title
  "readersLink" ## renderReadersLink
  "subscriptions" ## pureSplice . textSplice $ T.pack . show . readers
  "hasFragments" ## renderHasFragments
  "fragmentCount" ## pureSplice . textSplice $ T.pack . show . fragmentCount
  "pageCount" ## eitherDeferMap
    (return . note () . pageCount)
    (const runChildren)
    (pureSplice . textSplice $ T.pack . show)
  "ifKnowPages" ## renderIfKnowPages
  "homepage" ## pureSplice . textSplice $ homepage  -- TODO: Escape?
  "homepageText" ## pureSplice . textSplice $ HTML.text . homepage
  "ifExternLinks" ## renderIfExternLinks
  "ifAddDate" ## renderIfAddDate
  "categories" ## renderCategories
  "description" ## renderDescription
  "dead" ## deferMany (withSplices runChildren deadSplices) . fmap dead
  where
    renderBanner n = manyWithSplices runChildren
      ("bannerUrl" ## pureSplice . textSplice $ id) $
      ((("/banners/" <>) <$>) . banner) <$> n
    renderReadersLink n = do
      node <- getParamNode
      noLink <- runChildren
      withLink <- runNode $ X.Element "a"
                  [("href", "readers.html?cid=${h:cid}")] $
                  X.elementChildren node
      flip bindLater n $ \info ->
        codeGen $ if publicReaders info then withLink else noLink
    renderHasFragments n = do
      x <- runChildren
      flip bindLater n $ \info ->
        if fragmentCount info > 0 then codeGen x else return mempty
    renderIfKnowPages n = do
      nodes <- X.elementChildren <$> getParamNode
      let archivePagesSplices = do
            "firstPageUrl" ## pureSplice . textSplice $ \(x, _, _) -> x
            "lastPageUrl" ## pureSplice . textSplice $ \(_, x, _) -> x
            "ifFixedHead" ## \n' -> do
              c <- runChildren
              flip bindLater n' $ \(_, _, x) ->
                if x then codeGen c else return mempty
      let nodeFilter name = runNodeList $
                            concatMap X.childNodes $
                            filter (maybe False (== name) . X.tagName) nodes
      t <- withSplices (nodeFilter "true") archivePagesSplices $
           fromJust . archivePages <$> n
      f <- nodeFilter "false"
      flip bindLater n $ \info ->
        codeGen $ if (isJust $ archivePages info) then t else f
    renderIfExternLinks n = do
      c <- withSplices runChildren ("externLink" ## renderExternLink) $
           extern <$> n
      flip bindLater n $ \info ->
        if null $ extern info then return mempty else codeGen c
    renderExternLink :: RuntimeSplice AppHandler [ExternalEntry] -> Splice AppHandler
    renderExternLink = manyWithSplices runChildren $
                       mapV (pureSplice . textSplice) $ do
                         "url" ## \n -> (base n <> urlPart n)
                         "description" ## eDescription
                         "siteName" ## epediaTagName
    renderIfAddDate n =
      manyWithSplices runChildren
      ("addDate" ## pureSplice . textSplice $ T.pack . formatTime') $
      addedOn <$> n
    renderCategories n =
      let splices = do
            "name" ## pureSplice . textSplice $ tagName . snd
          attrSplices = mapV (\f x _ -> f <$> x) $ do
            "description" ##
              maybe [] (\d -> [(T.pack "title", d)]) . tagDescription . snd
            "class" ## bool [(T.pack "class", "odd")] [] . fst
      in manyWith runChildren splices attrSplices $
         (zip (cycle [False, True]) . tags) <$> n
    renderDescription = pureSplice . textSplice $ description
    deadSplices = do
      "removeDate" ## pureSplice . textSplice $ T.pack . formatTime' . snd
      "reason" ## pureSplice . textSplice $ fst
