{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Splices
  (
    piperkaSplices
  )
  where

import Control.Applicative ((<|>))
import Control.Error.Util (note, bool, hush)
import Control.Lens
import Control.Monad.State
import Data.DList (DList)
import Data.List (partition)
import Data.Map.Syntax
import Data.Maybe
import Data.Monoid
import qualified Data.Text as T
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8, decodeUtf8')
import Data.UUID
import qualified Database.Memcache.Client as M
import Heist
import Heist.Compiled as C
import qualified Heist.Compiled.Extra as C
import Heist.Internal.Types
import qualified HTMLEntities.Text as HTML
import Snap
import Snap.Snaplet.CustomAuth
import qualified Text.XmlHtml as X

import Application
import Backend()
import Piperka.Account
import Piperka.Action.Splices
import Piperka.Action.Types
import Piperka.API.Submit.Splices
import Piperka.Auth (currentUserPlain)
import Piperka.Auth.Splices
import Piperka.Bookmarklet
import Piperka.ComicInfo.Types (Prefetch(..))
import qualified Piperka.ComicInfo.Types as ComicInfo
import Piperka.OAuth2.Splices
import Piperka.Maint.Splices
import Piperka.Maint.Crawler.Splices
import Piperka.Maint.Health
import Piperka.Maint.Ticket.Splices
import Piperka.Profile.Follower
import Piperka.Recommend
import Piperka.Splices.Account
import Piperka.Splices.Flavor
import Piperka.Splices.Providers
import Piperka.Splices.Revert
import Piperka.Splices.Stats
import Piperka.Splices.UserDeleted
import Piperka.Submission
import Piperka.Submit.Splices
import Piperka.Listing.Render
import Piperka.ComicInfo.Splices
import Piperka.Messages
import Piperka.Readers
import Piperka.Recover
import Piperka.Ticket
import Piperka.Ticket.User.Splices
import Piperka.Util

piperkaSplices
  :: AppInit
  -> Splices (C.Splice AppHandler)
piperkaSplices ini = do
  "hostname" ## return $ yieldPureText $ appHostname ini
  "ifPage" ## do
    node <- getParamNode
    let nm = maybe "" encodeUtf8 $ X.getAttribute "name" node
        oper = bool (==) (/=) $ maybe False (read . T.unpack) $ X.getAttribute "not" node
    ctx <- listToMaybe . _curContext <$> getHS
    if Just nm `oper` ctx then runChildren else return mempty
  "ifMinimal" ## do
    node <- getParamNode
    let oper = bool (==) (/=) $ maybe False (read . T.unpack) $ X.getAttribute "not" node
    chld <- runChildren
    return $ yieldRuntime $ do
      useMinimal <- lift $ view minimal
      codeGen $ if useMinimal `oper` True then chld else mempty
  "submitAPI" ## submitAPISplices
  "script" ## hashTaggedNode "script" "src"
  "stylesheet" ## hashTaggedNode "link" "href"
  "subscribeForm" ## subscribeForm
  "submit" ## renderSubmit ini
  "paramAttrs" ## withLocalSplices mempty ("value" ## paramValue) runChildren
  "updateStatus" ## updateStatus
  "kaolSubs" ## renderKaolSubs
  "fortune" ## renderFortune
  "providers" ## renderProviders
  "piperka" ## do
    adsEnabledForPage <- maybe True (read . T.unpack) . X.getAttribute "ads" <$>
      getParamNode
    withLocalSplices
      ("ad" ## (fromJust . X.getAttribute "pos" <$> getParamNode) >>=
        maybe (return mempty)
        (\(link, title) -> withLocalSplices
          (mapV (return . yieldPureText) $ do
              "link" ## link
              "title" ## title
          ) mempty runChildren) . (\pos -> guard adsEnabledForPage >>
                                    lookup pos (adsParams ini))) mempty renderPiperka

-- Splice definition overridden if used via withCid.
  "comicInfo" ## renderMinimal renderComicInfo
  <> messagesSplices
  where
    updateStatus = return $ yieldRuntimeText $ do
      mc <- lift $ view memcache
      v <- liftIO $ (hush . decodeUtf8' . (\(v,_,_) -> v) =<<) <$>
        M.get mc "updatestatus"
      return $ maybe "" id v
    subscribeForm = do
      action <- X.getAttribute "action" <$> getParamNode
      withLocalSplices mempty
        ("action" ## const $ return $ maybe [] (\x -> [("action", x)]) action)
        $ callTemplate "_subscribe"
    hashTaggedNode tagName srcAttr = do
      node <- getParamNode
      let src = fromJust $ X.getAttribute srcAttr node
      token <- maybe (liftIO $ randomString 6) return $
        lookup src $ scriptHash ini
      runNode $ X.setAttribute srcAttr (src <> "?v=" <> token) $
        node {X.elementTag = tagName}

renderMinimal
  :: RuntimeAppHandler (Maybe MyData)
  -> C.Splice AppHandler
renderMinimal action =
  (\n -> withSplices (action n) contentSplices' n) `C.defer`
  (lift currentUserPlain)

renderContent
  :: [X.Node]
  -> RuntimeAppHandler (Maybe MyData)
renderContent ns n = do
  authContent <- deferMany (withSplices (callTemplate "_authFailure") authErrorSplices) $ do
    suppress <- lift $ withTop' id $ view suppressError
    if suppress then return Nothing else do
      err1 <- lift $ withTop auth $ getAuthFailData
      err2 <- lift $ withTop apiAuth $ getAuthFailData
      return $ err1 <|> err2
  content <- withSplices (runNodeList ns) contentSplices' n
  return $ authContent <> content

renderPiperka
  :: C.Splice AppHandler
renderPiperka = do
  node <- getParamNode
  let xs = X.childNodes node
      isExtra (X.Element "extra" _ _) = True
      isExtra _ = False
      (extraNodes, contentNodes) = partition isExtra xs
      title = X.getAttribute "title" node
      fetch = read . T.unpack <$> X.getAttribute "prefetch" node
  let getInner n = do
        content <- renderContent contentNodes $ snd <$> n
        extra <- withSplices (runNodeList extraNodes) contentSplices' $ snd <$> n
        C.withSplices (C.callTemplate "_base")
          (contentSplices title fetch content extra) n
      renderInner n =
        let inner = getInner $ (\(a, u) -> (a, user <$> u)) <$> n
        in C.eitherDeferMap (return . note () . snd)
           (C.withSplices inner nullStatsSplices)
           (C.withSplices inner statsSplices) n
  normal <- (renderInner `C.defer`) $ do
    u <- lift $ withTop auth currentUser
    a <- lift $ withTop' id $ view actionResult
    let fillPrefetch mode =
          modify . set prefetchComicInfo . Just =<< getComicInfoData mode (user <$> u)
    maybe (return ()) (lift . fillPrefetch) fetch
    return (a, u)
  bare <- renderMinimal $ nullCreateSplice .
          (C.withSplices (C.runNodeList xs)
           (contentSplices' <> ("onlyWithStats" ## const $ return mempty)))
  return $ yieldRuntime $ do
    useMinimal <- lift $ view minimal
    codeGen $ if useMinimal then bare else normal

statsSplices
  :: Splices (RuntimeAppHandler UserWithStats)
statsSplices = do
  "unreadStats" ## unreadCountSplice . fmap unreadCount
  "newComics" ## newComicsSplice . fmap newComics
  "modStats" ## \n -> do
    let modSplices =
          (mapV (C.pureSplice . C.textSplice . (fmap (T.pack . show))) $ do
              "modCount" ## fst
              "modDays" ## snd
          )
          <> ("haveModCount" ## C.conditionalChildren (const C.runChildren) ((>0) . fst))
          <> ("nag" ## C.conditionalChildren (const C.runChildren) ((>6) . snd))
    spl <- C.mayDeferMap (return . modStats)
           (C.withSplices C.runChildren modSplices) n
    return $ yieldRuntime $ do
      m <- (>0) . moderator . user <$> n
      if m then C.codeGen spl else return mempty
  "onlyWithStats" ## const $ C.runChildren

nullStatsSplices
  :: Splices (RuntimeAppHandler a)
nullStatsSplices = mapV (const . return) $ do
  "unreadStats" ## mempty
  "newComics" ## mempty
  "modStats" ## mempty
  "onlyWithStats" ## mempty

contentSplices
  :: Maybe Text
  -> Maybe Prefetch
  -> DList (Chunk AppHandler)
  -> DList (Chunk AppHandler)
  -> Splices (RuntimeAppHandler (Maybe (Maybe ActionError, Maybe Action), Maybe MyData))
contentSplices title fetch content extra =
  ("action" ## (\n -> renderAction (return content) $ fst <$> n)) <>
  ("extra" ## const $ return extra) <>
  ("title" ## \n -> do
      suffix <- withSplices runChildren (mapV (. fmap snd) contentSplices') n
      let titlePart = maybe "" (<> " — ") title
          titleWithComic = do
            comicTitle <- maybe ("Error")
              (either (const "Error") (HTML.text . ComicInfo.title)) <$>
              (lift $ view prefetchComicInfo)
            return $ titlePart <> comicTitle <> " — "
          prefix = yieldRuntimeText $
            maybe (return titlePart) (const titleWithComic) fetch
      return $ prefix <> suffix
  ) <>
  (mapV (. fmap snd)) contentSplices'

contentSplices'
  :: Splices (RuntimeAppHandler (Maybe MyData))
contentSplices' = do
  "ifLoggedIn" ## C.deferMany (C.withSplices C.runChildren loggedInSplices)
  "ifLoggedOut" ## C.conditionalChildren
    (const C.runChildren)
    isNothing
  "notMod" ## \n -> do
    level <- maybe 1 (read . T.unpack) . X.getAttribute "level" <$> getParamNode
    spl <- runChildren
    return $ yieldRuntime $ do
      m <- maybe False ((>=level) . moderator) <$> n
      if m
        then return mempty
        else lift (modifyResponse $ setResponseCode 403) >> codeGen spl
  "listing" ## renderListing
  "externA" ## \n -> renderExternA $ getPrefs <$> n
  "withCid" ## const renderWithCid
  "csrfForm" ## csrfForm
  "passwordRecovery" ## renderPasswordRecovery
  "usePasswordHash" ## renderUsePasswordHash
  "ifMod" ## \n -> do
    level <- maybe 1 (read . T.unpack) . X.getAttribute "level" <$> getParamNode
    C.conditionalChildren
      (manyWithSplices C.runChildren moderatorSplices)
      (maybe False ((>=level) . moderator)) n
  "email" ## defer $ \n -> return $ yieldRuntimeText $
    maybe (return "") (lift . getUserEmail . uid) =<< n
  "oauth2Create" ## renderOAuth2
  "readers" ## renderReaders
  "ticket" ## renderTicket

loggedInSplices
  :: Splices (RuntimeAppHandler MyData)
loggedInSplices = do
  "loggedInUser" ## C.pureSplice . C.textSplice $ HTML.text . uname
  "withBookmarklet" ## renderWithBookmarklet
  "csrf" ## C.pureSplice . C.textSplice $ toText . ucsrfToken
  "profileLink" ## profileLink
  "followers" ## renderFollowers
  "accountForm" ## renderAccountForm
  "recent" ## renderRecent
  "recommend" ## renderRecommend
  "userDeletedComics" ## renderUserDeletedComics
  "userTickets" ## renderUserTickets
  "explicitStats" ## explicitStats

moderatorSplices
  :: Splices (RuntimeAppHandler MyData)
moderatorSplices = do
  "listOfEdits" ## renderListOfEdits
  "submissions" ## const renderSubmissions
  "genentry" ## renderGenentry
  "ticketList" ## renderTicketList
  "ticketDetail" ## renderTicketDetail
  "graveyard" ## renderMorgue
  "crawlerControl" ## renderCrawlerControl
  "crawlerArchive" ## renderCrawlerArchive
  "healthCrawlerFailures" ## const healthCrawler

profileLink
  :: RuntimeAppHandler MyData
profileLink =
  let mkLink = \prof -> encodePathToText ["profile.html"]
                        [("name", Just $ encodeUtf8 prof)]
  in C.pureSplice . C.textSplice $ mkLink . uname

csrfForm
  :: RuntimeAppHandler a
csrfForm _ = do
  rootNode <- getParamNode
  let sub = X.childNodes rootNode
  inner <- runNodeList sub
  let formSplices = do
        "apply-form-content" ## return inner
        "form" ## do
          node <- getParamNode
          let node' = node { X.elementTag = "form"
                           , X.elementAttrs = X.elementAttrs rootNode
                           , X.elementChildren = X.childElements node
                           }
          runNode node'
  withLocalSplices formSplices mempty (C.callTemplate "_csrfForm")

renderExternA
  :: RuntimeAppHandler UserPrefs
renderExternA runtime = do
  node <- getParamNode
  let node' = node {X.elementTag = "a"}
  externTpl <- C.runNode
               . X.setAttribute "target" "_blank"
               . X.setAttribute "rel" "noopener noreferrer" $ node'
  localTpl <- C.runNode node'
  return $ C.yieldRuntime $ do
    p <- runtime
    C.codeGen (if (newExternWindows p)
               then externTpl
               else localTpl)

paramValue
  :: AttrSplice AppHandler
paramValue n = do
  t <- lift $ getParamText $ encodeUtf8 n
  return $ maybe [] ((:[]) . ("value",) . HTML.text) t
