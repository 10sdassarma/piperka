{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Readers (renderReaders) where

import Contravariant.Extras.Contrazip
import Control.Applicative
import Control.Error.Util
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Data.Map.Syntax
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Heist
import Heist.Compiled
import Heist.Compiled.Extra (eitherDeferMap, runConditionalChildren)
import qualified HTMLEntities.Text as HTML
import Network.URI.Encode (encodeText)
import Snap.Snaplet.Hasql

import Application
import Piperka.Error.Splices
import Piperka.Util (getCid)

data ReadersInfo = ReadersInfo
  { title :: Text
  , total :: Int
  , users :: Vector Reader
  }

data Reader = Reader
  { name :: Text
  , activeUser :: Bool
  }

decodeReadersInfo
  :: DE.Row (Vector Reader -> ReadersInfo)
decodeReadersInfo =
  ReadersInfo
  <$> liftA HTML.text (DE.column DE.text)
  <*> liftA fromIntegral (DE.column DE.int4)

decodeUsers
  :: DE.Row Reader
decodeUsers =
  Reader
  <$> DE.column DE.text
  <*> DE.column DE.bool

decodeUsers'
  :: DE.Row Reader
decodeUsers' =
  Reader
  <$> DE.column DE.text
  <*> pure False

queryReadersInfo
  :: (HasHasql m, MonadIO m)
  => Maybe UserID
  -> Int
  -> m (Either QueryError (Maybe ReadersInfo))
queryReadersInfo u c = let c' = fromIntegral c
  in run $ runMaybeT $
  (MaybeT $ statement c' basicInfo) <*>
  (lift $ maybe (statement c' readersUnlog) (flip statement readersLogged . (c',)) u)
  where
    basicInfo = Statement sql1 (EN.param EN.int4)
                (DE.rowMaybe decodeReadersInfo) True
    readersUnlog = Statement sql2 (EN.param EN.int4)
                   (DE.rowVector decodeUsers') True
    readersLogged = Statement sql3
                    (contrazip2 (EN.param EN.int4) (EN.param EN.int4))
                    (DE.rowVector decodeUsers) True
    sql1 = "SELECT title, readers FROM comics WHERE cid=$1 "
    sql2 = "SELECT name FROM users JOIN subscriptions USING (uid) \
           \WHERE cid=$1 AND countme AND privacy=3"
    sql3 = "SELECT name, countme OR users.uid = $2 \
           \FROM users JOIN subscriptions USING (uid) \
           \LEFT JOIN (SELECT cid, interest AS uid, interest \
           \FROM permitted_interest WHERE uid=$2) AS i USING (cid, uid) \
           \WHERE cid=$1 AND ((privacy=3 AND countme) OR \
           \interest IS NOT NULL OR uid=$2) \
           \ORDER BY interest IS NOT NULL DESC, LOWER(name)"

renderReaders
  :: RuntimeAppHandler (Maybe MyData)
renderReaders = eitherDeferMap (lift . getParams) stdSqlErrorSplice
                (withSplices runChildren readersSplices)
  where
    getParams :: Maybe MyData -> AppHandler (Either QueryError (Maybe (Int, ReadersInfo)))
    getParams n = do
      let u = uid <$> n
      maybe (return $ Right Nothing)
        (\c -> (fmap ((fmap . fmap) (c,))) $ queryReadersInfo u c) =<<
        (fmap snd) <$> getCid

readersSplices
  :: Splices (RuntimeAppHandler (Maybe (Int, ReadersInfo)))
readersSplices = do
  "positive" ## mayDeferMap return
    (withSplices runChildren positiveSplices)
  "missing" ## runConditionalChildren . fmap isNothing

positiveSplices
  :: Splices (RuntimeAppHandler (Int, ReadersInfo))
positiveSplices = do
  "title" ## pureSplice . textSplice $ title . snd
  "total" ## pureSplice . textSplice $ T.pack . show . total . snd
  "cid" ## pureSplice . textSplice $ T.pack . show . fst
  "some" ## runConditionalChildren . fmap (not . V.null . users . snd)
  "none" ## runConditionalChildren . fmap (V.null . users . snd)
  "list" ## manyWith runChildren listSplices listAttrSplices . fmap (users . snd)
  "hidden" ## runConditionalChildren . fmap ((>0) . total . snd)

listAttrSplices
  :: Splices (RuntimeSplice AppHandler Reader -> AttrSplice AppHandler)
listAttrSplices = "passive" ## \n _ -> do
  return . bool [("class", "passiveuser")] [] . activeUser =<< n

listSplices
  :: Splices (RuntimeAppHandler Reader)
listSplices = mapV (pureSplice . textSplice) $ do
  "user" ## HTML.text . name
  "userURL" ## encodeText . name
