{-# LANGUAGE OverloadedStrings #-}
module Piperka.Bookmarklet where

import Data.Map.Syntax
import Data.Text (Text)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run)
import Hasql.Statement
import Heist.Compiled
import Heist.Compiled.Extra
import Snap.Snaplet.Hasql

import Application
import Piperka.Error.Splices

renderWithBookmarklet
  :: RuntimeAppHandler MyData
renderWithBookmarklet =
  eitherDeferMap
  (run . getBookmarkletToken . uid)
  stdSqlErrorSplice
  (withSplices runChildren ("token" ## pureSplice . textSplice $ id))

getBookmarkletToken
  :: UserID
  -> Session Text
getBookmarkletToken = flip statement $ Statement
  "SELECT bookmarklet_token FROM users WHERE uid=$1"
  (EN.param EN.int4) (DE.singleRow $ DE.column DE.text) True
