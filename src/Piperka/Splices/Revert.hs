{-# LANGUAGE OverloadedStrings #-}

module Piperka.Splices.Revert (renderRecent) where

import Control.Applicative
import Control.Monad.Trans
import Data.Map.Syntax
import Data.Monoid
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Data.Text as T
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Heist
import Heist.Compiled
import Heist.Compiled.Extra (eitherDeferMap)
import Snap.Snaplet.Hasql (run, HasHasql)

import Application
import Piperka.Error.Splices

renderRecent
  :: RuntimeAppHandler MyData
renderRecent n =
  eitherDeferMap return stdSqlErrorSplice
  (manyWith runChildren renderSplices renderAttrSplices) ((lift . getRecent) =<< n)

renderSplices
  :: Splices (RuntimeAppHandler (Text, Text))
renderSplices = "name" ## pureSplice . textSplice $ snd

renderAttrSplices
  :: Splices (RuntimeSplice AppHandler (Text, Text) -> AttrSplice AppHandler)
renderAttrSplices = "id" ## \n t -> do
  c <- fst <$> n
  let idVal = "recent" <> c
  return $ case t of
    "id" -> [("id", idVal), ("value", c)]
    "for" -> [("for", idVal)]
    _ -> undefined

getRecent
  :: (HasHasql m, MonadIO m)
  => MyData
  -> m (Either QueryError (Vector (Text, Text)))
getRecent p = do
  let u = uid p
  run $ statement u stmt
  where
    stmt = Statement sql (EN.param EN.int4) (DE.rowVector decoder) True
    sql = "SELECT cid, comics.title FROM recent JOIN comics USING (cid) \
          \WHERE recent.uid=$1 ORDER BY recent.used_on DESC"
    decoder = (,)
      <$> (liftA (T.pack . show) (DE.column DE.int4))
      <*> DE.column DE.text
