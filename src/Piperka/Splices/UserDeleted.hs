{-# LANGUAGE OverloadedStrings #-}

module Piperka.Splices.UserDeleted (renderUserDeletedComics) where

import Control.Applicative
import Data.Int
import Data.Map.Syntax
import Data.Text (Text)
import qualified Data.Text as T
import Data.Vector (Vector)
import Data.Time.Clock
import Data.Time.LocalTime
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Heist
import Heist.Compiled
import Heist.Compiled.Extra (eitherDeferMap)
import qualified HTMLEntities.Text as HTML
import Snap.Snaplet.Hasql (run)

import Application
import Piperka.Error.Splices
import Piperka.Util (formatTime')

renderUserDeletedComics
  :: RuntimeAppHandler MyData
renderUserDeletedComics =
  eitherDeferMap (run . fetchUserDeletedComics . uid)
  stdSqlErrorSplice
  (withSplices runChildren
   ("row" ## manyWithSplices runChildren userDeletedSplices))

userDeletedSplices
  :: Splices (RuntimeAppHandler (Int32, Text, Maybe UTCTime, Text))
userDeletedSplices = mapV (pureSplice . textSplice) $ do
  "cid" ## T.pack . show . \(c,_,_,_) -> c
  "title" ## HTML.text . \(_,t,_,_) -> t
  "time" ## maybe "" (T.pack . formatTime') . \(_,_,t,_) -> t
  "reason" ## \(_,_,_,r) -> r

fetchUserDeletedComics
  :: UserID
  -> Session (Vector (Int32, Text, Maybe UTCTime, Text))
fetchUserDeletedComics = flip statement $ Statement sql
  (EN.param EN.int4) (DE.rowVector decoder) True
  where
    decoder = (,,,)
      <$> DE.column DE.int4
      <*> DE.column DE.text
      <*> (liftA (fmap $ localTimeToUTC utc) $ DE.nullableColumn DE.timestamp)
      <*> DE.column DE.text
    sql = "SELECT cid, title, removed_on, reason \
          \FROM graveyard JOIN subscriptions USING (cid) \
          \WHERE uid=$1 ORDER BY removed_on DESC NULLS LAST"
