{-# LANGUAGE OverloadedStrings #-}

module Piperka.Splices.Stats
  ( unreadCountSplice
  , newComicsSplice
  , explicitStats
  ) where

import Control.Monad
import Data.Int
import Data.Map.Syntax
import Data.Monoid
import qualified Data.Text as T
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Heist.Compiled
import Heist.Compiled.Extra (eitherDeferMap)
import Snap.Snaplet.Hasql

import Application
import Piperka.Error.Splices

unreadCountSplice
  :: RuntimeAppHandler (Int32, Int32)
unreadCountSplice = mayDeferMap
  (\a@(n, _) -> return $ guard (n > 0) >> return a)
  (withSplices (callTemplate "/_stats/_unread")
   (mapV (pureSplice . textSplice) $
    ("pages" ## T.pack . show . fst) <> ("comics" ## T.pack . show . snd)))

newComicsSplice
  :: RuntimeAppHandler Int32
newComicsSplice = mayDeferMap
  (\n -> return $ guard (n > 0) >> return n)
  (\n -> withLocalSplices
    ("new" ## (pureSplice . textSplice $ T.pack . show) n)
-- TODO: bring newest cid to this link for cache invalidation
    ("href" ## const $ return [("href", "browse.html?sort=new")])
    (callTemplate "/_stats/_new"))

explicitStats
  :: RuntimeAppHandler MyData
explicitStats = eitherDeferMap (run . flip statement fetchStats . uid)
  stdSqlErrorSplice
  (withSplices runChildren
   (("unread" ## unreadCountSplice . fmap snd) <>
    ("newComics" ## newComicsSplice . fmap fst)))

-- As opposed to the fetch done by UserWithStats auth this doesn't
-- update the seen_comics_before stamp in users table.
fetchStats
  :: Statement UserID (Int32, (Int32, Int32))
fetchStats = Statement sql
  (EN.param EN.int4)
  (DE.singleRow $ (,)
   <$> DE.column DE.int4
   <*> ((,)
        <$> DE.column DE.int4
        <*> DE.column DE.int4)
  ) True
  where
    sql =
      "SELECT (SELECT COUNT(*) FROM comics, users \
      \ WHERE uid=$1 AND comics.added_on > users.seen_comics_before), \
      \total_new, new_in FROM user_unread_stats($1)"
