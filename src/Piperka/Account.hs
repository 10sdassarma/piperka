{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Piperka.Account (renderAccountForm, accountUpdateHandler, getUserEmail) where

import Control.Error.Util (bool)
import Control.Lens
import Control.Monad.State
import Data.Maybe
import Heist.Compiled
import Snap
import Snap.Snaplet.CustomAuth
import Snap.Snaplet.CustomAuth.OAuth2
import Snap.Snaplet.Session

import Application
import Backend ()
import Heist.Compiled.Extra ( eitherDeferMap )
import Piperka.Account.Action
import Piperka.Account.Splices
import Piperka.Account.Types
import Piperka.Account.Query
import Piperka.Error.Splices
import Piperka.OAuth2.Types

renderAccountForm
  :: RuntimeAppHandler MyData
renderAccountForm =
  eitherDeferMap act stdSqlErrorSplice $
  \n' -> withLocalSplices
         (accountSplices n')
         (accountAttrSplices $ (over _1 userAccount . snd) <$> n') $ do
    chl <- runChildren
    attach <- callTemplate "_accountAttach"
    return $ yieldRuntime $ do
      isAttach <- fmap isJust $ lift $ withTop messages $
        getFromSession "p_attach"
      codeGen $ if isAttach then attach else chl
  where
    act usr = do
      err <- lift $ withTop' id $ view accountUpdateError
      let upd :: Either AccountUpdateError MyData = maybe (Right usr) Left err
      accs <- lift $ getAccountSettings $ uid usr
      return $ accs >>= \a -> Right $
        either (\e -> (Just e, (a, usr))) (\u' -> (Nothing, (a, u'))) upd

accountUpdateHandler
  :: AppHandler ()
accountUpdateHandler = do
  full <- maybe pass return =<< withTop auth currentUser
  let usr = user full
  (== (Just "Cancel")) <$> getParam "cancel_attach" >>= flip bool cancelAttach
    (do
        upd <- accountUpdates usr
        case upd of
          Left (Right (NeedsValidation p a)) -> withTop apiAuth $ do
            setUser usr
            let p' = providerName p
            saveAction True p' $ AccountPayload a
            redirectToProvider p'
            return ()
          Left (Left e) -> modify $ set accountUpdateError $ Just e
          Right u -> withTop auth (setUser $ full {user = u})
    )
