{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Piperka.API.Crawler where

import Control.Concurrent
import Control.Error.Util (hush)
import Control.Exception hiding (Handler)
import Control.Lens hiding ((.=))
import Control.Monad.State
import Control.Monad.Trans.Except
import Data.Aeson
import Data.ByteString.Char8 (pack)
import qualified Data.HashMap.Strict as Map
import Data.Int
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.IORef
import Data.Maybe
import Data.Semigroup
import Data.Scientific
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Read (decimal)
import Data.Time.Clock
import Data.Time.Clock.POSIX
import qualified Data.UUID as UUID
import qualified Data.Vector as V
import Hasql.Session hiding (run)
import Network.WebSockets hiding (DataMessage(Text))
import qualified Network.WebSockets as WS
import Network.WebSockets.Snap
import Snap
import Snap.Snaplet.Hasql

import Application
import Crawler.AutoDiscover
import Crawler.Crawl
import Crawler.Types
import Crawler.DB.Fetch
import Crawler.DB.Store
import Crawler.Helpers.Tapas
import Crawler.Parser.Perl.Comm
import Piperka.API.Common
import Piperka.API.Crawler.Query
import Piperka.API.Crawler.Types
import Piperka.Util.DB hiding (cfg)
import qualified Piperka.Util.DB

crawler :: AppHandler ()
crawler = route $
  (map (_2 %~ (runModQueries 2 $))
   [ ("init/:csrf_ham", initCrawl)
   , ("firstNewCid", firstNewCid)
   ])

firstNewCid
  :: a
  -> ExceptT QueryError AppHandler ()
firstNewCid _ = ExceptT (run fetchNewCid) >>=
  lift . writeBS . pack . show

data ExistingInfo = ExistingInfo Value

instance ToJSON ExistingInfo where
  toJSON (ExistingInfo bookmarks) =
    object ["bookmarks" .= bookmarks]

initCrawl
  :: MyData
  -> ExceptT QueryError (Handler App App) ()
initCrawl usr = (lift $ getParam "csrf_ham") >>= \hm ->
  if (hm /= (Just $ UUID.toASCIIBytes $ ucsrfToken usr))
  then lift $ modifyResponse $ setResponseStatus 400 "csrf_ham missing or invalid"
  else do
    ch <- lift $ gets (^. crawlHandle)
    token <- liftIO $ do
      token <- round . (* 1000) <$> getPOSIXTime
      start <- getCurrentTime
      let cr = Crawl start False [] Nothing [] (error "undefined")
      atomicModifyIORef ch ((,()) . (IntMap.insert token cr))
      return token
    lift $ runWebSocketsSnap $ crawlerApp usr token ch

data CrawlControl
  = CmdStart
  { parserType :: Int
  , crawlerConfigParams :: (Text -> Text -> Maybe Text -> Maybe Text -> CrawlerConfig)
                        -> CrawlerConfig
  , startPages :: [Text]
  } | CmdRegular CmdRegular

data CmdRegular
  = CmdStop
  | CmdReset
  | CmdSave Int Int32 [(Int, Int)] Text Text [Int]
  | CmdDiscover Text Text Text Text
  | CmdTapas Text
  | CmdUnknown
  deriving (Show)

instance Show CrawlControl where
  show (CmdStart t f p) = "CmdStart " ++ show t ++ " " ++
                          show (f $ makeCrawlerConfig Nothing "") ++ " " ++ show p
  show (CmdRegular x) = show x

instance FromJSON CrawlControl where
  parseJSON = withObject "CrawlControl" $ \v -> do
    cmd :: Text <- v .: "cmd"
    case cmd of
      "start" -> CmdStart
        <$> v .: "parser_type"
        <*> (do
                (a,b,c,d) <- (,,,)
                  <$> v .: "url_base"
                  <*> v .: "url_tail"
                  <*> v .:? "extra_url"
                  <*> v .:? "extra_data"
                return $ \f -> f a b c d
            )
        <*> v .: "pages"
      "halt" -> return $ CmdRegular CmdStop
      "reset" -> return $ CmdRegular CmdReset
      "save" -> CmdRegular <$>
        (CmdSave
         <$> v .: "cid"
         <*> v .: "firstNew"
         <*> (do
                 (Object o) <- v .: "bookmarkMoves"
                 maybe (fail "no parse") return $ sequence $ map
                   (\(a,b) -> (,)
                     <$> (hush $ fst <$> decimal a)
                     <*> (case b of
                            Number n -> toBoundedInteger n
                            _ -> Nothing)
                   ) $ Map.toList o
             )
         <*> v .: "homepage"
         <*> v .: "fixedHead"
         <*> v .: "deletions"
        )
      "discover" -> CmdRegular <$>
        (CmdDiscover
         <$> v .: "url_base"
         <*> v .: "url_tail"
         <*> v .: "source"
         <*> v .: "target")
      "tapas" -> CmdRegular . CmdTapas
        <$> v .: "url"
      _ -> return $ CmdRegular CmdUnknown

instance ToJSON CrawlControl where
  toJSON (CmdRegular CmdStop) = object ["cmd" .= ("halt" :: Text)]
  toJSON _ = Null

instance WebSocketsData CrawlControl where
  fromDataMessage (WS.Text raw _) = maybe (CmdRegular CmdUnknown) id $ decode raw
  fromDataMessage (WS.Binary _) = CmdRegular CmdUnknown
  fromLazyByteString = maybe (CmdRegular CmdUnknown) id . decode
  toLazyByteString = encode

data CrawlResult
  = CrawlProgress Int CrawlEvent
  | CrawlErr Text
  | CrawlAck Text
  | CrawlDone SiteState
  | CrawlClose
  | CrawlDiscover [Int]
  | CrawlTapasParams Text Text

instance ToJSON CrawlResult where
  toJSON (CrawlProgress ord ev) = object ["ord" .= ord, "ev" .= ev]
  toJSON (CrawlErr err) = object ["err" .= err]
  toJSON (CrawlAck msg) = object ["msg" .= msg]
  toJSON (CrawlDone end) = object ["ev" .= object ["done" .= toJSON end]]
  toJSON CrawlClose = object ["close" .= True]
  toJSON (CrawlDiscover match) = object ["discover" .= toJSON match]
  toJSON (CrawlTapasParams home rss) =
    object ["ev" .= object ["tapas" .= True], "homepage" .= home, "rss" .= rss]

instance WebSocketsData CrawlResult where
  fromDataMessage _ = error "no CrawlMessage fromDataMessage"
  fromLazyByteString = error "no CrawlMessage fromLazyByteString"
  toLazyByteString = encode

-- TODO: crawlerApp should ever use only one token value.  Passing the
-- whole IntMap is too generic.
crawlerApp
  :: MyData
  -> Int
  -> IORef (IntMap Crawl)
  -> ServerApp
crawlerApp _ token ch pending = bracket startParser closeParser $ \pHandle -> do
  conn <- acceptRequest pending
  let adjust f = atomicModifyIORef ch $ (,()) . IntMap.adjust f token
  forever $ do
    msg <- receiveData conn
    let starter cfgSeed storedPages = do
          let cfg = (crawlerConfigParams msg) cfgSeed
              pg = reverse $ startPages msg
              archive = IntMap.fromList
                        [(token, ArchiveEntry (length pg) (V.fromList pg) cfg)]
          when (null storedPages) $ adjust $
            (crawlPages .~ (startPages msg))
          adjust $ (parserId .~ (parserType msg))
          resultVar <- crawl' archive pHandle $ \cid ord ev -> do
            stop <- maybe True (^. crawlHalt) . IntMap.lookup token <$>
                    (liftIO $ readIORef ch)
            noEnd <- gets $ isNothing . (^. endCondition) . (IntMap.! token)
            when stop $
              modify $ IntMap.adjust (endCondition .~ Just EndRequest) cid
            when noEnd $ liftIO $ sendTextData conn $ CrawlProgress ord ev
          void $ forkIO $ takeMVar resultVar >>=
            \res -> do
              let r = res IntMap.! token
              adjust $ crawlResult %~ (<> Just r)
              sendTextData conn $ CrawlDone r
    case msg of
      CmdStart _ _ _ -> (adjust $ crawlHalt .~ False) >>
        (runSession $ fetchParser $ fromIntegral $ parserType msg) >>=
        (either
         (sendTextData conn . CrawlErr . T.pack . show)
         (maybe
          (sendTextData conn $ CrawlErr "no such parser")
          (\cfgSeed ->
             (IntMap.lookup token <$> readIORef ch) >>=
             maybe
             (sendTextData conn $ CrawlErr "parser session disappeared")
             (starter cfgSeed . (^. crawlPages))
          )
         )
        )
      CmdRegular CmdStop -> adjust $ crawlHalt .~ True
      CmdRegular CmdReset -> adjust $
        ((crawlResult .~ Nothing) .
         (crawlPages .~ []))
      CmdRegular (CmdSave cid firstNew bookmoves homepage fixedHead deletions) -> do
        crawlState <- (IntMap.! token) <$> readIORef ch
        let pages = crawlState ^. crawlPages
            -- Any pages manually inserted preceding the actual crawl
            -- need to be inserted.
            prequel :: [(Int32, Int32, Maybe Text)] =
              filter ((>= firstNew) . (^. _2)) $
              zipWith (\ord pg -> (fromIntegral cid, ord, Just pg)) [0..] pages
        maybe
          (sendTextData conn $ CrawlErr "no crawl done")
          (\res -> do
              x <- runTransaction $ do
                -- TODO: faking the original archive for the save is
                -- wrong
                let adjustedRes = originalArchive . archivePages .~
                      (V.fromList $ replicate
                       (fromIntegral firstNew + length prequel) T.empty) $ res
                deleteExistingPages cid firstNew
                moveBookmarks cid bookmoves
                statement prequel savePages
                saveCrawl $ IntMap.fromList [(cid, adjustedRes)]
                makeDeletions (fromIntegral cid) (map fromIntegral deletions)
                updateComicParams cid homepage
                  (if T.null fixedHead then Nothing else Just fixedHead)
                  (crawlState ^. parserId) $
                  res ^. originalArchive ^. cConfig
              sendTextData conn $ either
                (CrawlErr . T.pack . show)
                (const $ CrawlAck "success") x
          ) $ crawlState ^. crawlResult
      CmdRegular (CmdDiscover ub ut source target) -> do
        (autoDiscover Piperka.Util.DB.cfg ub ut source target) >>=
          (either
           (sendTextData conn . CrawlErr . T.pack . show)
           (sendTextData conn . CrawlDiscover))
      CmdRegular (CmdTapas url) -> tapasHelper url >>=
        either
        (sendTextData conn . CrawlErr . T.pack)
        (\(homepage, rss, pages) ->
           sendTextData conn (CrawlTapasParams homepage rss) >>
           (mapM_ (\(ord, pg) -> sendTextData conn $ CrawlProgress ord
                    (ParseEvent (Result pg V.empty)
                     (CrawlTarget False Nothing [] T.empty))) $
             zip [0..] $ V.toList pages)
        )
      _ -> return ()
