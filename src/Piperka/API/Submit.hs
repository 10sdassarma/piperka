{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.API.Submit (receiveSubmit) where

import Contravariant.Extras.Contrazip
import Control.Error.Util
import Control.Lens (set)
import Control.Monad
import Control.Monad.Except (throwError)
import Control.Monad.State
import Control.Monad.Trans.Except
import Data.Aeson hiding (Success)
import Data.Binary.Builder (putStringUtf8, toLazyByteString)
import qualified Data.ByteString.Char8 as B
import Data.ByteString.Lazy (toStrict)
import Data.Functor.Contravariant
import Data.Int
import Data.Maybe
import qualified Data.Map.Strict as M
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Heist.Compiled
import qualified HTMLEntities.Text as HTML
import Network.IP.Addr
import Snap
import Snap.Snaplet.Hasql
import Snap.Snaplet.Heist

import Application
import Piperka.API.Common
import Piperka.API.Submit.Banner
import Piperka.API.Submit.Types
import Piperka.ComicInfo.Epedia (eEntries)
import Piperka.Maint.Sanitize
import Piperka.Util (getCid, getParamInt, rqRemote)

receiveSubmit
  :: AppHandler ()
receiveSubmit = do
  b <- receiveBanner
  formType <- getPostParam "formtype"
  let handle b' = case formType of
        Just "submit" -> handleSubmit b'
        Just "editinfo" -> handleEditInfo b'
        _ -> returnMessage InvalidFormType
  maybe (handle Nothing)
    (either (returnMessage . BValidation) (handle . Just)) b

existCheck
  :: (HasHasql m, MonadIO m)
  => Text
  -> ExceptT SubmitValidation m ()
existCheck t = (maybe (return ()) (throwE . ComicExists)) =<<
  (withExceptT Database $
   ExceptT $ run $ statement t $ Statement sql
   (EN.param EN.text)
   (DE.rowMaybe ((,)
                  <$> (T.pack . show <$> DE.column DE.int4)
                  <*> (HTML.text <$> DE.column DE.text))) True)
  where
    sql = "SELECT cid, title FROM comics WHERE lower(title)=lower($1)"

insertSubmit
  :: (HasHasql m, MonadIO m)
  => Maybe Text
  -> Maybe Text
  -> NetAddr IP
  -> Bool
  -> Maybe Text
  -> Text
  -> Text
  -> Maybe UserID
  -> m (Either QueryError Int32)
insertSubmit first desc ip wantEmail email title home u =
  run $ statement (first, desc, ip, wantEmail, email, title, home, u) $ Statement sql
  (contrazip8
   (EN.nullableParam EN.text)
   (EN.nullableParam EN.text)
   (EN.param EN.inet)
   (EN.param EN.bool)
   (EN.nullableParam EN.text)
   (EN.param EN.text)
   (EN.param EN.text)
   (EN.nullableParam EN.int4))
  (DE.singleRow $ DE.column DE.int4) True
  where
    sql = "INSERT INTO submit (first_page, description, from_ip, \
          \want_email, email, title, homepage, uid) VALUES \
          \($1,$2,$3,$4,$5,$6,$7,$8) RETURNING sid"

returnMessage
  :: SubmitResult
  -> AppHandler ()
returnMessage status = do
  st <- getHeistState
  modify $ set submitResult $ Just status
  msg <- (decodeUtf8 . toStrict . toLazyByteString) <$>
    (maybe (return $ putStringUtf8 $ show status) fst $
     renderTemplate st "submitMessage_")
  let isSuccess = isSubmitSuccess status
      msgField = if isSuccess then "msg" else "errmsg"
      addOk = if isSuccess then ("ok" .= True :) else id
  writeLBS $ encode $ object $ addOk $ [msgField .= msg]

handleSubmit
  :: Maybe Banner
  -> AppHandler ()
handleSubmit b = do
  params <- getParams
  remote <- rqRemote <$> getRequest
  let lookupText n = (hush . decodeUtf8' =<<) . listToMaybe =<< M.lookup n params
  validation <- runExceptT $ do
    [title, url] <- hoistEither $ do
      xs <- forM ["title", "url"]
        (maybe (throwError TitleOrUrl) (return . T.strip) . lookupText)
      when (any T.null xs) $ throwError TitleOrUrl
      return xs
    existCheck title
    return (title, url)
  let submit (title, url) = runMaybeUserQueries $ \u -> do
        sid <- ExceptT $ insertSubmit
          (lookupText "first_page")
          (lookupText "description")
          remote
          ((Just ["1"]) == (M.lookup "want_notify" params))
          (lookupText "email")
          title
          url
          (uid <$> u)
        maybe (return Nothing)
          (\b' -> (ExceptT $ submitBanner sid b') >> return Nothing) b
        insertTagsEpedias sid params
        return (Success SubmissionAccepted)
  returnMessage =<< either (return . SValidation) submit validation

existCheck'
  :: (HasHasql m, MonadIO m)
  => Int32
  -> m (Either QueryError Bool)
existCheck' c = run $ statement c $
  Statement sql (EN.param EN.int4)
  (DE.singleRow $ DE.column DE.bool) True
  where
    sql = "SELECT $1 IN (SELECT cid FROM comics)"

insertEdit
  :: (HasHasql m, MonadIO m)
  => Maybe UserID
  -> Int32
  -> NetAddr IP
  -> Maybe Text
  -> m (Either QueryError Int32)
insertEdit u c ip desc = run $ statement (u, c, ip, desc) $ Statement sql
  (contrazip4
   (EN.nullableParam EN.int4)
   (EN.param EN.int4)
   (EN.param EN.inet)
   (EN.nullableParam EN.text))
  (DE.singleRow $ DE.column DE.int4) True
  where
    sql = "INSERT INTO user_edit (uid, cid, from_ip, description) VALUES \
          \($1, $2, $3, $4) RETURNING sid"

handleEditInfo
  :: Maybe Banner
  -> AppHandler ()
handleEditInfo b = do
  params <- getParams
  remote <- rqRemote <$> getRequest
  let lookupText n = (hush . decodeUtf8' =<<) . listToMaybe =<< M.lookup n params
      description = lookupText "description"
  validation <- runExceptT $ do
    c <- maybe (throwE NoComic) (return . fromIntegral . snd) =<< lift getCid
    either (throwE . Database2) (bool (throwE NoComic) (return ())) =<<
      (lift $ existCheck' c)
    return c
  let submit c = runMaybeUserQueries $ \u -> do
        let isMod = maybe False ((>0) . moderator) u
            validationError =
              encodeUtf8 <$> description >>=
              either (Just . T.pack) (const Nothing) .
              sanitize [ "a", "b", "br", "em", "h4", "h5", "h6"
                       , "i", "li", "ol", "ul", "p", "strong"]
        case (isMod, validationError) of
          (True, Just e) -> return $ ModeratorFailure e
          _ -> submit' c u isMod
      submit' c u isMod = do
        sid <- ExceptT $ insertEdit
          (uid <$> u)
          c
          remote
          description
        maybe (return ()) (ExceptT . submitBanner sid) b
        insertTagsEpedias sid params
        let editSuccess = return $ Success (EditSubmitted c)
        maybe editSuccess
          (\u' -> bool editSuccess
                  (saveEdit u' b c sid remote)
                  isMod) u
  returnMessage =<< either (return . EValidation) submit validation

saveEdit
  :: MyData
  -> Maybe Banner
  -> Int32
  -> Int32
  -> NetAddr IP
  -> ExceptT QueryError AppHandler SubmitResult
saveEdit u b c sid remote = do
  -- If the original submit had a banner and we're accepting it,
  -- transfer its banner to the edit done by the moderator.
  userSid <- lift $ (fromIntegral . snd <$>) <$> getParamInt "user_sid"
  haveUserSidBanner <- maybe (return False)
    (\sid' -> do
        acceptBanner <- lift $ (== (Just "1")) <$> getParam "acceptbanner"
        haveBanner <- if not acceptBanner then return False else (>0) <$>
          (ExceptT $ run $ statement (sid', sid) $ Statement
           "UPDATE submit_banner SET sid=$2 WHERE sid=$1"
           (contrazip2 (EN.param EN.int4) (EN.param EN.int4))
           DE.rowsAffected True)
        ExceptT $ run $ statement sid' $ Statement
          "DELETE FROM user_edit WHERE sid=$1"
          (EN.param EN.int4) DE.unit True
        return haveBanner) userSid
  historySid <- ExceptT $ run $ statement (uid u, c, sid, remote) $ Statement
    "SELECT edit_entry($1, $2, $3, $4)"
    (let e = EN.param EN.int4 in contrazip4 e e e (EN.param EN.inet))
    (DE.singleRow $ DE.column DE.int4) True
  (old, banner) <- if isJust b || haveUserSidBanner
    then do
    ExceptT $ saveFromSubmit sid (Just historySid) c b
    else return (Nothing, Nothing)
  ExceptT $ run $ statement sid $ Statement
    "DELETE FROM user_edit WHERE sid=$1" (EN.param EN.int4) DE.unit True
  if isJust banner then finishBannerSave (old, banner) else return ()
  return $ Success (EditedSuccessfully c)

insertTagsEpedias
  :: (HasHasql m, MonadIO m)
  => Int32
  -> Params
  -> ExceptT QueryError m ()
insertTagsEpedias sid params = do
  let encodeList = EN.param . EN.array . EN.dimension foldl . EN.element
  ExceptT $ run $ statement (sid, tags) $ Statement sql1
    (contrazip2 (EN.param EN.int4) (encodeList EN.int4))
    DE.unit True
  ExceptT $ run $ statement (sid, eEntries params) $ Statement sql2
    (contrazip2 (EN.param EN.int4)
     (unzip >$< contrazip2 (encodeList EN.int2) (encodeList EN.text)))
    DE.unit True
  where
    tags = maybe [] (map (fromIntegral . fst) . catMaybes . map B.readInt) $
      M.lookup "category" params
    sql1 = "INSERT INTO submit_tag (sid, tagid) SELECT $1, tagid FROM \
           \unnest($2) AS tagid"
    sql2 = "INSERT INTO external_entry_submit (sid, epid, entry) \
           \SELECT $1, epid, entry FROM unnest($2, $3) AS u (epid, entry)"
