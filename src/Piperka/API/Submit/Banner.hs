{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Piperka.API.Submit.Banner
  ( Banner
  , BannerError(..)
  , receiveBanner
  , submitBanner
  , saveFromSubmit
  , deleteBanner
  , finishBannerSave
  ) where

import Codec.Picture
import qualified Codec.Picture.Metadata as Meta
import Contravariant.Extras.Contrazip
import Control.Exception
import Control.Monad (join, when)
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import Data.Functor.Contravariant
import Data.Int (Int32)
import Data.Maybe (listToMaybe)
import Data.Monoid
import Data.String (IsString)
import qualified Data.Text as T
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap.Snaplet.Hasql
import Snap.Util.FileUploads
import System.Directory (removeFile)
import System.Posix.Files

import Application
import Piperka.API.Submit.Types

data Format = JPG | PNG | GIF

parseFormat
  :: (Eq a, IsString a)
  => a
  -> Maybe Format
parseFormat "image/jpeg" = Just JPG
parseFormat "image/png" = Just PNG
parseFormat "image/gif" = Just GIF
parseFormat _ = Nothing

type Banner = (Format, ByteString)

formatMime
  :: IsString a
  => Format
  -> a
formatMime JPG = "image/jpeg"
formatMime PNG = "image/png"
formatMime GIF = "image/gif"

formatExtension
  :: IsString a
  => Format
  -> a
formatExtension JPG = "jpg"
formatExtension PNG = "png"
formatExtension GIF = "gif"

partHandler
  :: PartInfo
  -> Either PolicyViolationException FilePath
  -> IO (Maybe (Either BannerError Banner))
partHandler _ errFile = do
  let sz = fmap fileSize . getFileStatus
  size <- either (const $ return Nothing) ((Just <$>) . sz) errFile
  if size == (Just 0) then return Nothing else (Just <$>) $ runExceptT $ do
    file <- either (throwE . Violation) (liftIO . B.readFile) errFile
    meta <- either (const $ throwE NoImage) (return . snd) $
      decodeImageWithMetadata file
    fmt <- case Meta.lookup Meta.Format meta of
      Just Meta.SourceJpeg -> return JPG
      Just Meta.SourceGif -> do
        either (const $ throwE UnknownError)
          (\xs -> when (length xs > 1) (throwE AnimatedImage)) $ decodeGifImages file
        return GIF
      Just Meta.SourcePng -> return PNG
      _ -> throwE InvalidMime
    case (,) <$> Meta.lookup Meta.Width meta <*> Meta.lookup Meta.Height meta of
      Just (468, 60) -> return ()
      Just (w, h) -> throwE $ InvalidDimensions (fromIntegral w) (fromIntegral h)
      _ -> throwE UnknownError
    return (fmt, file)

receiveBanner
  :: AppHandler (Maybe (Either BannerError Banner))
receiveBanner =
  let uploadPolicy = setMaximumNumberOfFormInputs 200 defaultUploadPolicy
      partPolicy part = if partFieldName part == "banner"
        then allowWithMaximumSize (getMaximumFormInputSize uploadPolicy)
        else disallow
  in join . listToMaybe <$> handleFileUploads "tmp" uploadPolicy
     partPolicy partHandler

submitBanner
  :: (HasHasql m, MonadIO m)
  => Int32
  -> Banner
  -> m (Either QueryError ())
submitBanner sid (format, content) = run $ statement (sid, format, content) $
  Statement sql (contrazip3 (EN.param EN.int4)
                 (formatMime >$< EN.param EN.text) (EN.param EN.bytea))
  DE.unit True
  where
    sql = "WITH bd AS (\
          \INSERT INTO banner_data (mime, content) VALUES ($2, $3) RETURNING bdid \
          \) INSERT INTO submit_banner (sid, bdid) SELECT $1, bd.bdid FROM bd \
          \ON CONFLICT (sid) DO UPDATE SET bdid = EXCLUDED.bdid"

saveFromSubmit
  :: (HasHasql m, MonadIO m)
  => Int32
  -> Maybe Int32
  -> Int32
  -> Maybe Banner
  -> m (Either QueryError (Maybe FilePath, Maybe (FilePath, ByteString)))
saveFromSubmit sid historySid c b = run $ do
  banner <- (fmap (\(fmt, bs) -> (show c <> "." <> formatExtension fmt, bs))) <$>
            maybe fetchBanner (return . Just) b
  old <- maybe fetchOld (saveBanner . fst) banner
  return (old, banner)
  where
    fetchBanner = fmap (>>= \(fmt, bs) -> (,bs) <$> fmt) <$>
      statement sid $ Statement
      "SELECT bd.mime, bd.content FROM submit_banner \
      \JOIN banner_data AS bd USING (bdid) WHERE sid=$1"
      (EN.param EN.int4)
      (DE.rowMaybe ((,)
                    <$> (parseFormat <$> DE.column DE.text)
                    <*> DE.column DE.bytea)) True
    saveBanner fileName = statement (sid, c, fileName, historySid) $ Statement
      "WITH old_banner AS (\
      \SELECT file FROM banners WHERE cid=$2 \
      \), insert_history AS (\
      \INSERT INTO submit_banner (sid, bdid) \
      \SELECT $4, bdid FROM banners WHERE cid=$2 AND $4 IS NOT NULL \
      \ON CONFLICT (sid) DO UPDATE SET bdid = EXCLUDED.bdid \
      \), upsert_new AS (\
      \INSERT INTO banners (cid, file, bdid) \
      \SELECT $2, $3, bdid FROM submit_banner WHERE sid=$1 \
      \ON CONFLICT (cid) DO UPDATE SET file = EXCLUDED.file, \
      \bdid = EXCLUDED.bdid) \
      \SELECT file FROM old_banner"
      (contrazip4 (EN.param EN.int4) (EN.param EN.int4)
       (T.pack >$< EN.param EN.text) (EN.nullableParam EN.int4))
      (DE.rowMaybe $ T.unpack <$> DE.column DE.text) True
    fetchOld = maybe
      (statement c $ Statement
        "SELECT file FROM banners WHERE cid=$1"
        (EN.param EN.int4) (DE.rowMaybe $ T.unpack <$> DE.column DE.text) True)
      (\s -> statement (c, s) $ Statement
        "WITH old_banner AS (\
        \SELECT bdid, file FROM banners WHERE cid=$1 \
        \), new_insert AS (\
        \INSERT INTO submit_banner (sid, bdid) SELECT $2, bdid \
        \FROM old_banner) \
        \SELECT file FROM old_banner"
        (contrazip2 (EN.param EN.int4) (EN.param EN.int4))
        (DE.rowMaybe $ T.unpack <$> DE.column DE.text) True) historySid

deleteBanner
  :: (HasHasql m, MonadIO m)
  => Int32
  -> Int32
  -> m (Either QueryError (Maybe FilePath))
deleteBanner c historySid = run $ statement (c, historySid) $ Statement
      "WITH old_banner AS (\
      \INSERT INTO submit_banner (sid, bdid) \
      \SELECT $2, bdid \
      \FROM banners WHERE cid=$1 RETURNING file\
      \), delete_old AS (\
      \DELETE FROM banners WHERE cid=$1) \
      \SELECT * from old_banner"
      (contrazip2 (EN.param EN.int4) (EN.param EN.int4))
      (DE.rowMaybe $ T.unpack <$> DE.column DE.text) True

finishBannerSave
  :: MonadIO m
  => (Maybe FilePath, Maybe (FilePath, ByteString))
  -> m ()
finishBannerSave (old, new) = liftIO $ do
  flip (maybe (return ())) old $ \fileName ->
    catch (removeFile ("/srv/piperka.net/files/banners/" <> fileName))
    (\e -> let _ = e :: IOException in return ())
  flip (maybe (return ())) new $ \(fileName, content) -> do
    B.writeFile ("/srv/piperka.net/files/banners/" <> fileName) content
