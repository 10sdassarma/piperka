{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

{-# OPTIONS -Wno-unused-top-binds #-}

module Piperka.API.SubmitInfo (
    readSubmit
  , readUserEdit
  , dropUserEdit
  , viewSubmitBanner
  ) where

import Codec.Picture
import qualified Codec.Picture.Metadata as Meta
import Control.Applicative
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Data.Aeson
import Data.ByteString (ByteString)
import Data.ByteString.UTF8 (fromString)
import Data.Time.Clock
import Data.Time.LocalTime
import Data.Semigroup
import Data.Text (Text)
import qualified Data.Textual
import GHC.Generics
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Network.IP.Addr
import Snap
import Snap.Snaplet.Hasql

import Application
import Piperka.API.Common
import Piperka.ComicInfo.Epedia
import Piperka.Util (getParamInt)

data UserSubmit = UserSubmit
  { sid :: Int
  , title :: Text
  , homepage :: Text
  , first_page :: Maybe Text
  , description :: Maybe Text
  , submitted_on :: UTCTime
  , user :: Maybe Text
  , want_email :: Bool
  , email :: Maybe Text
  , banner_url :: Maybe Text
  , from_ip :: Text
  , newbanner :: Bool
  } deriving (Generic)

instance ToJSON UserSubmit where
  toEncoding = genericToEncoding defaultOptions

decodeSubmit
  :: DE.Row UserSubmit
decodeSubmit =
  UserSubmit
  <$> (liftA fromIntegral $ DE.column DE.int4)
  <*> DE.column DE.text
  <*> DE.column DE.text
  <*> DE.nullableColumn DE.text
  <*> DE.nullableColumn DE.text
  <*> (liftA (localTimeToUTC utc) $ DE.column DE.timestamp)
  <*> DE.nullableColumn DE.text
  <*> DE.column DE.bool
  <*> DE.nullableColumn DE.text
  <*> DE.nullableColumn DE.text
  <*> (liftA (Data.Textual.toText . netHost) $ DE.column DE.inet)
  <*> DE.column DE.bool

readSubmit
  :: AppHandler ()
readSubmit = do
  s <- maybe (simpleFail 404 "Required parameter sid missing") (return . snd) =<<
       getParamInt "sid"
  writeLBS . encode =<<
    (runModQueries 1 $ const $
     ExceptT $ run $ statement (fromIntegral s) $
     Statement sql (EN.param EN.int4) (DE.rowMaybe decodeSubmit) False)
  where
    sql = "SELECT sid, title, homepage, first_page, description, submitted_on, \
          \name AS user, want_email, submit.email, banner_url, from_ip, \
          \sid in (SELECT sid FROM submit_banner) AS newbanner \
          \FROM submit LEFT JOIN users USING (uid) WHERE sid=$1"

data UserEdit = UserEdit
  { description' :: Maybe Text
  , oldbanner :: Maybe Text
  , newbanner' :: Bool
  , ok :: Bool
  , tags :: [Int]
  , epedias :: [Epedia]
  , origtags :: [Int]
  } deriving (Generic)

instance ToJSON UserEdit where
  toEncoding = genericToEncoding defaultOptions {
    fieldLabelModifier = filter (/= '\'') }

decodeEdit
  :: DE.Row ([Int] -> [Epedia] -> [Int] -> UserEdit)
decodeEdit =
  UserEdit
  <$> DE.nullableColumn DE.text
  <*> DE.nullableColumn DE.text
  <*> DE.column DE.bool
  <*> pure True

readUserEdit
  :: AppHandler ()
readUserEdit = do
  s <- maybe (simpleFail 404 "Required parameter sid missing")
    (return . fromIntegral . snd) =<< getParamInt "sid"
  let run' sql d = ExceptT $ run $ statement s $ Statement sql
                   (EN.param EN.int4) d True
  writeLBS . encode =<<
    (runModQueries 1 $ const $ do
        info <- run' sql1 (DE.rowMaybe decodeEdit)
        let fillInfo info' = do
              newtags <- run' sql2 (DE.rowList (fromIntegral <$> DE.column DE.int4))
              epedias' <- run' sql3 $ DE.rowList decodeEpedia
              oldtags <- run' sql4 (DE.rowList (fromIntegral <$> DE.column DE.int4))
              return $ info' newtags epedias' oldtags
        maybe (return Nothing) ((Just <$>) . fillInfo) info)
  where
    sql1 = "SELECT description, banner_url, sid IN (SELECT sid FROM submit_banner) \
           \FROM user_edit WHERE sid = $1"
    sql2 = "SELECT tagid FROM submit_tag WHERE sid=$1"
    sql3 = "SELECT epid, entry FROM external_entry_submit WHERE sid=$1"
    sql4 = "SELECT tagid FROM comic_tag JOIN user_edit USING (cid) \
           \WHERE sid=$1"

dropUserEdit
  :: AppHandler ()
dropUserEdit = do
  s <- requiredParam "sid" $ \n -> fmap snd <$> getParamInt n
  runModQueries 1 $ const $ do
    lift $ validateCsrf
    ExceptT $ run $ statement (fromIntegral s) $
      Statement sql (EN.param EN.int4) DE.unit False
    lift $ writeLBS $ encode $ object [ "ok" .= True]
  where
    sql = "DELETE FROM user_edit WHERE sid=$1"

-- Storing data files in a database is generally a bad idea but I went
-- with it for the little used moderator interface.
viewSubmitBanner
  :: AppHandler ()
viewSubmitBanner = do
  s <- requiredParam "sid" $ \n -> fmap snd <$> getParamInt n
  (mime, b) <- runModQueries 1 $ const $ do
    b <- ExceptT $ run $ statement (fromIntegral s) $
         Statement sql (EN.param EN.int4)
         (DE.rowMaybe $
          ((,) <$> DE.nullableColumn DE.bytea <*> DE.column DE.bytea)) True
    maybe (lift $ simpleFail 404 "No banner for that submission") return b
  modifyResponse . setHeader "Content-Type" =<< maybe (decodeMime b)
    return mime
  writeBS b
  where
    sql = "SELECT bd.mime, bd.content \
          \FROM submit_banner JOIN banner_data AS bd USING (bdid) WHERE sid=$1"

decodeMime
  :: ByteString
  -> AppHandler ByteString
decodeMime b = either
  (simpleFail 500 . ("Error decoding MIME type " <>) . fromString)
  ((\meta -> case Meta.lookup Meta.Format meta of
       Just Meta.SourceJpeg -> return "image/jpeg"
       Just Meta.SourceGif -> return "image/gif"
       Just Meta.SourcePng -> return "image/png"
       _ -> simpleFail 500 "Error decoding MIME type"
       ) . snd) $
  decodeImageWithMetadata b
