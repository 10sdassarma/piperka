{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}

{-# OPTIONS -Wno-unused-top-binds #-}

module Piperka.API.Archive (dumpArchive) where

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Data.Aeson
import Data.ByteString (ByteString)
import Data.Int
import Data.Text (Text)
import Data.Monoid
import Data.Vector (Vector)
import GHC.Generics
import qualified Hasql.Encoders as EN
import qualified Hasql.Decoders as DE
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.Hasql

import Application
import Piperka.API.Common
import Piperka.Util (getCid)

type Page = (Maybe Text, Int32, [Text], Bool, Maybe Text)

data Archive = Archive {
    url_base :: Text
  , url_tail :: Text
  , fixed_head :: Maybe Text
  , homepage :: Text
  , pages :: Vector Page
  } deriving (Generic)

instance ToJSON Archive where
  toEncoding = genericToEncoding defaultOptions

decodeArchiveInfo :: DE.Row (Vector Page -> Archive)
decodeArchiveInfo =
  Archive
  <$> DE.column DE.text
  <*> DE.column DE.text
  <*> DE.nullableColumn DE.text
  <*> DE.column DE.text

decodePageInfo :: DE.Row Page
decodePageInfo = do
  pageName <- DE.nullableColumn DE.text
  fragments <- DE.column $ DE.array $ DE.dimension replicateM $ DE.element DE.text
  thumb <- DE.column DE.bool
  title <- DE.nullableColumn DE.text
  return (pageName, fromIntegral $ length fragments, fragments, thumb, title)

dumpArchive
  :: ByteString
  -> AppHandler ()
dumpArchive hostname = do
  modifyResponse $
    addHeader "Access-Control-Allow-Origin" ("http://" <> hostname)
  c <- maybe (simpleFail 404 "Required parameter cid missing") return =<<
       fmap (fromIntegral . snd) <$> getCid
  runQueries $ do
    aMain' <- ExceptT $ run $ statement c $ Statement sql
              (EN.param EN.int4) (DE.rowMaybe decodeArchiveInfo) True
    maybe (lift $ simpleFail 404 "No such comic")
      (\aMain ->
         (ExceptT $ run $ statement c $ Statement sql'
          (EN.param EN.int4) (DE.rowVector decodePageInfo) True) >>=
         lift . writeLBS . encode . aMain
      ) aMain'
  where
    sql = "SELECT url_base, url_tail, fixed_head, homepage \
          \FROM comics WHERE cid=$1"
    sql' = "SELECT name, COALESCE(ARRAY_AGG(fragment) FILTER \
           \ (WHERE page_fragments.fragment IS NOT NULL), '{}'), \
           \COALESCE(thumb, false), title \
           \FROM updates AS u LEFT JOIN page_fragments using (cid, ord) \
           \LEFT JOIN screenshot_raw AS s ON (s.cid=u.cid AND s.ord=u.ord AND thumb) \
           \WHERE u.cid=$1 GROUP BY name, u.ord, thumb, title ORDER BY u.ord"
