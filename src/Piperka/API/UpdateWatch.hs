{-# LANGUAGE OverloadedStrings #-}

module Piperka.API.UpdateWatch where

import Control.Concurrent
import Control.Lens
import Control.Monad
import Control.Monad.State
import Data.Text (Text)
import qualified Data.UUID as UUID
import Network.WebSockets as WS
import Network.WebSockets.Snap
import Snap
import System.Random (randomRIO)

import Application
import Piperka.API.Common

updateWatch :: AppHandler ()
updateWatch = route $
  [ ("init/:csrf_ham", runUserQueries (lift . initUpdateWatch))
  ]

initUpdateWatch
  :: MyData
  -> AppHandler ()
initUpdateWatch usr = getParam "csrf_ham" >>= \hm ->
  if hm /= (Just $ UUID.toASCIIBytes $ ucsrfToken usr)
  then modifyResponse $ setResponseStatus 400 "csrf_ham missing or invalid"
  else do
    chan <- gets (^. updateWatcherChan)
    liftIO (dupChan chan) >>= runWebSocketsSnap . watchApp

watchApp
  :: Chan ()
  -> ServerApp
watchApp chan pending = do
  conn <- acceptRequest pending
  forever $ do
    void $ readChan chan
    randomRIO (0, 60*1000000) >>= threadDelay
    sendTextData conn ("true" :: Text)
