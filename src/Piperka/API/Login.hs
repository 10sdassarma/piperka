{-# LANGUAGE OverloadedStrings #-}

module Piperka.API.Login (apiLogin, tokenLogin) where

import Contravariant.Extras.Contrazip
import Control.Monad.Trans
import Data.Aeson
import Data.ByteString.UTF8 (fromString)
import Data.Text (Text)
import Data.Time.Clock
import Data.UUID (UUID, toASCIIBytes, toText)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.Hasql

import Application
import Piperka.API.Common
import Piperka.Auth (setCSRFCookie)
import Piperka.Util (getParamText)

login
  :: (HasHasql m, MonadIO m)
  => Text
  -> Text
  -> m (Either QueryError (Maybe (Text, UUID, UUID)))
login u p = run $ statement (u, p) $ Statement sql
            (contrazip2 (EN.param EN.text) (EN.param EN.text))
            (DE.rowMaybe $ (,,)
             <$> (DE.column DE.text)
             <*> (DE.column DE.uuid)
             <*> (DE.column DE.uuid)) True
  where
    sql = "WITH lg AS (\
          \SELECT name, p_session, csrf_ham FROM auth_login($1, $2) \
          \JOIN users USING (uid)) \
          \, upd AS (INSERT INTO appuser_token SELECT csrf_ham FROM lg) \
          \SELECT name, p_session, csrf_ham FROM lg"

tokenLogin
  :: (HasHasql m, MonadIO m)
  => UUID
  -> m (Either QueryError (Maybe UserID))
tokenLogin u = run $ statement u $ Statement sql (EN.param EN.uuid)
               (DE.singleRow $ DE.nullableColumn DE.int4) True
  where
    sql = "SELECT token_login($1)"

apiLogin
  :: AppHandler ()
apiLogin = do
  usr <- requiredParam "user" getParamText
  passwd <- requiredParam "passwd" getParamText
  lg <- either (simpleFail 403 . fromString . show) return =<<
    login usr passwd
  case lg of
    Nothing ->
      writeLBS $ encode $ object ["errmsg" .= ("Login failed" :: Text)]
    Just (name, sesTok, csrfTok) -> do
      now <- liftIO getCurrentTime
      modifyResponse $ addResponseCookie $
        Cookie sessionCookieName (toASCIIBytes sesTok)
        (Just $ addUTCTime (5*365*24*60*60) now) Nothing (Just "/") True True
      setCSRFCookie csrfTok
      writeLBS $ encode $ object ["name" .= name
                                 , "csrf_ham" .= toText csrfTok]
