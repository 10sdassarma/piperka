{-# LANGUAGE TemplateHaskell #-}

module Piperka.API.Crawler.Types where

import Control.Lens
import Data.Text (Text)
import Data.Time.Clock

import Crawler.Types

data Crawl = Crawl
  { _startTime :: UTCTime
  , _crawlHalt :: Bool
  , _crawlProgress :: [(Int, Int, CrawlEvent)]
  , _crawlResult :: Maybe SiteState
  -- Pages inserted to crawl outside of crawl or inherited from the
  -- current archive in database.
  , _crawlPages :: [Text]
  , _parserId :: Int
  } deriving (Show)

makeLenses ''Crawl

