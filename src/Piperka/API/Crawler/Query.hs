{-# LANGUAGE OverloadedStrings #-}

module Piperka.API.Crawler.Query where

import Contravariant.Extras.Contrazip
import Data.Functor.Contravariant
import Data.Int
import Data.Text (Text)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (sql, run)
import qualified Hasql.Session as S
import Hasql.Statement

import Crawler.Types

-- Where this is used the following page saves know how to make
-- upserts, but this is to make sure that any pages in the archive
-- after the saved portion don't linger.
deleteExistingPages
  :: Int
  -> Int32
  -> Session ()
deleteExistingPages cid firstNew = do
  let delete sql = statement (cid, firstNew) $ Statement sql
        (contrazip2 (fromIntegral >$< EN.param EN.int4)
         (EN.param EN.int4)) DE.unit True
  delete "DELETE FROM updates WHERE cid=$1 AND ord>=$2"
  delete "DELETE FROM page_fragments WHERE cid=$1 AND ord>=$2"

moveBookmarks
  :: Int
  -> [(Int, Int)]
  -> Session ()
moveBookmarks cid moves = do
  S.sql "CREATE TEMPORARY TABLE original_bookmarks \
        \(uid int, ord int)"
  statement cid $ Statement
    "INSERT INTO original_bookmarks SELECT uid, \
    \ord+CASE WHEN subord <= COALESCE(maxsubord, 0) \
    \ THEN 0 ELSE 1 END AS ord FROM subscriptions \
    \LEFT JOIN (SELECT cid, ord, max(subord) AS maxsubord FROM page_fragments \
    \ GROUP BY cid, ord) AS x USING (cid, ord) WHERE cid=$1"
    paramInt DE.unit True
  mapM_ (\(src, tgt) -> case tgt of
            0 -> statement (cid, src) move0
            _ -> statement (cid, src, tgt) move
        ) moves
  where
    paramInt = fromIntegral >$< EN.param EN.int4
    move0 = Statement
      "UPDATE subscriptions SET ord=0, subord=0 \
      \FROM original_bookmarks WHERE cid=$1 AND \
      \subscriptions.uid=original_bookmarks.uid AND original_bookmarks.ord=$2"
      (contrazip2 paramInt paramInt) DE.unit True
    move = Statement
      "UPDATE subscriptions SET ord=subscriptions.ord+($3-$2) \
      \FROM original_bookmarks WHERE cid=$1 \
      \AND subscriptions.uid=original_bookmarks.uid AND original_bookmarks.ord=$2"
      (contrazip3 paramInt paramInt paramInt) DE.unit True

-- This is called after saving the pages from the crawl.  The
-- deletions done on the web interface haven't affected the pages
-- saved from the crawl but instead they're applied afterwards to the
-- database in the order they've been performed.
makeDeletions
  :: Int32
  -> [Int32]
  -> Session ()
makeDeletions cid ds = do
  S.sql "CREATE TEMP TABLE updates_copy (LIKE updates)"
  S.sql "CREATE TEMP TABLE page_fragments_copy (LIKE page_fragments)"
  mapM_
    (\del -> do
        statement (cid, del) $
          Statement sql (contrazip2 (EN.param EN.int4) (EN.param EN.int4))
          DE.unit True
        S.sql "INSERT INTO updates SELECT * FROM updates_copy"
        S.sql "INSERT INTO page_fragments SELECT * FROM page_fragments_copy"
    ) ds
  where
    -- The unique constraint on (cid, ord) prohibits just running
    -- UPDATE updates SET ord=ord-1 WHERE cid=$1 AND ord>$2.
    sql = "WITH clear_u AS (\
          \DELETE FROM updates_copy), \
          \clear_f AS (\
          \DELETE FROM page_fragments_copy), \
          \copy_u AS (\
          \INSERT INTO updates_copy SELECT cid, ord-1, name \
          \FROM updates WHERE cid=$1 AND ord>$2), \
          \copy_f AS (\
          \INSERT INTO page_fragments_copy SELECT cid, ord-1, subord, fragment \
          \FROM page_fragments WHERE cid=$1 AND ord>$2), \
          \del_u AS (\
          \DELETE FROM updates WHERE cid=$1 AND ord>=$2) \
          \DELETE FROM page_fragments WHERE cid=$1 AND ord>=$2"

updateComicParams
  :: Int
  -> Text
  -> Maybe Text
  -> Int
  -> CrawlerConfig
  -> Session ()
updateComicParams cid homepage fixedHead pid cfg = do
  statement (cid, homepage, fixedHead, urlBase cfg, urlTail cfg) $ Statement
    "UPDATE comics SET homepage=$2, fixed_head=$3, url_base=$4, url_tail=$5 \
    \WHERE cid=$1"
    (contrazip5 (fromIntegral >$< EN.param EN.int4)
     (EN.param EN.text) (EN.nullableParam EN.text)
     (EN.param EN.text) (EN.param EN.text))
    DE.unit True
  statement (cid, pid, extraData cfg, extraURL cfg) $ Statement
    "UPDATE crawler_config SET parser_type=$2, extra_data=$3, extra_url=$4, \
    \update_score=0 WHERE cid=$1"
    (contrazip4 (fromIntegral >$< EN.param EN.int4)
     (fromIntegral >$< EN.param EN.int2)
     (EN.nullableParam EN.text) (EN.nullableParam EN.text))
    DE.unit True
