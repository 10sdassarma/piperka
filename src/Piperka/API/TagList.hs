{-# LANGUAGE OverloadedStrings #-}

module Piperka.API.TagList (tagList) where

import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Data.Aeson
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.Hasql

import Application
import Piperka.API.Common
import Piperka.Util (getParamInt)

tagList
  :: AppHandler ()
tagList = do
  t <- maybe (simpleFail 404 "Required parameter tagid missing") return =<<
       fmap snd <$> getParamInt "tagid"
  runQueries $ do
    cids <- ExceptT $ run $ statement (fromIntegral t) $
            Statement sql (EN.param EN.int4)
            (DE.rowVector $ DE.column DE.int4) False
    lift $ writeLBS $ encode $ object [ "cids" .= cids ]
  where
    sql = "SELECT cid FROM comic_tag WHERE tagid=$1"
