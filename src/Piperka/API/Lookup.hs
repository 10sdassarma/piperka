{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}

{-# OPTIONS -Wno-unused-top-binds #-}

module Piperka.API.Lookup (lookupPage) where

import Control.Error.Util
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Aeson
import Data.ByteString (ByteString)
import Data.Semigroup
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8')
import GHC.Generics
import Hasql.Session hiding (run, sql)
import Snap
import Snap.Snaplet.Hasql

import Application
import Piperka.Action.Statements (bookmarkWithDecode, bookmarkFetch, bookmarkAndLogFetch)
import Piperka.API.Common
import Piperka.Util (rqRemote)

data ComicPage = ComicPage
  { cid :: Int
  , title :: Text
  , ord :: Maybe Int
  } deriving (Generic, ToJSON, FromJSON)

lookupPage
  :: ByteString
  -> AppHandler ()
lookupPage hostname = do
  ip <- rqRemote <$> getRequest
  modifyResponse $
    addHeader "Access-Control-Allow-Origin" ("http://" <> hostname)
  url <- maybe (simpleFail 404 "Required parameter url missing") return =<<
         (hush . decodeUtf8' =<<) <$> getParam "url"
  runMaybeUserQueries $ \usr -> do
    res <- fmap (map $ \(c, t, x) -> ComicPage c t $ (\(o, _, _) -> o) <$> x) $
      ExceptT $ run $ bookmarkWithDecode url
      (\u -> statement (u, True, ip, uid <$> usr) bookmarkAndLogFetch)
      (\u -> statement (u, True) bookmarkFetch)
    lift $ writeLBS $ encode res
