{-# LANGUAGE OverloadedStrings #-}

module Piperka.Submission (renderSubmissions) where

import Control.Applicative
import Control.Monad.Trans
import Data.Map.Syntax
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Textual
import Data.Time.Clock
import Data.Time.LocalTime
import Data.Vector (Vector)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Heist.Compiled
import Heist.Compiled.Extra (eitherDeferMap)
import qualified HTMLEntities.Text as HTML
import Network.IP.Addr
import Snap.Snaplet.Hasql

import Application
import Piperka.Error.Splices

data Submission = Submission
  { sid :: Int
  , title :: Text
  , submittedOn :: UTCTime
  , fromIP :: NetAddr IP
  , name :: Maybe Text
  }
  deriving (Show)

decodeSubmission
  :: DE.Row Submission
decodeSubmission =
  Submission
  <$> (fromIntegral <$> DE.column DE.int4)
  <*> liftA HTML.text (DE.column DE.text)
  <*> liftA (localTimeToUTC utc) (DE.column DE.timestamp)
  <*> DE.column DE.inet
  <*> DE.nullableColumn DE.text

readSubmissions
  :: (HasHasql m, MonadIO m)
  => m (Either QueryError (Vector Submission))
readSubmissions = run $ statement () $ Statement sql EN.unit
  (DE.rowVector decodeSubmission) True
  where
    sql = "SELECT sid, title, submitted_on, from_ip, name \
          \FROM submit LEFT JOIN users USING (uid) ORDER BY sid DESC"

renderSubmissions
  :: Splice AppHandler
renderSubmissions =
  eitherDeferMap return stdSqlErrorSplice
  (manyWithSplices runChildren splices) $ lift readSubmissions
  where
    splices = mapV (pureSplice . textSplice) $ do
      "sid" ## T.pack . show . sid
      "title" ## title
      "submittedOn" ## T.pack . show . submittedOn
      "fromIP" ## Data.Textual.toText . netHost . fromIP
      "name" ## maybe "" id . name
