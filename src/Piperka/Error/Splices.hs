{-# LANGUAGE OverloadedStrings #-}

module Piperka.Error.Splices where

import Control.Monad.Trans
import Data.ByteString.UTF8 (fromString)
import Data.Map.Syntax
import Data.Semigroup
import Data.Text (pack)
import Data.Time.Clock
import Hasql.Session
import Heist.Compiled
import Heist
import Snap (logError)

import Application(RuntimeAppHandler)

sqlErrorSplices
  :: Splices (RuntimeAppHandler QueryError)
sqlErrorSplices = do
  "sqlError" ## \e -> return $ yieldRuntimeText $ do
    err <- show <$> e
    stamp <- show <$> liftIO getCurrentTime
    lift $ logError $ fromString $ stamp <> " " <> err
    return $ pack stamp

stdSqlErrorSplice
  :: RuntimeAppHandler QueryError
stdSqlErrorSplice = withSplices (callTemplate "_sqlErr") sqlErrorSplices
