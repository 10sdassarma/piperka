<h:piperka title="Lost password">
  <h2>Lost password</h2>
  <p>
    If you have lost your password, we can email you a hash with which
    you can get a new password.  Enter your user name and email to
    proceed.
  </p>
  <form method="post" action="lost2.html">
    <p>User: <input type="text" name="user_name" size="40"></p>
    <p>Email: <input type="text" name="email"></p>
    <p><input type="submit" name="action" value="Send me a recovery link"/></p>
  </form>
</h:piperka>
