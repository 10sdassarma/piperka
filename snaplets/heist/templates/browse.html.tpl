<h:piperka title="All comics">
  <h2>All comics</h2>
  <h:listing mode="Browse">
    <h:sortOptions/>
    <div class="qsearch_container">
      <h:alphabetIndex/>
      <h:qSearch/>
      <h:hilightButton/>
    </div>
  </h:listing>
</h:piperka>
