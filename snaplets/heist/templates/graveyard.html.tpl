<h:piperka title="Removed comics">
  <h2>Removed comics</h2>
  <p>
    Domains expire, comics get taken offline or are made impossible to
    index.  Such comics end up listed on this page.
    <h:ifLoggedIn>
      For a list of formerly listed comics that you were subscribed
      to, see the <a href="/my_removed.html">My removed comics</a>
      page.
    </h:ifLoggedIn>
  </p>
  <h:listing mode="Graveyard">
    <h:hilightButton/>
  </h:listing>
</h:piperka>
