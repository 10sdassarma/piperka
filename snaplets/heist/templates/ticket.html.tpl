<h:piperka title="Open a ticket">
  <h:ticket>
    <exists>
      <h2>Open a new ticket on <h:title/></h2>
      <p>
	This form is for reporting about failing updates and dead
	comics.  If you'd like to submit a change to the info page
	then please use the edit info form instead.
      </p>
      <p>
	Please fill out the fields below and add a comment about
	anything that you think would help in resolving the issue.
	Like if there was a reboot.
      </p>
      <div id="ticket">
	<h:live>
	  <ul>
	    <li><a href="#tabs-1">Site or title changes</a></li>
	    <li><a href="#tabs-2">Crawler stuck</a></li>
	    <li><a href="#tabs-3">Removals</a></li>
	  </ul>
	  <div id="tabs-1">
	    <form method="POST">
	      <input type="hidden" name="type" value="stale"/>
	      <input type="hidden" name="cid" value="${h:cid}"/>
	      <input type="hidden" name="jsok" value=""/>
	      <p>
		Use this if the archive structure has changed or the
		comic has moved to a new site, or there have been a
		number of pages removed from the archive and they need
		to be removed from Piperka's end.
	      </p>
	      Old data
	      <table>
		<tr>
		  <th>First page</th>
		  <td><a target="_blank" href="${h:firstPage}"><h:firstPage/></a></td>
		</tr>
		<tr>
		  <th>Last known page</th>
		  <td><a target="_blank" href="${h:lastPage}"><h:lastPage/></a></td>
		</tr>
		<tr>
		  <th>Homepage</th>
		  <td><a target="_blank" href="${h:homePage}"><h:homePage/></a></td>
		</tr>
	      </table>
	      New data (leave blank if no change necessary)
	      <table>
		<tr>
		  <th>First page</th>
		  <td><input type="text" name="first_page" size="60"/></td>
		</tr>
		<tr>
		  <th>Homepage</th>
		  <td><input type="text" name="homepage" size="60"/></td>
		</tr>
		<tr>
		  <th>Title change</th>
		  <td><input type="text" name="title" size="60"/></td>
		</tr>
	      </table>
	      <p>
		Comment
		<textarea name="comment" cols="80" rows="10"/>
	      </p>
	      <p class="noscript">
		Submitting tickets requires JavaScript.  Please enable
		it.
	      </p>
	      <p class="script show">
		<input type="submit" value="Submit"/>
	      </p>
	    </form>
	  </div>
	  <div id="tabs-2">
	    <form method="POST">
	      <input type="hidden" name="type" value="stuck"/>
	      <input type="hidden" name="cid" value="${h:cid}"/>
	      <input type="hidden" name="jsok" value=""/>
	      <p>
		Please select one.  If the archive structure or web
	        site have changed, use the site change form instead.
	      </p>
	      <ul>
		<li>
		  <input type="radio" id="stuck1" name="stuck" value="1" required/>
		  <label for="stuck1">Crawler stuck on nonexistent page</label>
		</li>
		<li>
		  <input type="radio" id="stuck2" name="stuck" value="2"/>
		  <label for="stuck2">No apparent reason</label>
		</li>
	      </ul>
	      <p>
		Comment
		<textarea name="comment" cols="80" rows="10"/>
	      </p>
	      <p class="noscript">
		Submitting tickets requires JavaScript.  Please enable
		it.
	      </p>
	      <p class="script show">
		<input type="submit" value="Submit"/>
	      </p>
	    </form>
	  </div>
	  <div id="tabs-3">
	    <form method="POST">
	      <input type="hidden" name="type" value="gone"/>
	      <input type="hidden" name="cid" value="${h:cid}"/>
	      <input type="hidden" name="jsok" value=""/>
	      Please select one.
	      <ul>
		<li>
		  <input type="radio" id="gone1" name="gone" value="1" required/>
		  <label for="gone1">Vanished with no trace</label>
		</li>
		<li>
		  <input type="radio" id="gone2" name="gone" value="2"/>
		  <label for="gone2">Online archive has been removed</label>
		</li>
	      </ul>
	      <p>
		Comment
		<textarea name="comment" cols="80" rows="10"/>
	      </p>
	      <p class="noscript">
		Submitting tickets requires JavaScript.  Please enable
		it.
	      </p>
	      <p class="script show">
		<input type="submit" value="Submit"/>
	      </p>
	    </form>
	  </div>
	</h:live>
	<h:dead>
	  <form method="POST">
	    <input type="hidden" name="type" value="restore"/>
	    <input type="hidden" name="cid" value="${h:cid}"/>
	    <input type="hidden" name="jsok" value=""/>
	    <p>
	      The comic used to be listed on Piperka, but it was
	      removed.  Reason given: <h:removalReason/>
	    </p>
	    <p>
	      Please don't open tickets to update the removal reasons
	      but only to inform that comics have reappeared or the
	      reason for their removal was invalid or no longer
	      applies.  The reason given may be valid at the time of
	      removal but that's all it is.
	    </p>
	    <p>
	      <textarea name="comment" cols="80" rows="10"/>
	    </p>
	    <p class="noscript">
	      Submitting tickets requires JavaScript.  Please enable
	      it.
	    </p>
	    <p class="script show">
	      <input type="submit" value="Submit"/>
	    </p>
	  </form>
	</h:dead>
      </div>
    </exists>
    <submitted>
      <h2>Submitted</h2>
      <p>
	Thank you for your report.
      </p>
      <p>
	I'll have to warn you that fixes to crawler issues may not
	happen all too timely currently.  I'm working on it.  But
	having tickets attached to comics instead of having people
	send me email is an improvement, already.
      </p>
      <p>
	If you're in a hurry, feel free to also send me one.
      </p>
    </submitted>
    <missing>
      <h2>Nothing found</h2>
      <p>
	No such comic.  CID missing, unused or for some reason the
	entry has been removed completely (most likely due to being a
	duplicate entry).
      </p>
    </missing>
  </h:ticket>
</h:piperka>
