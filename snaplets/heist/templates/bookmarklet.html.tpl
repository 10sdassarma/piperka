<h:piperka title="Bookmarklets">
  <h2>Piperka bookmarklets</h2>
  <h:ifLoggedIn>
    <h:withBookmarklet>
      <p>
	One way of using Piperka is with a bookmarklet.  With it, you
	can just navigate a comic archive as usual and click on a
	bookmark on your browser to take you to Piperka and set the
	bookmark on Piperka on the same step.
      </p>
      <h3>Bookmarking bookmarklet</h3>
      <p>
	To protect your user data this bookmarklet is unique to you.
	Just drag
	<a href="javascript:location.href='https://piperka.net/updates.html?user_token=${h:token}&amp;bookmark='+encodeURIComponent(location.href)">Piperka
	  bookmarklet</a> to your browser's bookmarks.  Alternatively,
	  this is the same bookmark as text:
      </p>
      <textarea cols="140" rows="2" readonly>
javascript:location.href='https://piperka.net/updates.html?user_token=<h:token/>&amp;bookmark='+encodeURIComponent(location.href)
      </textarea>
      <h3>Reader bookmarklet</h3>
      <p>
	The following bookmarklet doesn't directly modify your data
	and it's as such a bit simpler.  It takes you
	to <a href="http://piperka.net/reader/">Piperka Reader</a> and
	tries to match the page you're on with an archive page.  Drag
	this to your browser's
	bookmarks: <a href="javascript:location.href='http://piperka.net/reader/?url='+encodeURIComponent(location.href)">Piperka
	Reader</a>.
      </p>
      <textarea cols="140" rows="2" readonly>
javascript:location.href='http://piperka.net/reader/?url='+encodeURIComponent(location.href)
      </textarea>
    </h:withBookmarklet>
  </h:ifLoggedIn>
  <h:ifLoggedOut>
    <p>
      This page would have instructions for setting a bookmarklet for
      setting bookmarks with a single click in a browser on a web
      comic's archives.  Log in to see what it's about.
    </p>
  </h:ifLoggedOut>
</h:piperka>
