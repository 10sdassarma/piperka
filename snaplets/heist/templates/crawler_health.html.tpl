<h:piperka ads="False" title="Crawler health check">
  <h:ifMod>
    <h2>Crawler health check</h2>
    <table>
      <thead>
	<tr>
	  <th>CID</th>
	  <th>Title</th>
	  <th>Origin</th>
	  <th>Error</th>
	  <th>Last success</th>
	</tr>
      </thead>
      <tbody>
	<h:healthCrawlerFailures>
	  <tr>
	    <td><h:cid/></td>
	    <td><a href="info.html?cid=${h:cid}"><h:title/></a></td>
	    <td>
	      <h:hasOrigin>
		<a title="${h:origin}" href="${h:origin}" target="origin"><h:originText/></a>
	      </h:hasOrigin>
	    </td>
	    <td title="${h:error}"><h:errorAbbrev/></td>
	    <h:hasLastSuccess>
	      <td><h:lastSuccess/></td>
	    </h:hasLastSuccess>
	  </tr>
	</h:healthCrawlerFailures>
      </tbody>
    </table>
  </h:ifMod>
</h:piperka>
