<h:piperka title="Support">
  <h2>Support Piperka</h2>
  <p>
    I may have used a plural pronoun somewhere on Piperka but it's
    only for effect, it's nearly all done by just me, Kari Pahula.  If
    you like what Piperka is about, there are ways you can help it.
  </p>
  <p>
    Running Piperka costs money and I put in a lot of work towards it,
    if you don't mind me saying.  Though I want to make it clear that
    using Piperka is and will remain free and I won't think any less
    of you if you never contribute anything.  I won't consider putting
    any site features behind a pay wall.  I'm not offering any
    specific returns, rewards or services if you choose to donate but
    if you like what I'm doing and would like me to do more of it then
    please consider.
  </p>
  <h3>Donations</h3>
  <p>
    You can find me
    on <a href="https://www.patreon.com/kaol">Patreon</a>
    or <a href="https://liberapay.com/kaol">Liberapay</a>.  I
    recommend Liberapay since they incur fewer fees and transaction
    costs.  Patreon requires two layers of transaction fees and a 5%
    share for themselves on top of that and it all adds up to around
    15%.  The monthly sum they show on my Patron page is not the
    amount I'll be receiving and I've no use for their reward
    features.
    <a href="https://liberapay.com/about/faq">Liberapay</a> has fewer
    transaction fees all around and they take no mandatory cut.
    Unlike Patreon they take any payments up front and keep a wallet
    on their end for your funds.  And they allow donations in the
    cents range.  Yay?  More significantly, they won't take VAT from
    EU residents.  I'm not selling anything since any service I
    provide is available equally if you pay nothing.  It's not about
    avoiding taxes either, I do report this as taxable income.
  </p>
  <p>
    I'm grateful if you use either but it's just that with one of them
    more of your donations reach me.
  </p>
  <p>
    <script src="https://liberapay.com/kaol/widgets/button.js"></script>
    <noscript><a href="https://liberapay.com/kaol/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>
    <a href="https://www.patreon.com/bePatron?u=3993722" data-patreon-widget-type="become-patron-button">Become a Patron!</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>
  </p>
  <h3>Affiliate programs</h3>
  <p>
    Patreon has a <a href="https://patreon.com/invite/wkoryo">referral
    program</a>.  Joining them via the invite link and getting patrons
    would get both of us a bonus.  I gave a great sales pitch for
    them, didn't I.
  </p>
  <h3>Other</h3>
  <p>
    The usual: If you find Piperka useful, tell your friends about it.
    Piperka's source code is open source and I'm sure I could think of
    something for you to do if you're up to it and not averse of
    Haskell.  If there's some particular feature you'd like then I may
    be persuaded to add it if you code
    it.  <a href="https://gitlab.com/piperka">The source code</a> is
    hosted on GitLab.
  </p>
  <p>
    Finally, a word about Finnish law.
  </p>
  <div lang="fi">
    <p>
      Lopuksi sananen rahankeräyslaista, kun se usein nousee esiin
      nettikeskusteluissa verkkosivujen lahjoitusmahdollisuuksista, ja
      siitä miten minä sitä tulkitsen.  Siitä huolimatta että kerään
      vastikkeetonta rahaa, olen sitä mieltä että rahankeräyslaki ei
      koske Piperkaa, koska lain määritelmässä puhutaan yleisöön, eli
      ennalta määrittelemättömään joukkoon ihmisiä kohdistuvasta
      toiminnasta.  Jotain kickstartermaista kampanjointia välttäisin
      kun sen luonne olisi enemmän ilmiön herättelyä ja uusien
      ihmisten tavoittelua, mutta se että löydyn hakutoiminnolla
      parilta sivustolta ja pelkästään käyttäjille näytettävän linkin
      takaa omalta sivustoltani nähdäkseni tuskin täyttäisi tätä
      ehtoa.
    </p>
    <p>
      <a href="https://effi.org/blog/2009-02-10-Tapani-Tarvainen.html">Electronic
      Frontier Finland ry</a> on omalta osaltaan joutunut
      rahankeräyslain vuoksi menemään hovioikeuteen asti missä
      todettiin että verkkosivuilla lahjoitusmahdollisuuden
      tarjoaminen ei riitä rahankeräysrikoksen tunnusmerkiksi.  Suomen
      lainsäädäntö ei ole niin ennakkotapauspainotteinen kuin jossain
      muualla mutta laskisin tämän silti tukevan omaa tulkintaani.
    </p>
  </div>
</h:piperka>
