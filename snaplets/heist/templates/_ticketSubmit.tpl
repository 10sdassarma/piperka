<h:when case="stale">
  <h3>Site or title change</h3>
  <h:ifMod level="2">
    <a target="_blank" href="mod_crawler.html?cid=${h:cid}&firstPage=${h:firstPageEnc}">crawler</a>
  </h:ifMod>
  <table>
    <tr>
      <th>Title</th>
      <td><h:title/></td>
    </tr>
    <tr>
      <th>First page</th>
      <td><h:firstPage/></td>
    </tr>
    <tr>
      <th>Homepage</th>
      <td><h:homePage/></td>
    </tr>
  </table>
</h:when>
<h:when case="stuck">
  <h:ifMod level="2">
    <a target="_blank" href="mod_crawler.html?cid=${h:cid}">crawler</a>
  </h:ifMod>
  <h3>Crawler stuck</h3>
  <p>
    <h:reason case="1">Crawler stuck on nonexistent page</h:reason>
    <h:reason case="2">No apparent reason</h:reason>
  </p>
</h:when>
<h:when case="gone">
  <h:ifMod level="2">
    <a target="_blank" href="mod_crawler.html?cid=${h:cid}">crawler</a>
  </h:ifMod>
  <h3>Removal</h3>
  <p>
    <h:reason case="1">Vanished with no trace</h:reason>
    <h:reason case="2">Online archive has been removed</h:reason>
  </p>
</h:when>
<h:when case="restore">
  <h3>Restoral</h3>
</h:when>
