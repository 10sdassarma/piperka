<h:piperka title="Your followers">
  <h:ifLoggedIn>
    <h2>Your followers</h2>
    <h:followers>
      <h:haveFollowers>
	<h:privacy level="Private">
	  <p>
	    Your current privacy setting prevents anyone else from
	    seeing your profile.
	  </p>
	</h:privacy>
	<table class="followcheck" id="followers">
	  <tr>
	    <th>Name</th>
	    <h:privacy level="Friends">
	      <th>Permission</th>
	    </h:privacy>
	  </tr>
	  <h:followers>
	    <tr>
	      <td>
		<h:followerPublic>
		  <h:activeFollower>
		    <a href="profile.html?name=${h:followerURL}">
		      <h:followerName/>
		    </a>
		  </h:activeFollower>
		  <h:activeFollower check="False">
		    <a href="profile.html?name=${h:followerURL}"
		       class="permissiononly" title="Not currently following you, but still has your permission.">
		       <h:followerName/>
		    </a>
		  </h:activeFollower>
		</h:followerPublic>
		<h:followerPrivate>
		  <span title="The user has set his/hers profile
			       private."><h:followerName/></span>
		</h:followerPrivate>
	      </td>
	      <h:privacy level="Friends">
		<td>
		  <input type="checkbox" id="fp-${h:i}"
			 name="followee" value="${h:followerURL}"
			 h:checked=""/>
		  <label for="fp-${h:i}">Permit seeing profile</label>
		</td>
	      </h:privacy>
	    </tr>
	  </h:followers>
	</table>
      </h:haveFollowers>
      <h:haveFollowers check="False">
	Sorry, nobody's interested in what you read.
      </h:haveFollowers>
      <h2>Who you follow</h2>
      <h:isFollowing>
	<table id="followee">
	  <tr>
	    <th>Name</th>
	  </tr>
	  <h:followees>
	    <tr>
	      <td>
		<a href="profile.html?name=${h:followeeURL}"><h:followeeName/></a>
	      </td>
	      <td>
		<h:ifPending>(pending)</h:ifPending>
		<button name="${h:followeeURL}">Stop following</button>
	      </td>
	    </tr>
	  </h:followees>
	</table>
      </h:isFollowing>
      <h:isFollowing check="False">
	You aren't following anyone.
      </h:isFollowing>
    </h:followers>
  </h:ifLoggedIn>
  <h:ifLoggedOut>
    This page has a list of people who a user follows and his or hers
    followers.  That is, people who are interested in others' comic
    picks.  But you aren't logged in so there's nothing here.
  </h:ifLoggedOut>
</h:piperka>
