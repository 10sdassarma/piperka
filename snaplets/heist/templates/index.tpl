<h:piperka>
  <h2>Welcome.</h2>
  <p>
    This is a webcomic tracking and bookmarking service.
  </p>
  <p>
    You might find this site useful if you like to read many
    webcomics, like <h:kaolSubs/> as your humble web master does.
  </p>
</h:piperka>
