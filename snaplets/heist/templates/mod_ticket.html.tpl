<h:piperka ads="False" title="Tickets">
  <h:ifLoggedIn>
    <h:ifMod level="2">
      <h:ticketList>
	<h:resolved>
	  <h3>Resolved tickets</h3>
	</h:resolved>
	<table>
	  <tr>
	    <th>CID</th>
	    <th>Title</th>
	    <th>Stamp</th>
	    <th>Count</th>
	  </tr>
	  <h:comics>
	    <tr data-cid="${h:cid}">
	      <td><h:cid/></td>
	      <h:notResolved>
		<td><a h:class="" href="mod_ticket.html?cid=${h:cid}"><h:title/></a></td>
	      </h:notResolved>
	      <h:resolved>
		<td><a h:class="" href="mod_ticket.html?resolved=1&cid=${h:cid}"><h:title/></a></td>
	      </h:resolved>
	      <td><h:stamp/></td>
	      <td><h:count/></td>
	    </tr>
	  </h:comics>
	</table>
      </h:ticketList>
      <h:ticketDetail>
	<p>
	  <a href="/mod_ticket.html">Ticket list</a>
	</p>
	<p>CID: <h:cid/> <a href="mod_morgue.html?cid=${h:cid}">morgue</a></p>
	<div id="comicinfo"><h:comicInfo/></div>
	<p>
	  Crawler type: <h:parserType/>
	  Update score: <h:updateScore/>
	  Update value: <h:updateValue/>
	</p>
	<p>
	  Bookmark regexp: <h:bookmarkRegexp/>
	  Extra URL: <h:extraURL/>
	  Extra data: <h:extraData/>
	</p>
	<form method="POST" action="/mod_ticket.html">
	  <h:notResolved>
	    <input type="hidden" name="cid" value="${h:cid}"/>
	    <input type="hidden" name="close_ticket" value="1"/>
	    <label>
	      <input type="radio" name="to_close" value="all" checked/>
	      Reply to all
	    </label>
	  </h:notResolved>
	  <h:tickets>
	    <div>
	      <p>
		<label>
		  <h:notResolved>
		    <input type="radio" name="to_close" value="${h:tid}"/>
		  </h:notResolved>
		  TID: <h:tid/>
		</label>
		UID: <h:uid/>
	      </p>
	      <p>Stamp: <h:stamp/></p>
	      <h:resolved>
		<h3>Resolved</h3>
		<p>
		  At: <h:stamp/>
		</p>
		<p>
		  <h:msg/>
		</p>
	      </h:resolved>
	      <h:ticketSubmit/>
	      <textarea readonly cols="80" rows="5"><h:comment/></textarea>
	    </div>
	  </h:tickets>
	  <h:notResolved>
	    <h3>Resolve</h3>
	    <textarea name="resolve_msg" cols="80" rows="5"/>
	    <p>
	      <input type="submit" value="Resolve"/>
	    </p>
	  </h:notResolved>
	</form>
      </h:ticketDetail>
    </h:ifMod>
  </h:ifLoggedIn>
  <h:notMod level="2">
    <h2>403 Go away</h2>
    <p>
      This page is meant for admins, not for mere mortals.
    </p>
  </h:notMod>
</h:piperka>
