<h:piperka title="Recommended comics">
  <h2>Recommended comics</h2>
  <h:ifLoggedOut>
    <p>
      This page would show personal recommendations.  Log in to see
      your recommendations.
    </p>
  </h:ifLoggedOut>
  <h:ifLoggedIn>
    <h:recommend>
      <h:tooFew>
	<p>
	  Please subscribe to at least 5 comics to help the
	  recommendation algorithm.
	</p>
      </h:tooFew>
      <h:canRecommend>
	<h:error>
	  <p>
	    An error occurred while generating recommendations.
	    Please try again in a few minutes and let us know if the
	    problem persists.
	  </p>
	</h:error>
	<h:results>
	  <p>
	    Recommendations are recomputed every minute.
	  </p>
	  <h:listing mode="Recommend"/>
	</h:results>
      </h:canRecommend>
    </h:recommend>
  </h:ifLoggedIn>
</h:piperka>
