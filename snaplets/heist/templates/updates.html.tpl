<h:piperka title="Updates">
  <h:ifLoggedOut>
    <h2>Why am I seeing this page?</h2>
    <p>
      You are most likely a webcomic author following site statistics.
      This page has bookmark links for logged in users.

      Check the <a href="help.html">help</a> or

      <a href="example.html">example</a> page to see what Piperka is
      about.
    </p>
  </h:ifLoggedOut>
  <h:ifLoggedIn>
    <h:ifMinimal>
      <h:explicitStats>
	<span class="scripthidden" id="newin"><h:unread/></span>
	<span class="scripthidden" id="newcomics"><h:newComics/></span>
      </h:explicitStats>
    </h:ifMinimal>
    <h2>Set bookmark</h2>
    <h:ifMod>
      <h:onlyWithStats>
	<h:modStats>
	  <h:nag>
	    <p>
	      It's <h:modDays/> days since you last
	      <a href="/moderate.html">moderated</a>.  Pretty please?
            </p>
	  </h:nag>
	</h:modStats>
      </h:onlyWithStats>
    </h:ifMod>
    <form method="post" action="updates.html">
      <input type="hidden" name="csrf_ham" value="${h:csrf}"/>
      <p>URL: <input type="text" name="bookmark"/>
	<button name="action" value="set_bookmark">Set</button>
	<label for="wantbookmarkhere">Set bookmark here</label>
	<input type="checkbox" id="wantbookmarkhere" name="wantbookmarkhere"/>
	<a href="help.html#bookmarkhere" rel="help">?</a>
	<a href="bookmarklet.html">Get a bookmarklet</a>
      </p>
    </form>
    <p>
      <a href="revert.html">Revert</a> updates.
      <span class="script">
	|
	<label for="openinreader">Open comic in Piperka Reader</label>
	<input type="checkbox" id="openinreader"/>
	| <a href="recommend.html">View Piperka's recommendations</a>
	<span id="updatewatch" class="scripthidden">
	  |
	  <label for="updatewatch-toggle">Watch updates</label>
	  <input type="checkbox" id="updatewatch-toggle"/>
	  <span class="updatewatch-message" id="updatewatch-msg-upd">Updating...</span>
	  <span class="updatewatch-message" id="updatewatch-msg-reconnecting">Reconnecting...</span>
	  <span class="updatewatch-message error" id="updatewatch-msg-connfailed">
	    Watch failed.  Toggle to manually try reconnecting or
	    just reload the page.
	  </span>
	</span>
      </span>
    </p>
    <h2>Updated comics</h2>
    <h:updateStatus/>
    <h:listing mode="Update"/>
  </h:ifLoggedIn>
</h:piperka>
