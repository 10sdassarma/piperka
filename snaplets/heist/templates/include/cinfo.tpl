<h:comicInfo>
  <h:banner>
    <p>
      <img src="${h:bannerUrl}"/>
    </p>
  </h:banner>
  <h2><h:title/></h2>
  <p>
    Subscriptions: <h:readersLink><h:subscriptions/></h:readersLink>
  </p>
  <p>
    Total pages:
    <h:ifPage name="info.html">
      <a href="javascript::" id="info-archive">
	<h:hasFragments><h:fragmentCount/>/</h:hasFragments><h:pageCount><i>unknown</i></h:pageCount>
      </a>
    </h:ifPage>
    <h:ifPage name="info.html" not="True">
      <h:hasFragments><h:fragmentCount/>/</h:hasFragments><h:pageCount><i>unknown</i></h:pageCount>
    </h:ifPage>
    <h:ifKnowPages>
      <true>
	| <h:externA href="${h:firstPageUrl}">First page</h:externA>
	| <h:externA href="${h:lastPageUrl}">Last known page</h:externA>
	<h:ifFixedHead>
	  (excluding front page)
	</h:ifFixedHead>
      </true>
      <false>
	| No pages in index
      </false>
    </h:ifKnowPages>
  </p>
  <p>
    Homepage: <h:externA href="${h:homepage}"><h:homepageText/></h:externA>
  </p>
  <h:ifExternLinks>
    <p>
      This comic on:
      <h:externLink>
	<h:externA href="${h:url}" title="${h:description}"><h:siteName/></h:externA>
      </h:externLink>
    </p>
  </h:ifExternLinks>
  <h:ifAddDate>
    <p>
      Added on: <h:addDate/>
    </p>
  </h:ifAddDate>
  <h:dead>
    <p>
      Removed on: <h:removeDate/>
    </p>
    <p>
      Reason given: <i><h:reason/></i>
    </p>
  </h:dead>
  <p>
    Categories:
    <h:categories>
      <span h:description="" h:class=""><h:name/></span>
    </h:categories>
  </p>
  <div>
    <h:description/>
  </div>
  <h:ifPage name="info.html">
    <div id="archivePages" class="script">
      <span id="currentpagemarker" class="marker">Viewing</span>
      <span id="bookmarkmarker" class="marker">Bookmark</span>
      <div id="archivedialog" title="Archive">
	<table>
	  <thead>
	    <tr>
	      <th>#</th>
	      <th class="page">Page</th>
	    </tr>
	  </thead>
	  <tbody/>
	</table>
      </div>
      <div id="thumbdialog" title="Thumbnails">
      </div>
    </div>
  </h:ifPage>
</h:comicInfo>
