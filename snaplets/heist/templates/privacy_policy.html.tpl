<h:piperka title="Privacy policy">
  <h2>Privacy policy</h2>
  <p>
    Piperka does not store personally identifiable information on its
    users or visitors.  If you choose a recognizable user name or
    email for you account then it's on you.
  </p>
  <p>
    Comic reading selections of Piperka's users are private by
    default.  They are used in aggregate form for recommendations and
    other site features.
  </p>
  <p>
    Piperka does not use tracking cookies.  Cookies are used for site
    features like sessions for logged in users.  Just visiting the
    site without logging in sets no cookies.
  </p>
  <p>
    Piperka uses a third party advertising
    network.  <a href="https://www.projectwonderful.com/privacypolicy.php">Please
    refer to their privacy policy</a>.  They only collect anonymous
    traffic statistics.  Use an ad blocker if you don't like that.
  </p>
  <p>
    Piperka allows connecting accounts using OAuth2 with a number of
    service providers.  If you do so, Piperka will store the minimum
    amount of user data on its end.
  </p>
  <p>
    Piperka does not share users' emails or other preferences with
    anyone, if it can avoid it.  I'm not sure what legal action could
    make it necessary.  I can't absolutely guarantee that there could
    not be some data breach that would leak them but I don't think it
    likely.
  </p>
  <p>
    This privacy policy was not written by a lawyer.  I may update
    this if it is made necessary by new site features but the spirit
    of any new versions should remain the same.
  </p>
</h:piperka>
