<h:piperka prefetch="Info">
  <h:withCid>
    <h:exists>
      <h:related/>
      <div class="script chart" id="readerchart"></div>
      <h:comicInfo/>
      <h3>Actions</h3>
      <ul>
	<li><a href="edit_info.html?cid=${h:cid}">Edit information</a></li>
	<h:ifLoggedIn>
	  <li>
	    <h:ifSubscribed>
	      <true>
		<h:subscribeForm/>
	      </true>
	      <false>
		<h:csrfForm method="post">
		  <input type="hidden" name="unsubscribe" value="${h:cid}"/>
		  <input type="submit" name="action" value="Unsubscribe"/>
		</h:csrfForm>
	      </false>
	    </h:ifSubscribed>
	  </li>
	</h:ifLoggedIn>
	<li><a href="http://${h:hostname}/reader/?cid=${h:cid}">View in Piperka Reader</a>
	<h:ifMapped>
	  <li><a href="/map/?cid=${h:cid}">View on Piperka Map</a></li>
	</h:ifMapped>
	<li><a href="ticket.html?cid=${h:cid}">Open ticket</a></li>
	<h:ifLoggedIn>
	  <li><a href="/my_tickets.html">My tickets</a></li>
	</h:ifLoggedIn>
      </ul>
      <h:ifLoggedIn>
	<h:ifMod level="2">
	  <h3>Admin</h3>
	  <ul>
	    <li><a href="mod_crawler.html?cid=${h:cid}">Crawler</a></li>
	    <li><a href="mod_morgue.html?cid=${h:cid}">Morgue</a></li>
	  </ul>
	</h:ifMod>
      </h:ifLoggedIn>
      <h:crawlErrors>
	<h3>Crawl errors</h3>
	<p>
	  The last 5 crawl errors during the last 30 days.  Having
	  this empty doesn't necessarily imply that there isn't
	  something wrong with the crawler.  I'll go through these
	  eventually but I don't mind if you ask me to check whether
	  the crawler's doing the right thing.
	</p>
	<table id="crawlerr">
	  <tr>
	    <th>Page order</th>
	    <th>Time</th>
	    <th>URL</th>
	    <th colspan="2">HTTP status</th>
	  </tr>
	  <h:rows>
	    <tr>
	      <td><h:ord/></td>
	      <td><h:time/></td>
	      <td><h:url/></td>
	      <td><h:code/></td>
	      <td><h:msg/></td>
	    </tr>
	  </h:rows>
	</table>
      </h:crawlErrors>
    </h:exists>
  </h:withCid>
</h:piperka>
