<h:piperka title="My removed comics">
  <h2>My removed comics</h2>
  <p>
    Web comics tend to be ephemeral.  Sites die, domains expire and
    comics disappear.  Or sometimes they still exist but undergo some
    transition that prevents Piperka from any longer following them.
  </p>
  <p>
    The full list of removed comics is on
    the <a href="/graveyard.html">graveyard</a> page.  This page shows
    a more personalised list of formerly listed comics.  We've
    provided an informal summary of a reason for a comic's removal at
    the time of removal which may or may not be accurate.
  </p>
  <h:ifLoggedIn>
    <h:userDeletedComics>
      <table>
	<thead>
	  <tr>
	    <th>CID</th>
	    <th>Title</th>
	    <th>Removed On</th>
	    <th>Reason</th>
	  </tr>
	</thead>
	<tbody>
	  <h:row>
	    <tr>
	      <td><h:cid/></td>
	      <td><a href="/deadinfo.html?cid=${h:cid}"><h:title/></a></td>
	      <td><h:time/></td>
	      <td><h:reason/></td>
	    </tr>
	  </h:row>
	</tbody>
      </table>
    </h:userDeletedComics>
  </h:ifLoggedIn>
  <h:ifLoggedOut>
    <p>
      If you were logged in, that is.
    </p>
  </h:ifLoggedOut>
</h:piperka>
