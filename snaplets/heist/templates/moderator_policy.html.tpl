<h:piperka ads="False" title="Moderator policy">
  <h2>Moderator policy</h2>
  <p>
    This is a policy document for moderators.  Brief and simple.
  </p>
  <p>
    The comic descriptions and tags are meant to give short
    introduction for comics.  If you think they can be improved on
    then feel free to.
  </p>
  <p>
    Comics listed on Piperka may be mature.  Some very much so.  But
    descriptions and banners on Piperka itself should remain SFW.
  </p>
  <p>
    Some comic blurbs on the authors' comic pages may be written using
    first person nouns.  It's fine for their use but I'd advise
    against copying them as is to Piperka.  It's not like I'm drawing
    their comics.
  </p>
  <p>
    Moderators' comic edits
    are <a href="edit_history.html">logged</a>.  If you feel like not
    volunteering for moderating anymore then please let me know.
  </p>
</h:piperka>
