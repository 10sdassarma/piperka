<h:piperka title="My tickets">
  <h2>My tickets</h2>
  <h:ifLoggedIn>
    <h:userTickets>
      <h:some>
	<div id="ticket">
	  <ul>
	    <li><a href="#tabs-1">Open tickets (<h:nOpen/>)</a></li>
	    <li><a href="#tabs-2">Closed tickets (<h:nClosed/>)</a></li>
	  </ul>
	  <h:common>
	    <div h:resolvedId="">
	      <h:list>
		<div class="ticket-entry">
		  Entry: <a href="info.html?cid=${h:cid}"><h:name/></a><br/>
		  Date: <h:stamp/>
		  <h:reason/><br/>
		  Message:<br/>
		  <textarea cols="80" rows="10" readonly="1"><h:message/></textarea><br/>
		  <h:resolved>
		    <hr/>
		    Date resolved: <h:resolvedDate/>
		    <h:hasMessage>
		      <br/>
		      Message:<br/>
		      <textarea cols="80" rows="10" readonly="1"><h:resolvedMessage/></textarea>
		    </h:hasMessage>
		  </h:resolved>
		</div>
	      </h:list>
	    </div>
	  </h:common>
	</div>
      </h:some>
      <h:none>
	You haven't opened any tickets yet.
      </h:none>
    </h:userTickets>
  </h:ifLoggedIn>
  <h:ifLoggedOut>
    But you are not logged in!
  </h:ifLoggedOut>
</h:piperka>
